package sereyd.com.soyeducadorasereyd.View.GroupView.StudentV;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.InformationStudent;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class StudentDetailActivity extends AppCompatActivity {

    @BindView(R.id.textViewStudentName)
    TextView textViewStudentName;
    @BindView(R.id.textViewFaltas)
    TextView textViewFaltas;
    @BindView(R.id.textViewFaltaDetalle)
    TextView textViewFaltasDetalle;
    @BindView(R.id.textViewAprovechamiento)
    TextView textViewAprovechamiento;
    @BindView(R.id.textViewAprovechamientoDetalle)
    TextView textViewAprovechamientoDetalle;
    @BindView(R.id.textViewDiagnostico)
    TextView textViewDiagnostico;
    @BindView(R.id.textViewDiagnosticoDetalle)
    TextView textViewDiagnosticoDetalle;
    @BindView(R.id.textViewConducta)
    TextView textViewConducta;
    @BindView(R.id.textViewConductaDetalle)
    TextView textViewConductaDetalle;

    @BindView(R.id.language)
    TextView textViewLanguage;
    @BindView(R.id.progressBarLanguauge)
    ProgressBar progressBarLanguauge;

    @BindView(R.id.math)
    TextView textViewMath;
    @BindView(R.id.progressBarMath)
    ProgressBar progressBarMath;

    @BindView(R.id.world)
    TextView textViewWorld;
    @BindView(R.id.progressBarWorld)
    ProgressBar progressBarWorld;

    @BindView(R.id.health)
    TextView textViewHealth;
    @BindView(R.id.progressBarHealth)
    ProgressBar progressBarHealth;

    @BindView(R.id.social)
    TextView textViewSocial;
    @BindView(R.id.progressBarSocial)
    ProgressBar progressBarSocial;

    @BindView(R.id.art)
    TextView textViewArt;
    @BindView(R.id.progressBarArt)
    ProgressBar progressBarArt;

    private Long idStudent;
    Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_detail);

        setCustomView();
        idStudent = getIntent().getExtras().getLong("student");
        student = Student.findById(Student.class, idStudent);
        setView(student);
        requestForStudentData(student);

    }

    private void requestForStudentData(final Student student) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();
        String url = Constants.URL_ENDPOINT_STUDENT_INFORMATION + student.getId_Student();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                InformationStudent informationStudent = StudentController.getInformationForStudent(response);

                if (informationStudent != null) {

                    ArrayList<InformationStudent> list = StudentController.getStudentInformationInDatabase(idStudent);
                    if (list.size() == 0) {

                        informationStudent.setStudent(student);
                        informationStudent.save();

                    } else {

                        InformationStudent infoDatabase = StudentController.getInformationById(list.get(0).getId());

                        infoDatabase.setAttendance(informationStudent.getAttendance());
                        infoDatabase.setBehaviorAverage(informationStudent.getBehaviorAverage());
                        infoDatabase.setBehaviorDescription(informationStudent.getBehaviorDescription());
                        infoDatabase.setMath(informationStudent.getMath());
                        infoDatabase.setWorld(informationStudent.getWorld());
                        infoDatabase.setHealth(informationStudent.getHealth());
                        infoDatabase.setSocial(informationStudent.getSocial());
                        infoDatabase.setArt(informationStudent.getArt());
                        infoDatabase.setDiagnostic(informationStudent.getDiagnostic());
                        infoDatabase.setSchoolPerformance(informationStudent.getSchoolPerformance());

                        infoDatabase.save();


                    }

                    setView(student);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(StudentDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getResources().getString(R.string.error_general))
                        .show();
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void setCustomView() {

        ButterKnife.bind(this);
        textViewStudentName.setTypeface(FontUtil.getBoldFont(getApplicationContext()));

        textViewFaltas.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewFaltasDetalle.setTypeface(FontUtil.getBoldFont(getApplicationContext()));
        textViewAprovechamiento.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewAprovechamientoDetalle.setTypeface(FontUtil.getBoldFont(getApplicationContext()));
        textViewDiagnostico.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewDiagnosticoDetalle.setTypeface(FontUtil.getBoldFont(getApplicationContext()));
        textViewConducta.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewConductaDetalle.setTypeface(FontUtil.getBoldFont(getApplicationContext()));


    }

    public void setView(Student student) {

        textViewStudentName.setText(student.getName() + " " + student.getLastName());
        InformationStudent informationStudent = StudentController.getInformationByStudent(idStudent);
        if (informationStudent != null) {

            textViewDiagnosticoDetalle.setText(informationStudent.getDiagnostic() + " pts");
            textViewFaltasDetalle.setText(informationStudent.getAttendance() + "");
            textViewConductaDetalle.setText(informationStudent.getBehaviorDescription());
            textViewAprovechamientoDetalle.setText(informationStudent.getSchoolPerformance() + "%");

            textViewLanguage.setText(informationStudent.getLanguage() + "%");
            progressBarLanguauge.setProgress((int) informationStudent.getLanguage());

            textViewMath.setText(informationStudent.getMath() + "%");
            progressBarMath.setProgress((int) informationStudent.getMath());

            textViewWorld.setText(informationStudent.getWorld() + "%");
            progressBarWorld.setProgress((int) informationStudent.getWorld());

            textViewHealth.setText(informationStudent.getHealth() + "%");
            progressBarHealth.setProgress((int) informationStudent.getHealth());

            textViewSocial.setText(informationStudent.getSocial() + "%");
            progressBarSocial.setProgress((int) informationStudent.getSocial());

            textViewArt.setText(informationStudent.getArt() + "%");
            progressBarArt.setProgress((int) informationStudent.getArt());
        }

    }

    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    @OnClick(R.id.buttonEmergencyCall)
    public void emergencyCall(View view) {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            makeCall();

        }
    }

    private void makeCall() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(this.getResources().getString(R.string.aviso))
                .setContentText(this.getResources().getString(R.string.pregunta_llamada))
                .setConfirmText(this.getResources().getString(R.string.dialog_ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + student.getEmergencyContact()));
                        if (ActivityCompat.checkSelfPermission(StudentDetailActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(callIntent);
                    }
                })
                .setCancelText(this.getResources().getString(R.string.cancelar))
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    makeCall();

                } else {


                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }


}
