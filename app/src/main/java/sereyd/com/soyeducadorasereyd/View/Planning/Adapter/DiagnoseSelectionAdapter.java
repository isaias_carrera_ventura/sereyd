package sereyd.com.soyeducadorasereyd.View.Planning.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.GenericClass.RecyclerViewDiagnoseSelection;
import sereyd.com.soyeducadorasereyd.Models.DiagnoseSelection;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;

/**
 * Created by chaycv on 19/07/17.
 */

public class DiagnoseSelectionAdapter extends RecyclerView.Adapter<DiagnoseSelectionAdapter.MyViewHolder> {

    private List<DiagnoseSelection> diagnoseSelections;
    private Context context;
    private static RecyclerViewDiagnoseSelection itemListener;

    public DiagnoseSelectionAdapter(List<DiagnoseSelection> diagnose, Context context, RecyclerViewDiagnoseSelection itemListenerActivity) {

        this.diagnoseSelections = diagnose;
        itemListener = itemListenerActivity;
        this.context = context;

    }

    public Context getContext() {
        return context;
    }

    @Override
    public DiagnoseSelectionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_diagnose_selection, parent, false);
        return new DiagnoseSelectionAdapter.MyViewHolder(itemView);

    }

    private RadioButton lastCheckedRB = null;
    final int positionAux = 0;

    @Override
    public void onBindViewHolder(DiagnoseSelectionAdapter.MyViewHolder holder, int position) {

        final int positionAux = position;
        holder.title.setText(diagnoseSelections.get(position).getName());
        holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                RadioButton checked_rb = (RadioButton) compoundButton.findViewById(R.id.radioButtonSelection);
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }
                //store the clicked radiobutton
                lastCheckedRB = checked_rb;

                itemListener.recyclerViewListClicked(compoundButton, positionAux);
            }
        });
    }

    @Override
    public int getItemCount() {
        return diagnoseSelections.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {//implements View.OnClickListener {

        @BindView(R.id.textViewNameDiagnostic)
        TextView title;

        @BindView(R.id.radioButtonSelection)
        RadioButton radioButton;

        public MyViewHolder(View view) {

            super(view);
            ButterKnife.bind(this, view);
            title.setTypeface(FontUtil.getRegularFont(getContext()));

        }

    }

}
