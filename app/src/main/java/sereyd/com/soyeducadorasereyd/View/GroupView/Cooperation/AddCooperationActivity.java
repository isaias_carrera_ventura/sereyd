package sereyd.com.soyeducadorasereyd.View.GroupView.Cooperation;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.CooperationController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Cooperation;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class AddCooperationActivity extends Activity implements Validator.ValidationListener {

    @BindView(R.id.textViewAvisoJunta)
    TextView textViewTitleView;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTexTCooperationName)
    EditText editTextCooperationName;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextCooperationSubject)
    EditText editTextCooperationSubject;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextQuantityCooperation)
    EditText editTextQuantity;

    @BindView(R.id.buttonCooperationRegister)
    Button button;

    private Validator validator;

    private Long idToEdit;
    private int positonRecyclerToReturn;

    MyGroup myGroup;
    Long idGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cooperation);

        ButterKnife.bind(this);

        setCustomView();

        idGroup = getIntent().getExtras().getLong("group");
        myGroup = MyGroup.findById(MyGroup.class, idGroup);

        idToEdit = getIntent().getLongExtra("cooperationToEdit", -1);
        positonRecyclerToReturn = getIntent().getIntExtra("positionRecycler", -1);

        //Edition
        if (idToEdit != -1 && positonRecyclerToReturn != -1) {
            setCooperationToViewDetail(Cooperation.findById(Cooperation.class, idToEdit));
        }

    }

    private void setCooperationToViewDetail(Cooperation cooperation) {

        editTextCooperationName.setText(cooperation.getConcept());
        editTextCooperationSubject.setText(cooperation.getDescription());
        editTextQuantity.setText(String.valueOf(cooperation.getAmmount()));
    }

    private void setCustomView() {

        validator = new Validator(this);
        validator.setValidationListener(this);
        textViewTitleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextCooperationName.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextCooperationSubject.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextQuantity.setTypeface((FontUtil.getRegularFont(getApplicationContext())));
        button.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
    }

    @OnClick(R.id.buttonCooperationRegister)
    public void addCooperationButtonClicked(View view) {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {

        final Cooperation cooperation = new Cooperation(null, editTextCooperationName.getText().toString(),
                editTextCooperationSubject.getText().toString(),
                Double.valueOf(editTextQuantity.getText().toString()),
                null);

        if (idToEdit != -1) {

            Cooperation cooperationEdit = Cooperation.findById(Cooperation.class, idToEdit);
            requestForUpdateCooperation(cooperation, cooperationEdit);

        } else {

            requestForNewCooperation(cooperation);


        }

    }

    private void requestForUpdateCooperation(final Cooperation cooperation, Cooperation cooperationInDB) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, Constants.URL_ENDPOINT_COOPERATION + cooperationInDB.getId_cooperation(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                if (CooperationController.editCooperationFromResponse(response)) {

                    new SweetAlertDialog(AddCooperationActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.aviso))
                            .setContentText(getString(R.string.cooperacion_editada))
                            .setConfirmText(getString(R.string.dialog_ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .show();
                } else {

                    new SweetAlertDialog(AddCooperationActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(AddCooperationActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("concept", cooperation.getConcept());
                params.put("description", cooperation.getDescription());
                params.put("amount", String.valueOf(cooperation.getAmmount()));
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void requestForNewCooperation(final Cooperation cooperation) {
        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_COOPERATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                Cooperation cooperationToSave = CooperationController.getCooperationFromResponse(response);

                if (cooperationToSave != null) {

                    cooperationToSave.setMyGroup(myGroup);
                    CooperationController.saveCooperationToDatabase(cooperationToSave);

                    new SweetAlertDialog(AddCooperationActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.cooperacion_creada))
                            .setConfirmText(getString(R.string.dialog_ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .show();
                } else {
                    new SweetAlertDialog(AddCooperationActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(AddCooperationActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("concept", cooperation.getConcept());
                params.put("description", cooperation.getDescription());
                params.put("amount", String.valueOf(cooperation.getAmmount()));
                params.put("id_group", String.valueOf(myGroup.getIdGpr()));

                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
