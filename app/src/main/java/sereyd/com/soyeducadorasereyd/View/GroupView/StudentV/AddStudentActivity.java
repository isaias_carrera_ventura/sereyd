package sereyd.com.soyeducadorasereyd.View.GroupView.StudentV;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class AddStudentActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.editTextEmailContact)
    EditText editTextEmailContact;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextStudentName)
    EditText editTextStudentName;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextLastName)
    EditText editTextLastName;

    @Pattern(regex = "^((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])$", messageResId = R.string.field_required)
    @NotEmpty(trim = true)
    @BindView(R.id.editTextBirthday)
    EditText editTextBirthday;

    @NotEmpty(trim = true)
    @Pattern(regex = "^[0-9]{10}$", messageResId = R.string.phone_incorrect)
    @BindView(R.id.editTextEmergencyContact)
    EditText editTextEmergencyContact;

    @BindView(R.id.textViewAge)
    TextView textViewAgeStudent;
    @BindView(R.id.textViewSex)
    TextView textViewSexStudent;
    @BindView(R.id.textViewStudentStatus)
    TextView textViewStudentStatus;
    @BindView(R.id.textViewAlertEmailParent)
    TextView textViewAlertEmailParent;

    @BindView(R.id.buttonStudentRegister)
    Button buttonRegisterStudent;

    @BindView(R.id.sexRadioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.radio_H)
    RadioButton buttonRadioH;
    @BindView(R.id.radio_M)
    RadioButton buttonRadioM;

    private Validator validator;

    private Calendar calendar;
    private int year, month, day;
    int sexStudent = Constants.FEMALE; //0 Hombres - 1 Mujeres

    private Long idToEdit;
    private Long idGroup;
    private int positonRecyclerToReturn;
    private boolean hasEmailStudent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_student);

        setCustomViews();

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        idToEdit = getIntent().getLongExtra("studentToEdit", -1);
        idGroup = getIntent().getLongExtra("group", -1);
        positonRecyclerToReturn = getIntent().getIntExtra("positionRecycler", -1);

        if (idToEdit != -1 && positonRecyclerToReturn != -1) {
            textViewStudentStatus.setText(R.string.editar_alumno);
            setStudentToViewDetail(Student.findById(Student.class, idToEdit));
        }

    }

    private void setStudentToViewDetail(Student student) {

        editTextStudentName.setText(student.getName());
        editTextLastName.setText(student.getLastName());
        editTextBirthday.setText(student.getBirthday());
        editTextEmergencyContact.setText(student.getEmergencyContact());
        editTextEmailContact.setText(student.getEmailTutor());
        //RADIO BUTTON
        if (student.getSex() == Constants.MALE) {
            buttonRadioH.setChecked(true);
            sexStudent = Constants.MALE;
        } else {
            buttonRadioM.setChecked(true);
            sexStudent = Constants.FEMALE;
        }

    }

    public void setDate() {

        DatePickerDialog dialog = new DatePickerDialog(this, R.style.datepicker, myDateListener, year, month, day);
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        dialog.setTitle("");
        dialog.getDatePicker().setBackgroundColor(Color.WHITE);

        dialog.show();

    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // arg1 = year arg2 = month arg3 = day
                    String date = arg1 + "/" + (arg2 + 1) + "/" + arg3;
                    editTextBirthday.setText(date);
                }
            };


    private void setCustomViews() {

        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        editTextStudentName.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextLastName.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextBirthday.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextEmergencyContact.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewAgeStudent.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewSexStudent.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        buttonRegisterStudent.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewStudentStatus.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextEmailContact.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewAlertEmailParent.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextBirthday.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                    setDate();
                }

                return true;
            }
        });

        editTextBirthday.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    setDate();
                }
            }
        });

    }

    @OnClick({R.id.radio_H, R.id.radio_M})
    public void getSex(View view) {

        if (view.getId() == R.id.radio_M) {
            this.sexStudent = Constants.FEMALE;
        } else {
            this.sexStudent = Constants.MALE;
        }
    }

    @OnClick(R.id.buttonStudentRegister)
    public void RegisterStudent(View view) {

        if (!editTextEmailContact.getText().toString().equals("")) {

            if (Constants.isValidEmail(editTextEmailContact.getText().toString())) {

                hasEmailStudent = true;
                validator.validate();

            } else {

                new SweetAlertDialog(AddStudentActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.add_email))
                        .show();

            }

        } else {

            hasEmailStudent = false;
            validator.validate();

        }


    }

    @Override
    public void onValidationSucceeded() {

        Student student = new Student(
                editTextStudentName.getText().toString(),
                editTextLastName.getText().toString(),
                editTextBirthday.getText().toString(),
                this.sexStudent,
                editTextEmergencyContact.getText().toString(),
                editTextEmailContact.getText().toString(),
                null, null,
                GroupController.getGroupById(idGroup));

        if (idToEdit != -1) {

            udpateStudentToSever(student);

        } else {

            requestNewStudentToServer(student);

        }

    }

    private void udpateStudentToSever(final Student student) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, Constants.URL_ENDPOINT_STUDENT + Student.findById(Student.class, idToEdit).getId_Student(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                if (StudentController.editStudentResponse(response)) {

                    new SweetAlertDialog(AddStudentActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.aviso))
                            .setContentText(getString(R.string.alumno_editado))
                            .setConfirmText(getString(R.string.dialog_ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .show();


                } else {
                    new SweetAlertDialog(AddStudentActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(AddStudentActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", student.getName());
                params.put("lastname", student.getLastName());
                params.put("birthday", student.getBirthday());
                params.put("gender", String.valueOf(student.getSex()));
                params.put("emergency_contact", student.getEmergencyContact());
                if (hasEmailStudent) {
                    params.put("email", editTextEmailContact.getText().toString());
                }
                return params;
            }

        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void requestNewStudentToServer(final Student student) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_STUDENT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                Student studentToSave = StudentController.getStudentFromResponse(response);
                if (studentToSave != null) {

                    studentToSave.setMyGroup(GroupController.getGroupById(idGroup));
                    StudentController.addStudentToDatabase(studentToSave);

                    new SweetAlertDialog(AddStudentActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.alumno_creado))
                            .setConfirmText(getString(R.string.dialog_ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK, returnIntent);
                                    finish();
                                }
                            })
                            .show();


                } else {
                    new SweetAlertDialog(AddStudentActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(AddStudentActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", student.getName());
                params.put("lastname", student.getLastName());
                params.put("birthday", student.getBirthday());
                params.put("gender", String.valueOf(student.getSex()));
                params.put("emergency_contact", student.getEmergencyContact());
                params.put("id_group", String.valueOf(GroupController.getGroupById(idGroup).getIdGpr()));
                if (hasEmailStudent) {
                    params.put("email", editTextEmailContact.getText().toString());
                }
                return params;
            }

        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);


    }

    @OnClick(R.id.textViewAlertEmailParent)
    public void alertEmailParent(View view) {
        new SweetAlertDialog(AddStudentActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.aviso))
                .setContentText(getString(R.string.portal_parent))
                .setConfirmText("Ir")
                .setCancelText(getString(R.string.cancelar))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                    }
                })
                .show();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {

            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
