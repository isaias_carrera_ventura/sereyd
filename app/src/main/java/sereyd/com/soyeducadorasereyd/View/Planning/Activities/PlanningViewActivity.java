package sereyd.com.soyeducadorasereyd.View.Planning.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.PlanningController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Planning;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.InputStreamVolleyRequest;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class PlanningViewActivity extends AppCompatActivity {

    @BindView(R.id.pdfView)
    PDFView pdfView;
    Planning planning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning_view);
        ButterKnife.bind(this);

        planning = (Planning) getIntent().getSerializableExtra("planning");


        if (PlanningController.isPlanningFilePDFInDisk(planning, this)) {

            byte[] planningRaw = PlanningController.getPlanningFilePDFInDisk(planning, PlanningViewActivity.this);
            pdfView.fromBytes(planningRaw).load();

        } else {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
            customProgressDialog.showCustomProgressDialog();

            String mUrl = planning.getUrlPDF();
            InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, mUrl,
                    new Response.Listener<byte[]>() {

                        @Override
                        public void onResponse(byte[] response) {
                            // TODO handle the response
                            customProgressDialog.dismissCustomProgressDialog();
                            try {
                                if (response != null) {

                                    pdfView.fromBytes(response).onError(new OnErrorListener() {
                                        @Override
                                        public void onError(Throwable t) {
                                            new SweetAlertDialog(PlanningViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText(getString(R.string.ops))
                                                    .setContentText(getString(R.string.algo_salio_mal))
                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                            sweetAlertDialog.dismissWithAnimation();
                                                            finish();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }).load();
                                }
                            } catch (Exception e) {
                                new SweetAlertDialog(PlanningViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText(getString(R.string.ops))
                                        .setContentText(getString(R.string.algo_salio_mal))
                                        .show();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO handle the error
                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(PlanningViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }
            }, null);

            MySingleton.getInstance(this).addToRequestQueue(request);
        }


    }
}
