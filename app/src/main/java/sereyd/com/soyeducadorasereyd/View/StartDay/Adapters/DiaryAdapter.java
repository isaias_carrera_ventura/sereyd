package sereyd.com.soyeducadorasereyd.View.StartDay.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.Models.Diary;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.StartDay.Activities.DiaryDetail;

/**
 * Created by chaycv on 01/05/17.
 */

public class DiaryAdapter extends RecyclerView.Adapter<DiaryAdapter.MyViewHolder> {


    private List<Diary> diaryList;
    private Context context;

    public DiaryAdapter(List<Diary> diaryList, Context context) {
        this.diaryList = diaryList;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public DiaryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_diary, parent, false);

        return new DiaryAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final DiaryAdapter.MyViewHolder holder, final int position) {
        //Populate view

        holder.titleDiary.setText(getContext().getResources().getString(R.string.diario) + ": " + diaryList.get(position).getDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(getContext(), DiaryDetail.class);
                i.putExtra("diary", diaryList.get(position));
                getContext().startActivity(i);


            }
        });
    }

    @Override
    public int getItemCount() {
        return diaryList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.titleDiary)
        TextView titleDiary;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            titleDiary.setTypeface(FontUtil.getRegularFont(getContext()));

        }

    }

}
