package sereyd.com.soyeducadorasereyd.View.GroupView.Group;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.DecimalMax;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class AddGroupActivity extends AppCompatActivity implements Validator.ValidationListener {


    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextSchoolName)
    EditText editTextSchoolName;

    @DecimalMax(value = 500)
    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextGrade)
    EditText editTextGrade;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextGroup)
    EditText editTextGroup;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextSchoolPeriod)
    EditText editTextSchoolPeriod;

    @BindView(R.id.textViewGroupAdd)
    TextView textViewGroup;
    @BindView(R.id.textViewGradeAdd)
    TextView textViewGrade;
    @BindView(R.id.textViewGroupLabelStatus)
    TextView textViewGroupLabelStatus;
    @BindView(R.id.buttonGroupRegister)
    Button button;

    private Validator validator;
    private String idUser;
    private Long idToEdit;
    private int positonRecyclerToReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_group);
        setCustomViews();
        idToEdit = getIntent().getLongExtra("groupToEdit", -1);
        positonRecyclerToReturn = getIntent().getIntExtra("positionRecycler", -1);
        idUser = UserController.getUser().getIdUser();

        if (idToEdit != -1 && positonRecyclerToReturn != -1) {
            textViewGroupLabelStatus.setText(R.string.editar_grupo);
            setGroupToViewDetail(MyGroup.findById(MyGroup.class, idToEdit));
        }

    }

    private void setGroupToViewDetail(MyGroup group) {


        editTextSchoolName.setText(group.getSchool());
        editTextGrade.setText(String.valueOf(group.getGrade()));
        editTextGroup.setText(group.getGroupLetter());
        editTextSchoolPeriod.setText(group.getPeriod());

    }


    private void setCustomViews() {

        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        editTextSchoolName.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextGrade.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextGroup.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextSchoolPeriod.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewGrade.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewGroup.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewGroupLabelStatus.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        button.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

    }

    @Override
    public void onValidationSucceeded() {

        final MyGroup myGroup = new MyGroup(0,
                editTextSchoolName.getText().toString(),
                Integer.valueOf(editTextGrade.getText().toString()),
                editTextGroup.getText().toString(),
                editTextSchoolPeriod.getText().toString(), 0.0);

        if (idToEdit != -1) {


            updateGroup(idToEdit, myGroup);

        } else {

            //REGISTER NEW GROUP
            registerNewGroup(myGroup);

        }


    }


    private void updateGroup(Long idGroup, final MyGroup myGroupParams) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        final MyGroup myGroup = MyGroup.findById(MyGroup.class, idGroup);
        int idGroupServer = myGroup.getIdGpr();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, Constants.URL_ENDPOINT_GROUP + idGroupServer, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                if (GroupController.editResponseFromServer(response)) {

                    new SweetAlertDialog(AddGroupActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.aviso))
                            .setContentText(getString(R.string.grupo_editado))
                            .setConfirmText(getString(R.string.dialog_ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .show();
                } else {
                    new SweetAlertDialog(AddGroupActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(AddGroupActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("school", myGroupParams.getSchool());
                params.put("grade", String.valueOf(myGroupParams.getGrade()));
                params.put("group", myGroupParams.getGroupLetter());
                params.put("period", myGroupParams.getPeriod());
                params.put("user", idUser);
                return params;
            }

        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);


    }

    private void registerNewGroup(final MyGroup myGroup) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        final String school = myGroup.getSchool();
        final int grade = myGroup.getGrade();
        final String group = myGroup.getGroupLetter();
        final String period = myGroup.getPeriod();
        final String idUser = UserController.getUser().getIdUser();

        String url = Constants.URL_ENDPOINT_GROUP;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                final MyGroup groupResponse = GroupController.getGroupFromResponse(response);
                if (groupResponse != null) {

                    GroupController.saveGroupInDatabase(groupResponse);

                    new SweetAlertDialog(AddGroupActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.grupo_creado))
                            .setConfirmText(getString(R.string.dialog_ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra("group", groupResponse);
                                    setResult(Activity.RESULT_OK, returnIntent);
                                    finish();
                                }
                            })
                            .show();


                } else {

                    new SweetAlertDialog(AddGroupActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(AddGroupActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("school", school);
                params.put("grade", String.valueOf(grade));
                params.put("group", group);
                params.put("period", period);
                params.put("user", idUser);
                return params;
            }

        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {

            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }


    @OnClick(R.id.buttonGroupRegister)
    public void RegisterGroup(View view) {
        validator.validate();
    }

}
