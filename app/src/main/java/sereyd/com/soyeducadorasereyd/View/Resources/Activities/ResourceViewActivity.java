package sereyd.com.soyeducadorasereyd.View.Resources.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.snatik.storage.Storage;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.ResourceController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.ResourceItemFile;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.InputStreamVolleyRequest;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class ResourceViewActivity extends AppCompatActivity {

    @BindView(R.id.pdfView)
    PDFView pdfView;
    @BindView(R.id.textViewResourceName)
    TextView textViewResourceName;
    @BindView(R.id.recursosViewText)
    TextView recursosViewText;
    @BindView(R.id.toolbarMainGroup)
    Toolbar toolbar;
    ResourceItemFile resourceItemFile;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource_view);
        resourceItemFile = (ResourceItemFile) getIntent().getExtras().getSerializable("resource");
        setupView();

        if (ResourceController.isResourceInDatabase(resourceItemFile) && ResourceController.isResourceFileInDisk(resourceItemFile, this)) {

            byte[] resourceFile = ResourceController.getResourceFilePDFInDisk(resourceItemFile, this);
            pdfView.fromBytes(resourceFile).load();

        } else {
            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
            customProgressDialog.showCustomProgressDialog();

            String mUrl = resourceItemFile.getUrlFile();
            InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, mUrl,
                    new Response.Listener<byte[]>() {

                        @Override
                        public void onResponse(byte[] response) {
                            // TODO handle the response
                            customProgressDialog.dismissCustomProgressDialog();
                            try {
                                if (response != null) {

                                    pdfView.fromBytes(response).onError(new OnErrorListener() {
                                        @Override
                                        public void onError(Throwable t) {
                                            new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText(getString(R.string.ops))
                                                    .setContentText(getString(R.string.algo_salio_mal))
                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                            sweetAlertDialog.dismissWithAnimation();
                                                            finish();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }).load();
                                }
                            } catch (Exception e) {
                                new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText(getString(R.string.ops))
                                        .setContentText(getString(R.string.algo_salio_mal))
                                        .show();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO handle the error
                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }
            }, null);

            MySingleton.getInstance(this).addToRequestQueue(request);

        }


    }

    private void setupView() {

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }
        textViewResourceName.setTypeface(FontUtil.getRegularFont(this));
        recursosViewText.setTypeface(FontUtil.getRegularFont(this));
        textViewResourceName.setText(resourceItemFile.getName());
    }

    @OnClick(R.id.buttonShareResource)
    public void shareFileResource(View view) {

        if (ResourceController.isResourceFileInDisk(resourceItemFile, this)) {

            String filePathPDF = ResourceController.getFileAbsoluteFilePDFPathForPlanning(resourceItemFile, this);
            File filePDF = new File(filePathPDF);
            Uri pathPDF = Uri.fromFile(filePDF);

            //Intent emailIntent = new Intent(Intent.ACTION_SEND);
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            // set the type to 'email'
            emailIntent.setType("vnd.android.cursor.dir/email");
            String to[] = {};
            emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
            // the attachment
            emailIntent.putExtra(Intent.EXTRA_STREAM, pathPDF);

            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Recurso");
            startActivity(Intent.createChooser(emailIntent, "Enviar recurso vía e-mail..."));

        } else {
            new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.adquirir_download_r))
                    .show();
        }

    }

    @OnClick(R.id.buttonDownloadResource)
    public void downloadResource(View view) {

        if (ResourceController.isResourceFileInDisk(resourceItemFile, this)) {

            new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.file_alredy_downloaded))
                    .show();

        } else {

            // Check if we have write permission
            int permission = ActivityCompat.checkSelfPermission(ResourceViewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permission != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    // We don't have permission so prompt the user
                    ActivityCompat.requestPermissions(
                            ResourceViewActivity.this,
                            PERMISSIONS_STORAGE,
                            REQUEST_EXTERNAL_STORAGE
                    );


                } else {

                    new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.aviso))
                            .setContentText(getString(R.string.access_files))
                            .setConfirmText("Si").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            sweetAlertDialog.dismissWithAnimation();
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        }
                    }).show();


                }

            } else {

                new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.aviso))
                        .setContentText(getString(R.string.download_file_resource))
                        .setConfirmText("Si")
                        .setCancelText("Cancelar")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                                requestForDownloadResourceFile();
                            }
                        }).show();


            }


        }


    }


    private void requestForDownloadResourceFile() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_RESOURCE_USER, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {

                if (ResourceController.asignResourceToDatabase(resourceItemFile, response)) {

                    final Storage storage = new Storage(getApplicationContext());
                    final String path = storage.getExternalStorageDirectory();
                    final String newDir = path + File.separator + "resources";
                    storage.createDirectory(newDir);

                    InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, resourceItemFile.getUrlFile(),
                            new Response.Listener<byte[]>() {

                                @Override
                                public void onResponse(final byte[] responsePDF) {

                                    customProgressDialog.dismissCustomProgressDialog();

                                    try {

                                        if (responsePDF != null) {

                                            String responseAccount = ResourceController.getMissingResource(response);
                                            storage.createFile(ResourceController.getFilePathForResource(newDir, resourceItemFile), responsePDF);
                                            new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                    .setTitleText(getString(R.string.aviso))
                                                    .setContentText(getString(R.string.download_success_r) + responseAccount)
                                                    .show();

                                        }
                                    } catch (Exception e) {

                                        customProgressDialog.dismissCustomProgressDialog();
                                        new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText(getString(R.string.ops))
                                                .setContentText(getString(R.string.algo_salio_mal))
                                                .show();
                                    }
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            customProgressDialog.dismissCustomProgressDialog();
                            new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText(getString(R.string.ops))
                                    .setContentText(getString(R.string.algo_salio_mal))
                                    .show();
                        }
                    }, null);

                    MySingleton.getInstance(ResourceViewActivity.this).addToRequestQueue(request);


                } else {

                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal) + " " + getString(R.string.no_items_left))
                            .show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(new String(error.networkResponse.data, "UTF-8"));
                    if (jsonObject.getInt("error") == 1) {
                        new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.algo_salio_mal) + " " + getString(R.string.no_items_left))
                                .show();

                    }
                } catch (Exception e) {
                    new SweetAlertDialog(ResourceViewActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", UserController.getUser().getIdUser());
                params.put("id_resource", String.valueOf(resourceItemFile.getIdResource()));
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }
}
