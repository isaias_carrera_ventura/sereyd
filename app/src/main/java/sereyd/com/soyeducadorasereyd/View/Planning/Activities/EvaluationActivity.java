package sereyd.com.soyeducadorasereyd.View.Planning.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.DiagnoseController;
import sereyd.com.soyeducadorasereyd.Controllers.EvaluationController;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.Controllers.TestController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Evaluation;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Models.TestModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Fragments.EvaluationStudentFragment;

public class EvaluationActivity extends AppCompatActivity {

    @BindView(R.id.toolbarEvaluation)
    Toolbar toolbar;
    @BindView(R.id.viewpagerEvaluation)
    ViewPager viewPager;
    @BindView(R.id.evaluationTitleView)
    TextView titleView;

    Long myGroupId;
    Long evaluationId;

    List<Student> studentArrayList;
    List<TestModel> testModelList;
    List<String> idStudentList;
    private Evaluation evaluation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation);

        myGroupId = getIntent().getExtras().getLong("group");
        studentArrayList = StudentController.getStudents(myGroupId);
        testModelList = (List<TestModel>) getIntent().getExtras().getSerializable("test");
        idStudentList = (List<String>) getIntent().getExtras().getSerializable("studentsID");
        evaluationId = getIntent().getExtras().getLong("evaluation");
        evaluation = EvaluationController.getEvaluationById(evaluationId);

        if (idStudentList == null) {
            Log.w("Mensaje", "AOC");
            studentArrayList = StudentController.getStudents(myGroupId);
        } else {
            studentArrayList = TestController.getStudentArrayInDatabaseForPendingDiagnose(idStudentList, myGroupId);
            Log.w("ARRAY LIST", studentArrayList.size() + "");
            Log.w("idStudentList LIST", idStudentList.size() + "");
        }

        setCustomView();
    }

    private void setCustomView() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager.beginFakeDrag();
        setupViewPager(viewPager);
        titleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

    }

    private void setupViewPager(ViewPager viewPager) {

        EvaluationActivity.ViewPagerAdapter adapter = new EvaluationActivity.ViewPagerAdapter(getSupportFragmentManager());
        for (Student student : studentArrayList) {

            Bundle bundle = new Bundle();
            bundle.putLong("idStudent", student.getId());
            bundle.putInt("position", studentArrayList.indexOf(student));
            bundle.putLong("group", myGroupId);
            bundle.putLong("evaluation", evaluationId);
            bundle.putSerializable("test", (Serializable) testModelList);

            EvaluationStudentFragment diagnoseFragment = new EvaluationStudentFragment();
            diagnoseFragment.setArguments(bundle);
            adapter.addFragment(diagnoseFragment, "SAMPLE");

        }

        viewPager.setAdapter(adapter);

    }

    public void selectPage(int page) {

        if (page == studentArrayList.size()) {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(EvaluationActivity.this);
            customProgressDialog.showCustomProgressDialog();

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, Constants.URL_ENDPOINT_TEST_EVALUATION_FINISHED + evaluation.getEvaluationGroupId(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    if (DiagnoseController.getStatusForCompletionDiagnose(response)) {

                        Evaluation evaluation = EvaluationController.getEvaluationById(evaluationId);
                        evaluation.setState(Constants.COMPLETE);
                        evaluation.save();
                        new SweetAlertDialog(EvaluationActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(getString(R.string.aviso))
                                .setContentText(getString(R.string.evaluation_terminada))
                                .setConfirmText(getString(R.string.dialog_ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                        finish();
                                    }
                                })
                                .show();

                    } else {

                        new SweetAlertDialog(EvaluationActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.algo_salio_mal))
                                .show();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(EvaluationActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();

                }
            }) {

            };

            MySingleton.getInstance(EvaluationActivity.this).addToRequestQueue(stringRequest);


        } else {

            for (int i = 0; i < testModelList.size(); i++) {
                testModelList.get(i).setResult(Constants.UNDEFINED);
            }
            viewPager.setCurrentItem(page, true);

        }
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                navigateUpFromActivity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void navigateUpFromActivity() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.aviso_diagnostico))
                .setContentText(getString(R.string.question_diagnostico_finish))
                .setConfirmText(getString(R.string.later))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        NavUtils.navigateUpFromSameTask(EvaluationActivity.this);

                    }
                })
                .show();

    }

    @Override
    public void onBackPressed() {

        //super.onBackPressed();
        navigateUpFromActivity();

    }

}
