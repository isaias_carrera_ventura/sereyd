package sereyd.com.soyeducadorasereyd.View.Planning.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.GenericClass.RecyclerViewDiagnoseSelection;
import sereyd.com.soyeducadorasereyd.Models.EvaluationSelection;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;

/**
 * Created by chaycv on 23/08/17.
 */

public class EvaluationSelectionAdapter extends RecyclerView.Adapter<EvaluationSelectionAdapter.MyViewHolder> {

    private List<EvaluationSelection> evaluationSelections;
    private Context context;
    private static RecyclerViewDiagnoseSelection itemListener;

    public EvaluationSelectionAdapter(List<EvaluationSelection> evaluation, Context context, RecyclerViewDiagnoseSelection itemListenerActivity) {

        this.evaluationSelections = evaluation;
        itemListener = itemListenerActivity;
        this.context = context;

    }

    public Context getContext() {
        return context;
    }

    @Override
    public EvaluationSelectionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_diagnose_selection, parent, false);
        return new EvaluationSelectionAdapter.MyViewHolder(itemView);

    }

    private RadioButton lastCheckedRB = null;
    final int positionAux = 0;

    @Override
    public void onBindViewHolder(EvaluationSelectionAdapter.MyViewHolder holder, int position) {

        final int positionAux = position;
        holder.title.setText(evaluationSelections.get(position).getName());
        holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                RadioButton checked_rb = (RadioButton) compoundButton.findViewById(R.id.radioButtonSelection);
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }
                //store the clicked radiobutton
                lastCheckedRB = checked_rb;

                itemListener.recyclerViewListClicked(compoundButton, positionAux);
            }
        });
    }

    @Override
    public int getItemCount() {
        return evaluationSelections.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {//implements View.OnClickListener {

        @BindView(R.id.textViewNameDiagnostic)
        TextView title;

        @BindView(R.id.radioButtonSelection)
        RadioButton radioButton;

        public MyViewHolder(View view) {

            super(view);
            ButterKnife.bind(this, view);
            title.setTypeface(FontUtil.getRegularFont(getContext()));

        }

    }


}
