package sereyd.com.soyeducadorasereyd.View.Forum;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.ForumDiscussionModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RankAnswerActivityForum extends Activity {

    @BindView(R.id.answer)
    TextView answerTextView;
    @BindView(R.id.imageViewProfile)
    NetworkImageView imageViewProfile;
    @BindView(R.id.answerDate)
    TextView answerDate;
    @BindView(R.id.rankingAnswer)
    MaterialRatingBar rankingAnswer;

    @BindView(R.id.rankingAnswerSend)
    MaterialRatingBar rankingAnswerSend;


    ForumDiscussionModel forumDiscussionModel;
    private boolean flagEnable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank_answer_forum);

        ButterKnife.bind(this);

        forumDiscussionModel = (ForumDiscussionModel) getIntent().getExtras().get("answer");

        ImageLoader mImageLoader = MySingleton.getInstance(this).getImageLoader();
        final String url = forumDiscussionModel.getImage();
        mImageLoader.get(url, ImageLoader.getImageListener(imageViewProfile, R.mipmap.person_gray, R.mipmap.person_gray));
        rankingAnswer.setMax(5);
        rankingAnswer.setEnabled(false);
        rankingAnswer.setProgress(forumDiscussionModel.getRanking());
        imageViewProfile.setImageUrl(url, mImageLoader);
        answerTextView.setText(forumDiscussionModel.getAnswer());
        answerDate.setText(forumDiscussionModel.getDate());

        rankingAnswerSend.setStepSize(1);
        rankingAnswerSend.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                flagEnable = true;
            }
        });

    }

    @OnClick(R.id.buttonSend)
    public void sendRanking() {

        if (flagEnable) {
            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_ANSWER_RANKING, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {


                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getInt("error") == 0) {

                            new SweetAlertDialog(RankAnswerActivityForum.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(getString(R.string.answer_created))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismissWithAnimation();
                                            RankAnswerActivityForum.this.finish();
                                        }
                                    })
                                    .setContentText(getString(R.string.answer_created_success))
                                    .show();
                        }

                    } catch (Exception ignored) {

                    }

                    customProgressDialog.dismissCustomProgressDialog();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    String msg = "";
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 401) {
                            msg = "Tu calificación ya se ha enviado previamente.";
                        } else {

                            msg = (getString(R.string.algo_salio_mal));
                        }
                    } else {

                        msg = (getString(R.string.algo_salio_mal));
                    }
                    new SweetAlertDialog(RankAnswerActivityForum.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(msg)
                            .show();

                    customProgressDialog.dismissCustomProgressDialog();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();
                    params.put("ranking", String.valueOf(rankingAnswerSend.getRating()));
                    params.put("id_user", UserController.getUser().getIdUser());
                    params.put("id_answer", forumDiscussionModel.getIdAnswer());
                    Log.w("Mensaje", params.toString());

                    return params;

                }
            };

            MySingleton.getInstance(this).addToRequestQueue(stringRequest);
            customProgressDialog.showCustomProgressDialog();

        } else {

            new SweetAlertDialog(RankAnswerActivityForum.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops!")
                    .setContentText(getString(R.string.califica_answer))
                    .show();

        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
