package sereyd.com.soyeducadorasereyd.View.Sign;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.User;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.HandlerVolleyError;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class RestorePasswordActivity extends AppCompatActivity implements Validator.ValidationListener {

    @NotEmpty(messageResId = R.string.field_required)
    @Email(messageResId = R.string.email_required)
    @BindView(R.id.editTextUserEmail)
    EditText editTextUserEmail;
    @BindView(R.id.buttonRestore)
    Button buttonRestore;
    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restore_password);
        setCustomViews();
    }

    private void setCustomViews() {
        ButterKnife.bind(this);
        editTextUserEmail.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        buttonRestore.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @OnClick(R.id.buttonRestore)
    public void restorePassword(View view) {

        validator.validate();

    }

    @Override
    public void onValidationSucceeded() {

        final String email = editTextUserEmail.getText().toString();

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, Constants.URL_RESTORE_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                try {

                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getInt("error") == 0) {

                        new SweetAlertDialog(RestorePasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Contraseña restablecida")
                                .setContentText("Verifica tu email e ingresa la nueva contraseña.")
                                .show();

                    }else{
                        new SweetAlertDialog(RestorePasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops!")
                                .setContentText("Algo salió mal\nNo pudimos cambiar tu contraseña.")
                                .show();
                    }

                } catch (Exception e) {
                    new SweetAlertDialog(RestorePasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText("Algo salió mal")
                            .show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(RestorePasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(HandlerVolleyError.getErrorMessage(error, RestorePasswordActivity.this))
                        .show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                return params;
            }
        };

        MySingleton.getInstance(RestorePasswordActivity.this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {

            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
