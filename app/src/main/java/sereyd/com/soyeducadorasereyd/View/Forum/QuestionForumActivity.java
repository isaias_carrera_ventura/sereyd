package sereyd.com.soyeducadorasereyd.View.Forum;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.HasTypeface;

public class QuestionForumActivity extends Activity implements Validator.ValidationListener {

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextSubjectQuestion)
    EditText editTextQuestion;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextTitleQuestion)
    EditText editTextTitle;

    Validator validator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_forum);

        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

    }

    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));

    }

    @OnClick(R.id.buttonSend)
    public void sendQuestion() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_DISCUSSION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                new SweetAlertDialog(QuestionForumActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(getString(R.string.question_create))
                        .setContentText(getString(R.string.question_create_description))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                                QuestionForumActivity.this.finish();
                            }
                        })
                        .show();

                customProgressDialog.dismissCustomProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                new SweetAlertDialog(QuestionForumActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();

                customProgressDialog.dismissCustomProgressDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", UserController.getUser().getIdUser());
                params.put("title", editTextTitle.getText().toString());
                params.put("question", editTextQuestion.getText().toString());
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
