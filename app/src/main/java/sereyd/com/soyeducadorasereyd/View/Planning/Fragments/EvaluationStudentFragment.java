package sereyd.com.soyeducadorasereyd.View.Planning.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.DiagnoseController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Evaluation;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Models.TestModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.EvaluationActivity;
import sereyd.com.soyeducadorasereyd.View.Planning.Adapter.ResultDiagnoseAdapter;

public class EvaluationStudentFragment extends Fragment {

    @BindView(R.id.nameStudentFragmentEvaluation)
    TextView textViewNameStudent;
    @BindView(R.id.buttonRegisterResultEvaluation)
    Button buttonContinue;

    @BindView(R.id.recyclerViewQuestionEvaluation)
    RecyclerView recyclerViewQuestionsDiagnose;
    ResultDiagnoseAdapter mAdapter;

    int position = 0;
    Long idStudent;
    Student student;
    Long idEvaluation;
    Evaluation evaluation;
    Long idGroup;
    MyGroup myGroup;
    int numberOfStudents;
    List<TestModel> testModelList;

    public EvaluationStudentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_evaluation_student, container, false);
        ButterKnife.bind(this, view);
        buttonContinue.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewNameStudent.setTypeface(FontUtil.getRegularFont(getContext()));

        Bundle bundle = this.getArguments();
        position = bundle.getInt("position");
        idStudent = bundle.getLong("idStudent", -1);
        student = Student.findById(Student.class, idStudent);

        idEvaluation = bundle.getLong("evaluation", -1);
        evaluation = Evaluation.findById(Evaluation.class, idEvaluation);

        idGroup = bundle.getLong("group", -1);
        myGroup = MyGroup.findById(MyGroup.class, idGroup);

        testModelList = (List<TestModel>) bundle.getSerializable("test");

        mAdapter = new ResultDiagnoseAdapter(testModelList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewQuestionsDiagnose.setLayoutManager(mLayoutManager);
        recyclerViewQuestionsDiagnose.setItemAnimator(new DefaultItemAnimator());
        recyclerViewQuestionsDiagnose.setAdapter(mAdapter);
        textViewNameStudent.setText(student.getName() + " " + student.getLastName());

        return view;
    }

    @OnClick(R.id.buttonRegisterResultEvaluation)
    public void moveAndSaveButtonClicked(View view) {

        List<TestModel> questionObjects = mAdapter.getListResult();
        boolean complete = true;
        for (int i = 0; i < questionObjects.size(); i++) {
            if (questionObjects.get(i).getResult() == Constants.UNDEFINED) {
                complete = false;
            }
        }

        if (complete) {
            String studentId = student.getId_Student();
            int evaluationID = Integer.parseInt(evaluation.getEvaluationGroupId());

            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id_group_evaluation", evaluationID);
                jsonObject.put("id_student", studentId);

                JSONArray jsonArray = new JSONArray();

                for (int i = 0; i < questionObjects.size(); i++) {

                    TestModel testModel = questionObjects.get(i);

                    JSONObject jsonObjectQuestion = new JSONObject();
                    jsonObjectQuestion.put("question_evaluation", testModel.getIdQuestion());
                    jsonObjectQuestion.put("answer", testModel.getResult());
                    jsonArray.put(jsonObjectQuestion);

                }

                jsonObject.put("answers", jsonArray);

                final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getActivity());
                customProgressDialog.showCustomProgressDialog();

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.URL_ENDPOINT_QUESTION_EVALUATION_STUDENT, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        customProgressDialog.dismissCustomProgressDialog();

                        int responseAnswer = DiagnoseController.getDiagnoseResponse(response);
                        if (responseAnswer == 0) {

                            ((EvaluationActivity) getActivity()).selectPage(position + 1);

                            new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(getString(R.string.dialog_ok))
                                    .setContentText(getString(R.string.evaluation_complete))
                                    .setConfirmText(getString(R.string.dialog_ok))
                                    .show();

                        } else {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText(getString(R.string.ops))
                                    .setContentText(getString(R.string.algo_salio_mal))
                                    .setConfirmText(getString(R.string.dialog_ok))
                                    .show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        customProgressDialog.dismissCustomProgressDialog();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.algo_salio_mal))
                                .setConfirmText(getString(R.string.dialog_ok))
                                .show();

                        error.printStackTrace();

                    }
                });

                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);

            } catch (Exception e) {

                e.printStackTrace();

            }
        } else {
            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.evaluation_incomplete))
                    .setContentText(getString(R.string.complete_all_questions))
                    .setConfirmText(getString(R.string.dialog_ok))
                    .show();
        }
    }

}
