package sereyd.com.soyeducadorasereyd.View.GroupView.Homework;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.HomeworkController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Homework;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class AddHomeworkActivity extends Activity implements Validator.ValidationListener {

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextHomeworkName)
    EditText editTextHomeworkName;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextHomeworkSubject)
    EditText editTextHomeworkDescription;

    @BindView(R.id.buttonHomeworkRegister)
    Button button;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @Pattern(regex = "^((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])$")
    @BindView(R.id.editTextHomeworkDate)
    EditText editTextHomeworkDate;

    private Validator validator;
    private Calendar calendar;
    private int year, month, day;

    private Long idGroup;
    private MyGroup myGroup;

    private Long idToEdit;
    private int positonRecyclerToReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_homework);
        setCustomView();
        setCalendar();

        idGroup = getIntent().getExtras().getLong("group");
        myGroup = MyGroup.findById(MyGroup.class, idGroup);

        idToEdit = getIntent().getLongExtra("homeworkToEdit", -1);
        positonRecyclerToReturn = getIntent().getIntExtra("positionRecycler", -1);

        if (idToEdit != -1 && positonRecyclerToReturn != -1) {
            setViewDetail(Homework.findById(Homework.class, idToEdit));
        }
    }

    private void setViewDetail(Homework homework) {

        year = homework.getYear();
        month = homework.getMonth();
        day = homework.getDay();

        editTextHomeworkDate.setText(homework.getYear() + "/" + (homework.getMonth()) + "/" + homework.getDay());
        editTextHomeworkName.setText(homework.getName());
        editTextHomeworkDescription.setText(homework.getDescription());

    }

    private void setCalendar() {

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

    }

    private void setCustomView() {

        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        button.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextHomeworkName.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextHomeworkDescription.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextHomeworkDate.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

        editTextHomeworkDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                    setDate();
                }

                return true;
            }
        });

        editTextHomeworkDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    setDate();
                }
            }
        });

    }

    private void setDate() {
        DatePickerDialog dialog = new DatePickerDialog(this, R.style.datepicker, myDateListener, year, month, day);
        dialog.setTitle("");
        dialog.getDatePicker().setBackgroundColor(Color.WHITE);
        dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        dialog.show();
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {

                    // arg1 = year arg2 = month arg3 = day
                    String date = arg1 + "/" + (arg2 + 1) + "/" + arg3;
                    editTextHomeworkDate.setText(date);

                    year = arg1;
                    month = arg2;
                    day = arg3;

                }
            };

    @Override
    public void onValidationSucceeded() {

        Homework homework = new Homework(null, year, month, day, editTextHomeworkName.getText().toString(), editTextHomeworkDescription.getText().toString(), null);

        if (idToEdit != -1) {

            editHomework(homework, idToEdit);


        } else {

            homework.setMyGroup(myGroup);
            sendRequestToCreateHomework(homework);

        }


    }

    private void editHomework(final Homework homeworkAux, Long idToEdit) {

        Homework homeworkToEdit = Homework.findById(Homework.class, idToEdit);
        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, Constants.URL_ENDPOINT_HOMEWORK + homeworkToEdit.getIdHomework(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customProgressDialog.dismissCustomProgressDialog();

                if (HomeworkController.editResponseHomeworkFromServer(response)) {

                    new SweetAlertDialog(AddHomeworkActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.aviso))
                            .setContentText(getString(R.string.tarea_editada))
                            .setConfirmText(getString(R.string.dialog_ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .show();

                } else {

                    new SweetAlertDialog(AddHomeworkActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(AddHomeworkActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", homeworkAux.getName());
                params.put("description", homeworkAux.getDescription());
                params.put("deadline", homeworkAux.getYear() + "-" + (homeworkAux.getMonth() + 1) + "-" + homeworkAux.getDay());
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
            /*Intent returnIntent = new Intent();
            returnIntent.putExtra("homework", homework);
            returnIntent.putExtra("positionRecycler", positonRecyclerToReturn);
            setResult(Activity.RESULT_OK, returnIntent);*/
    }

    private void sendRequestToCreateHomework(final Homework homework) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_HOMEWORK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                customProgressDialog.dismissCustomProgressDialog();

                Homework homeworkToSave = HomeworkController.getHomeworkFromResponse(response);

                if (homeworkToSave != null) {

                    homeworkToSave.setMyGroup(homework.getMyGroup());
                    homeworkToSave.save();

                    new SweetAlertDialog(AddHomeworkActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.tarea_creada))
                            .setConfirmText(getString(R.string.dialog_ok))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .show();

                } else {

                    new SweetAlertDialog(AddHomeworkActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(AddHomeworkActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", homework.getName());
                params.put("description", homework.getDescription());
                params.put("deadline", homework.getYear() + "-" + (homework.getMonth() + 1) + "-" + homework.getDay());
                params.put("id_group", String.valueOf(myGroup.getIdGpr()));
                return params;
            }

        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    @OnClick(R.id.buttonHomeworkRegister)
    public void addAlarmButtonClicked(View view) {
        validator.validate();
    }
}
