package sereyd.com.soyeducadorasereyd.View.Planning.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.Controllers.EvaluationController;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.Controllers.ResultController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Evaluation;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.ResultModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Adapter.GraphEvaluationAdapter;

public class GraphEvaluationActivity extends AppCompatActivity {

    @BindView(R.id.graficasTitleView)
    TextView textViewTitleView;
    @BindView(R.id.toolbarGraficas)
    Toolbar toolbar;

    @BindView(R.id.recyclerViewGraphsEva)
    RecyclerView recyclerGraphs;

    private List<Evaluation> evaluations;
    private List<ResultModel> resultModels = new ArrayList<>();
    private Long idGroup;
    private GraphEvaluationAdapter graphEvaluationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_evaluation);

        idGroup = getIntent().getExtras().getLong("group");
        MyGroup myGroup = GroupController.getGroupById(idGroup);
        evaluations = EvaluationController.getEvaluationForGroup(myGroup);
        setupView();

        for (int i = 0; i < evaluations.size(); i++) {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(GraphEvaluationActivity.this);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_TEST_EVALUATION_FULL + evaluations.get(i).getEvaluationGroupId(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    ResultModel resultModel = ResultController.getResultsFromResponseEvaluation(response);
                    if (resultModel != null) {
                        resultModels.add(resultModel);
                        graphEvaluationAdapter.notifyDataSetChanged();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    customProgressDialog.dismissCustomProgressDialog();
                    error.printStackTrace();

                }
            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MySingleton.getInstance(this).addToRequestQueue(stringRequest);
            customProgressDialog.showCustomProgressDialog();

        }

    }

    private void setupView() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textViewTitleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        graphEvaluationAdapter = new GraphEvaluationAdapter(GraphEvaluationActivity.this, this, resultModels, evaluations);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerGraphs.setLayoutManager(mLayoutManager);
        recyclerGraphs.setItemAnimator(new DefaultItemAnimator());
        recyclerGraphs.setAdapter(graphEvaluationAdapter);
    }


}