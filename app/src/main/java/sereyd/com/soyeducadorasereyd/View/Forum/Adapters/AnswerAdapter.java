package sereyd.com.soyeducadorasereyd.View.Forum.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.ForumDiscussionModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.View.Forum.RankAnswerActivityForum;

/**
 * Created by chaycv on 18/12/17.
 */

public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.MyViewHolder> {

    private List<ForumDiscussionModel> forumDiscussionModels;
    private Context context;

    public AnswerAdapter(List<ForumDiscussionModel> diaryList, Context context) {
        this.forumDiscussionModels = diaryList;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public AnswerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_answer, parent, false);

        return new AnswerAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final AnswerAdapter.MyViewHolder holder, final int position) {

        ImageLoader mImageLoader = MySingleton.getInstance(getContext()).getImageLoader();
        final String url = forumDiscussionModels.get(position).getImage();
        mImageLoader.get(url, ImageLoader.getImageListener(holder.imageViewProfile, R.mipmap.person_gray, R.mipmap.person_gray));

        holder.rankingAnswer.setProgress(forumDiscussionModels.get(position).getRanking());
        holder.imageViewProfile.setImageUrl(url, mImageLoader);
        holder.answerTextView.setText(forumDiscussionModels.get(position).getAnswer());
        holder.answerDate.setText(forumDiscussionModels.get(position).getDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), RankAnswerActivityForum.class);
                intent.putExtra("answer", forumDiscussionModels.get(position));
                getContext().startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return forumDiscussionModels.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.answer)
        TextView answerTextView;
        @BindView(R.id.imageViewProfile)
        NetworkImageView imageViewProfile;
        @BindView(R.id.answerDate)
        TextView answerDate;
        @BindView(R.id.rankingAnswer)
        MaterialRatingBar rankingAnswer;

        MyViewHolder(View view) {

            super(view);
            ButterKnife.bind(this, view);
            rankingAnswer.setEnabled(false);
            rankingAnswer.setMax(5);

        }

    }
}