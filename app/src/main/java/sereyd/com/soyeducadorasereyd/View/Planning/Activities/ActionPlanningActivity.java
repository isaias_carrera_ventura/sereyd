package sereyd.com.soyeducadorasereyd.View.Planning.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;

public class ActionPlanningActivity extends Activity {

    @BindView(R.id.textViewSharePlanning)
    TextView textViewSharePlanning;
    @BindView(R.id.textViewCancelPlanning)
    TextView textViewCancelPlanning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_planning);

        setupView();

    }

    private void setupView() {

        ButterKnife.bind(this);
        textViewSharePlanning.setTypeface(FontUtil.getRegularFont(this));
        textViewCancelPlanning.setTypeface(FontUtil.getRegularFont(this));

    }

    @OnClick(R.id.cancelView)
    public void cancelView(View view) {

        this.finish();

    }
    @OnClick(R.id.sharePlanning)
    public void sharePlanning(View view) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", Constants.SHARE_OPTION);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();

    }


}
