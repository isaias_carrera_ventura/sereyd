package sereyd.com.soyeducadorasereyd.View.Forum;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import sereyd.com.soyeducadorasereyd.Controllers.ForumController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.ForumDiscussionModel;
import sereyd.com.soyeducadorasereyd.Models.ForumQuestionModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.View.Forum.Adapters.AnswerAdapter;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailQuestionForumActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.recyclerViewAnswer)
    RecyclerView recyclerViewAnswer;
    @BindView(R.id.titleQuestion)
    TextView titleQuestion;
    @BindView(R.id.contentQuestion)
    TextView contentQuestion;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextDiarySubject)
    EditText ediTextDiscussion;

    AnswerAdapter mAdapter;
    ArrayList<ForumDiscussionModel> forumDiscussionModelList;

    Validator validator;
    ForumQuestionModel forumQuestionModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_question_forum);

        ButterKnife.bind(this);
        forumQuestionModel = (ForumQuestionModel) getIntent().getExtras().get("question");
        setupView();

        validator = new Validator(this);
        validator.setValidationListener(this);

    }

    @Override
    protected void onResume() {

        super.onResume();
        getDiscussion();

    }

    private void getDiscussion() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_DISCUSSION + "/" + forumQuestionModel.getIdDiscussion(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                List<ForumDiscussionModel> forumQuestionModels = ForumController.getDiscussionAnswerList(response);
                if (forumQuestionModels != null) {

                    forumDiscussionModelList.clear();
                    forumDiscussionModelList.addAll(forumQuestionModels);
                    mAdapter.notifyDataSetChanged();

                } else {

                    new SweetAlertDialog(DetailQuestionForumActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }

                customProgressDialog.dismissCustomProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(DetailQuestionForumActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();

    }

    private void setupView() {


        forumDiscussionModelList = new ArrayList<>();
        mAdapter = new AnswerAdapter(forumDiscussionModelList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewAnswer.setLayoutManager(mLayoutManager);
        recyclerViewAnswer.setItemAnimator(new DefaultItemAnimator());
        recyclerViewAnswer.setAdapter(mAdapter);

        titleQuestion.setText(forumQuestionModel.getTitle());
        contentQuestion.setText(forumQuestionModel.getQuestion());



    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick(R.id.buttonSendDiscussion)
    public void sendDiscussion() {

        validator.validate();

    }

    @Override
    public void onValidationSucceeded() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_ANSWER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(DetailQuestionForumActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(getString(R.string.question_create))
                        .setContentText(getString(R.string.question_create_description))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                ediTextDiscussion.getText().clear();
                                sweetAlertDialog.dismissWithAnimation();
                                getDiscussion();
                            }
                        })
                        .show();


                customProgressDialog.dismissCustomProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                new SweetAlertDialog(DetailQuestionForumActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();

                customProgressDialog.dismissCustomProgressDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("answer", ediTextDiscussion.getText().toString());
                params.put("id_user", UserController.getUser().getIdUser());
                params.put("id_discussion", forumQuestionModel.getIdDiscussion());
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
