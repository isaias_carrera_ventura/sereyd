package sereyd.com.soyeducadorasereyd.View.Planning.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.Controllers.PlanningController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Planning;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.CircleNetworkImageView;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.PlanningDetailActivity;

/**
 * Created by isaiascarreraventura on 11/02/17.
 */

public class PlanningAdapter extends RecyclerView.Adapter<PlanningAdapter.RecyclerViewHolders> implements Filterable {

    private List<Planning> mArrayList;
    private List<Planning> mFilteredList;
    private Context context;

    public PlanningAdapter(Context context, List<Planning> itemList) {
        this.mArrayList = itemList;
        this.mFilteredList = mArrayList;
        this.context = context;
    }

    @Override
    public PlanningAdapter.RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.item_planning, parent, false);
        return new RecyclerViewHolders(contactView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {

        holder.name.setText(mFilteredList.get(position).getName());
        holder.description.setText(mFilteredList.get(position).getDescription());
        holder.textViewField.setText(mFilteredList.get(position).getField());

        ImageLoader mImageLoader = MySingleton.getInstance(getContext()).getImageLoader();
        final String url = mFilteredList.get(position).getUrlImage();
        mImageLoader.get(url, ImageLoader.getImageListener(holder.imageView, R.mipmap.ic_launcher, R.mipmap.ic_launcher));
        holder.imageView.setImageUrl(url, mImageLoader);

        final int positionAux = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getContext(), PlanningDetailActivity.class);
                i.putExtra("planning", mFilteredList.get(positionAux));
                getContext().startActivity(i);

            }
        });

        holder.imageViewIndicator.setVisibility(PlanningController.isPlanningInDatabase(mFilteredList.get(position)) ? View.VISIBLE : View.INVISIBLE);
        holder.planningPremium.setVisibility(mFilteredList.get(position).getType() == Constants.FREE ? View.VISIBLE : View.GONE);

    }

    @Override
    public int getItemCount() {
        return this.mFilteredList.size();
    }

    public Context getContext() {
        return context;
    }

    class RecyclerViewHolders extends RecyclerView.ViewHolder {

        @BindView(R.id.titlePlanning)
        TextView name;

        @BindView(R.id.descriptionPlanning)
        TextView description;

        @BindView(R.id.imagePlanning)
        NetworkImageView imageView;

        @BindView(R.id.planning_download_indicator)
        ImageView imageViewIndicator;

        @BindView(R.id.planning_premium)
        RelativeLayout planningPremium;

        @BindView(R.id.fieldPlanning)
        TextView textViewField;

        public RecyclerViewHolders(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
            name.setTypeface(FontUtil.getBoldFont(getContext()));
            description.setTypeface(FontUtil.getRegularFont(getContext()));
            textViewField.setTypeface(FontUtil.getHairlineFont(getContext()));
        }

    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<Planning> filteredList = new ArrayList<>();

                    for (Planning businessModel : mArrayList) {

                        if (businessModel.getName().toLowerCase().contains(charString)
                                || businessModel.getName().toLowerCase().contains(charString)
                                || businessModel.getField().toLowerCase().contains(charString)) {
                            filteredList.add(businessModel);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Planning>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


}