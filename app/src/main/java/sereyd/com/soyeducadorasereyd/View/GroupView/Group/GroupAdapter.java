package sereyd.com.soyeducadorasereyd.View.GroupView.Group;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.GenericClass.AlarmReceiver;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.AlertReminder;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.GroupView.StudentV.GroupDetailOptionsActivity;

/**
 * Created by isaiascarreraventura on 26/12/16.
 */
public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {

    private List<MyGroup> groupsList;

    private Context contextAdapter;

    public GroupAdapter(Context context, List<MyGroup> groups) {
        groupsList = groups;
        contextAdapter = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return contextAdapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_group, parent, false);

        // Return a new holder instance
        return new ViewHolder(contactView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // Get the data model based on position
        MyGroup myGroup = groupsList.get(position);

        // Set item views based on your views and data model
        TextView textViewNameGroup = holder.nameTextView;
        TextView textViewAsistenciasTitle = holder.textViewAsistenciaTitle;
        TextView avisos = holder.avisosJuntasLabel;
        TextView textViewAsistenciaDetail = holder.textViewAsitenciaProgress;
        TextView textViewAvisosRecycler = holder.textViewAvisosRecycler;
        TextView textViewProductividadTitle = holder.textViewProductivitadTitle;
        TextView textViewProductividadDetaill = holder.textViewProductividadProgress;
        TextView textViewNinio = holder.textViewNinio;
        TextView textViewNinioCounter = holder.textViewNinioCounter;
        TextView textViewNinias = holder.textViewNinias;
        TextView textViewNiniaCounter = holder.textViewNiniaCounter;
        TextView textViewTotalStdnt = holder.textViewTotalStdnt;
        TextView textViewTotalStdntCounter = holder.textViewTotalStdntCounter;

        textViewNinio.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewNinioCounter.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewNinias.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewNiniaCounter.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewTotalStdnt.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewTotalStdntCounter.setTypeface(FontUtil.getRegularFont(getContext()));

        textViewAvisosRecycler.setTypeface(FontUtil.getHairlineFont(getContext()));
        textViewAsistenciaDetail.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewAsistenciasTitle.setTypeface(FontUtil.getRegularFont(getContext()));
        avisos.setTypeface(FontUtil.getBoldFont(getContext()));
        textViewNameGroup.setTypeface(FontUtil.getBoldFont(getContext()));
        textViewProductividadTitle.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewProductividadDetaill.setTypeface(FontUtil.getRegularFont(getContext()));

        textViewNameGroup.setText(myGroup.getGrade() + "" + myGroup.getGroupLetter() + " / " + myGroup.getSchool());

        //GET ALERTS
        AlertReminder alertReminder = AlertReminder.getFirstAlerReminder(groupsList.get(position).getId());
        if (alertReminder == null)
            textViewAvisosRecycler.setText(R.string.no_avisos);
        else
            textViewAvisosRecycler.setText(alertReminder.getSubject());

        //GET COUNTER FOR STUDENTS
        int[] counter = GroupController.counterStudentsGender(myGroup.getId());
        textViewTotalStdntCounter.setText(counter[2] + "");
        textViewNiniaCounter.setText(counter[1] + "");
        textViewNinioCounter.setText(counter[0] + "");

        holder.progressBarAsistencia.setProgress((int) myGroup.getAttendancePercent());
        holder.textViewAsitenciaProgress.setText((int) myGroup.getAttendancePercent() + "%");

    }

    @Override
    public int getItemCount() {
        return groupsList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.contact_name)
        TextView nameTextView;
        @BindView(R.id.textViewAvisos)
        TextView avisosJuntasLabel;
        @BindView(R.id.buttonRemoveGroup)
        Button buttonRemove;
        @BindView(R.id.buttonEditGroup)
        Button buttonEdit;
        @BindView(R.id.textViewAsistencia)
        TextView textViewAsistenciaTitle;
        @BindView(R.id.textViewProgressAsistencia)
        TextView textViewAsitenciaProgress;
        @BindView(R.id.textViewAvisosRecycler)
        TextView textViewAvisosRecycler;
        @BindView(R.id.textViewProductivity)
        TextView textViewProductivitadTitle;
        @BindView(R.id.textViewProgressProductivity)
        TextView textViewProductividadProgress;

        @BindView(R.id.textViewNinio)
        TextView textViewNinio;
        @BindView(R.id.textViewNinioCounter)
        TextView textViewNinioCounter;

        @BindView(R.id.textViewNinias)
        TextView textViewNinias;
        @BindView(R.id.textViewNiniaCounter)
        TextView textViewNiniaCounter;

        @BindView(R.id.textViewTotalStdnt)
        TextView textViewTotalStdnt;
        @BindView(R.id.textViewTotalStdntCounter)
        TextView textViewTotalStdntCounter;

        @BindView(R.id.progressBarAsistencia)
        ProgressBar progressBarAsistencia;


        public ViewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @OnClick(R.id.buttonRemoveGroup)
        public void removeGroupButtonClicked(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                removeGroup(position);
            }
        }

        @OnClick(R.id.buttonEditGroup)
        public void editGroupButtonClicked(View view) {

            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {

                Intent intent = new Intent(getContext(), AddGroupActivity.class);
                intent.putExtra("groupToEdit", groupsList.get(position).getId());
                intent.putExtra("positionRecycler", position);
                ((Activity) getContext()).startActivityForResult(intent, Constants.EDIT_ACTIVITY_RESULT);

            }

        }

        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                selectGroup(position);
            }
        }

        private void removeGroup(final int position) {

            new SweetAlertDialog(contextAdapter, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getContext().getResources().getString(R.string.aviso))
                    .setContentText(getContext().getResources().getString(R.string.pregunta_grupo))
                    .setConfirmText(getContext().getResources().getString(R.string.dialog_ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            deleteGroup(position);
                        }
                    })
                    .show();

        }

        private void deleteGroup(final int position) {

            final CustomProgressDialog customProgressDialog;
            customProgressDialog = new CustomProgressDialog(getContext());
            customProgressDialog.showCustomProgressDialog();

            String url = Constants.URL_ENDPOINT_GROUP + groupsList.get(position).getIdGpr();
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {


                    customProgressDialog.dismissCustomProgressDialog();
                    if (GroupController.deleteGroupResponse(response)) {

                        removeAlertInGroup(groupsList.get(position));
                        groupsList.get(position).delete();
                        groupsList.remove(position);
                        notifyItemRemoved(position);

                        new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                                .setContentText(getContext().getString(R.string.grupo_eliminado))
                                .setTitleText(getContext().getString(R.string.aviso))
                                .show();

                    } else {

                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getContext().getString(R.string.ops))
                                .setContentText(getContext().getString(R.string.algo_salio_mal))
                                .show();

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getContext().getString(R.string.ops))
                            .setContentText(getContext().getString(R.string.algo_salio_mal))
                            .show();

                }
            });

            MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);


        }

        private void removeAlertInGroup(MyGroup myGroup) {

            List<AlertReminder> list = AlertReminder.getAlerts(myGroup.getId());//myGroup.getAlerts(myGroup.getId());
            for (AlertReminder alert : list) {
                int idAlert = alert.getId().intValue();
                Intent alarmIntent = new Intent(getContext(), AlarmReceiver.class);
                alarmIntent.putExtra("idAlarmToTrigger", idAlert);
                PendingIntent pi = PendingIntent.getBroadcast(getContext(), idAlert, alarmIntent, 0);
                AlarmManager manager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
                manager.cancel(pi);
                pi.cancel();
            }

        }


        private void selectGroup(int position) {

            MyGroup group = groupsList.get(position);
            Intent intent = new Intent(contextAdapter, GroupDetailOptionsActivity.class);
            intent.putExtra("group", group.getId());
            getContext().startActivity(intent);

        }

    }


}
