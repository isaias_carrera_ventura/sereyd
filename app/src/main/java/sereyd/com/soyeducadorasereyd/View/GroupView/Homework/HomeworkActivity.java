package sereyd.com.soyeducadorasereyd.View.GroupView.Homework;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.Controllers.HomeworkController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Homework;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class HomeworkActivity extends AppCompatActivity {

    @BindView(R.id.homeworkRecycleView)
    RecyclerView homeworkRecycler;
    @BindView(R.id.toolbarHomework)
    Toolbar toolbar;
    @BindView(R.id.misTareas)
    TextView textViewTitleView;
    MyGroup myGroup;
    Long idGroup;
    List<Homework> homeworkList;
    HomeworkAdapter homeworkAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_homework);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }

        textViewTitleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

        idGroup = getIntent().getExtras().getLong("group");
        myGroup = MyGroup.findById(MyGroup.class, idGroup);

        homeworkList = HomeworkController.getHomeworks(idGroup);
        homeworkAdapter = new HomeworkAdapter(this, homeworkList);
        homeworkRecycler.setAdapter(homeworkAdapter);
        homeworkRecycler.setLayoutManager(new LinearLayoutManager(this));


    }

    @Override
    protected void onResume() {
        super.onResume();
        requestForHomeworkInServer(myGroup);
    }

    private void requestForHomeworkInServer(MyGroup myGroup) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_GROUP + myGroup.getIdGpr(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                List<Homework> homeworkListFromServer = HomeworkController.getHomeworkListFromResponse(response);
                if (homeworkListFromServer != null) {

                    homeworkList.clear();
                    HomeworkController.saveListHomeworkInDatabase(homeworkListFromServer, idGroup);
                    homeworkList.addAll(HomeworkController.getHomeworks(idGroup));
                    homeworkAdapter.notifyDataSetChanged();


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == Constants.EDIT_ACTIVITY_RESULT) {

                int positionRecyclerChanged = data.getIntExtra("positionRecycler", -1);
                Homework homework = (Homework) data.getSerializableExtra("homework");
                Homework homeworkInDisk = homeworkList.get(positionRecyclerChanged);

                homeworkInDisk.setDescription(homework.getDescription());
                homeworkInDisk.setName(homework.getName());
                homeworkInDisk.setDay(homework.getDay());
                homeworkInDisk.setMonth(homework.getMonth());
                homeworkInDisk.setYear(homework.getYear());
                homeworkInDisk.save();
                Toast.makeText(getApplicationContext(), R.string.tarea_editada, Toast.LENGTH_SHORT).show();
                homeworkList.set(positionRecyclerChanged, homeworkInDisk);
                homeworkAdapter.notifyDataSetChanged();

            }
        }
    }

}
