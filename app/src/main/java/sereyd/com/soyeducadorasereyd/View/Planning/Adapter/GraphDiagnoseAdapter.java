package sereyd.com.soyeducadorasereyd.View.Planning.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.snatik.storage.Storage;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.DiagnoseController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Diagnose;
import sereyd.com.soyeducadorasereyd.Models.ResultModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.InputStreamVolleyRequest;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

/**
 * Created by isaiascarreraventura on 08/03/17.
 * Project SoyeducadoraSEREYD
 */

public class GraphDiagnoseAdapter extends RecyclerView.Adapter<GraphDiagnoseAdapter.MyViewHolder> {

    private List<List<ResultModel>> resultList;
    private Context context;
    private List<Diagnose> diagnoses;
    Activity activity;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public GraphDiagnoseAdapter(Activity activity, Context context, ArrayList<List<ResultModel>> diagnoseList, ArrayList<Diagnose> diagnoses) {

        this.activity = activity;
        this.resultList = diagnoseList;
        this.diagnoses = diagnoses;
        this.context = context;

    }

    public Context getContext() {
        return this.context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_graph_diagnose, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final GraphDiagnoseAdapter.MyViewHolder holder, int position) {

        if (position != RecyclerView.NO_POSITION) {

            holder.textTitle.setText(diagnoses.get(position).getDate());
            holder.progressBar1.setProgress(resultList.get(position).get(0).getPercent());
            holder.textView1_1.setText(resultList.get(position).get(0).getPercent() + "%");

            holder.progressBar2.setProgress(resultList.get(position).get(1).getPercent());
            holder.textView2_1.setText(resultList.get(position).get(1).getPercent() + "%");

            holder.progressBar3.setProgress(resultList.get(position).get(2).getPercent());
            holder.textView3_1.setText(resultList.get(position).get(2).getPercent() + "%");

            holder.progressBar4.setProgress(resultList.get(position).get(3).getPercent());
            holder.textView4_1.setText(resultList.get(position).get(3).getPercent() + "%");

            holder.progressBar5.setProgress(resultList.get(position).get(4).getPercent());
            holder.textView5_1.setText(resultList.get(position).get(4).getPercent() + "%");

            holder.progressBar6.setProgress(resultList.get(position).get(5).getPercent());
            holder.textView6_1.setText(resultList.get(position).get(5).getPercent() + "%");

            holder.buttonExportDiagnose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // Check if we have write permission
                    int permission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

                    if (permission != PackageManager.PERMISSION_GRANTED) {

                        // We don't have permission so prompt the user
                        ActivityCompat.requestPermissions(
                                activity,
                                PERMISSIONS_STORAGE,
                                REQUEST_EXTERNAL_STORAGE);

                    } else {

                        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Exportar diagnóstico")
                                .setContentText("Se exportará tu diagnóstico a Excel")
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        requestForDownloadDiagnoseFile(holder.getAdapterPosition());
                                    }
                                })
                                .setCancelText("Cancelar")
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();


                    }



                }
            });

        }

    }

    private void requestForDownloadDiagnoseFile(final int position) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                        activity,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );


            } else {

                new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getContext().getResources().getString(R.string.aviso))
                        .setContentText(getContext().getResources().getString(R.string.access_files))
                        .setConfirmText("Si").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", activity.getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);


                    }
                }).show();


            }

        } else {


            final Storage storage = new Storage(getContext());
            final String path = storage.getExternalStorageDirectory();
            final String newDir = path + File.separator + "diagnoses";
            storage.createDirectory(newDir);

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getContext());
            customProgressDialog.showCustomProgressDialog();

            final String url = Constants.URL_DIAGNOSE_FILE + diagnoses.get(position).getIdGroupDiagnose();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    try {

                        JSONObject jsonObject = new JSONObject(response);

                        if (jsonObject.getInt("error") == 0) {

                            final CustomProgressDialog customProgressDialog2 = new CustomProgressDialog(getContext());
                            customProgressDialog2.showCustomProgressDialog();
                            String urlFile = Constants.URL_SERVER + "/" + jsonObject.getString("path");
                            //DOWNLOAD File
                            InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, urlFile,
                                    new Response.Listener<byte[]>() {
                                        @Override
                                        public void onResponse(byte[] response) {

                                            customProgressDialog2.dismissCustomProgressDialog();
                                            try {
                                                if (response != null) {

                                                    storage.createFile(DiagnoseController.getFileExcelPathForDiagnose(newDir, diagnoses.get(position)), response);
                                                    //Attach file
                                                    String filePathExcel = DiagnoseController.getFileAbsoluteFileExcelPathForDiagnose(diagnoses.get(position), getContext());
                                                    File fileExcel = new File(filePathExcel);
                                                    Uri pathExcel = Uri.fromFile(fileExcel);

                                                    //Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                                    // set the type to 'email'
                                                    emailIntent.setType("vnd.android.cursor.dir/email");
                                                    String to[] = {};
                                                    emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                                                    // the attachment
                                                    emailIntent.putExtra(Intent.EXTRA_STREAM, pathExcel);

                                                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Diagnóstico");
                                                    context.startActivity(Intent.createChooser(emailIntent, "Enviar diagnóstico vía e-mail..."));

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                                        .setTitleText(getContext().getResources().getString(R.string.ops))
                                                        .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                                                        .show();
                                            }
                                        }
                                    }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    error.printStackTrace();
                                    customProgressDialog2.dismissCustomProgressDialog();
                                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(getContext().getResources().getString(R.string.ops))
                                            .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                                            .show();
                                }
                            }, null);

                            request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            MySingleton.getInstance(getContext()).addToRequestQueue(request);


                        } else {
                            new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText(getContext().getResources().getString(R.string.ops))
                                    .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                                    .show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getContext().getResources().getString(R.string.ops))
                                .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                                .show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getContext().getResources().getString(R.string.ops))
                            .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                            .show();
                }
            });

            MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);

        }
    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView textTitle;
        @BindView(R.id.textView1)
        TextView textView1;
        @BindView(R.id.textView1_1)
        TextView textView1_1;
        @BindView(R.id.textView2)
        TextView textView2;
        @BindView(R.id.textView2_1)
        TextView textView2_1;
        @BindView(R.id.textView3)
        TextView textView3;
        @BindView(R.id.textView3_1)
        TextView textView3_1;
        @BindView(R.id.textView4)
        TextView textView4;
        @BindView(R.id.textView4_1)
        TextView textView4_1;
        @BindView(R.id.textView5)
        TextView textView5;
        @BindView(R.id.textView5_1)
        TextView textView5_1;
        @BindView(R.id.textView6_1)
        TextView textView6_1;

        @BindView(R.id.progressBar1)
        ProgressBar progressBar1;

        @BindView(R.id.progressBar2)
        ProgressBar progressBar2;

        @BindView(R.id.progressBar3)
        ProgressBar progressBar3;

        @BindView(R.id.progressBar4)
        ProgressBar progressBar4;

        @BindView(R.id.progressBar5)
        ProgressBar progressBar5;

        @BindView(R.id.progressBar6)
        ProgressBar progressBar6;

        @BindView(R.id.buttonExportDiagnose)
        Button buttonExportDiagnose;

        public MyViewHolder(View view) {

            super(view);
            ButterKnife.bind(this, view);

            textTitle.setTypeface(FontUtil.getRegularFont(getContext()));
            textView1.setTypeface(FontUtil.getRegularFont(getContext()));
            textView1_1.setTypeface(FontUtil.getRegularFont(getContext()));
            textView2.setTypeface(FontUtil.getRegularFont(getContext()));
            textView2_1.setTypeface(FontUtil.getRegularFont(getContext()));
            textView3.setTypeface(FontUtil.getRegularFont(getContext()));
            textView3_1.setTypeface(FontUtil.getRegularFont(getContext()));
            textView4.setTypeface(FontUtil.getRegularFont(getContext()));
            textView4_1.setTypeface(FontUtil.getRegularFont(getContext()));
            textView5.setTypeface(FontUtil.getRegularFont(getContext()));
            textView5_1.setTypeface(FontUtil.getRegularFont(getContext()));
            textView6_1.setTypeface(FontUtil.getRegularFont(getContext()));

        }

    }

}
