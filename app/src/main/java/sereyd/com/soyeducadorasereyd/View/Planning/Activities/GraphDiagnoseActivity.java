package sereyd.com.soyeducadorasereyd.View.Planning.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.Controllers.DiagnoseController;
import sereyd.com.soyeducadorasereyd.Controllers.ResultController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Diagnose;
import sereyd.com.soyeducadorasereyd.Models.ResultModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Adapter.GraphDiagnoseAdapter;

public class GraphDiagnoseActivity extends AppCompatActivity {

    @BindView(R.id.graficasTitleView)
    TextView textViewTitleView;
    @BindView(R.id.toolbarGraficas)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewGraphs)
    RecyclerView recyclerGraphs;

    Long idGroup;
    ArrayList<Diagnose> diagnoses = new ArrayList<>();
    ArrayList<List<ResultModel>> listResult = new ArrayList<>();
    GraphDiagnoseAdapter graphDiagnoseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_diagnose);

        idGroup = getIntent().getExtras().getLong("group");
        diagnoses = DiagnoseController.getDiagnoseByGroup(idGroup);

        setupView();

        for (int i = 0; i < diagnoses.size(); i++) {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(GraphDiagnoseActivity.this);

                    StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_TEST_FULL + diagnoses.get(i).getIdGroupDiagnose(), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            customProgressDialog.dismissCustomProgressDialog();
                            List<ResultModel> list = ResultController.getResultsFromResponseDiagnostic(response);
                            if (list != null) {

                                listResult.add(list);
                                graphDiagnoseAdapter.notifyDataSetChanged();

                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            customProgressDialog.dismissCustomProgressDialog();
                            error.printStackTrace();

                }
            });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MySingleton.getInstance(this).addToRequestQueue(stringRequest);
            customProgressDialog.showCustomProgressDialog();

        }

    }

    private void setupView() {

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textViewTitleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

        graphDiagnoseAdapter = new GraphDiagnoseAdapter(GraphDiagnoseActivity.this, this, listResult, diagnoses);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerGraphs.setLayoutManager(mLayoutManager);
        recyclerGraphs.setItemAnimator(new DefaultItemAnimator());
        recyclerGraphs.setAdapter(graphDiagnoseAdapter);

    }

}
