package sereyd.com.soyeducadorasereyd.View;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.PaymentController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.InicioActivity;
import sereyd.com.soyeducadorasereyd.Models.PaymentInformation;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.HandlerVolleyError;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PaymentInformationActivity extends AppCompatActivity {

    @BindView(R.id.paymentDescriptionLabel)
    TextView textViewDescriptionStatus;
    @BindView(R.id.textViewDescriptionPaymentLabel)
    TextView textViewDescriptionPaymentLabel;
    @BindView(R.id.textViewDatePayment)
    TextView textViewDatePayment;
    @BindView(R.id.holderLabel)
    TextView holderLabel;
    @BindView(R.id.cardLabel)
    TextView cardLabel;
    @BindView(R.id.amount)
    TextView amountLabel;
    @BindView(R.id.textViewDateTest)
    TextView textViewDateTest;

    @BindView(R.id.testPeriodLayout)
    LinearLayout testPeriodLayout;
    @BindView(R.id.layoutPayPalInformation)
    LinearLayout layoutPayPalInformation;
    @BindView(R.id.layoutCardInformation)
    LinearLayout layoutCardInformation;
    @BindView(R.id.paymentInformationLayout)
    LinearLayout paymentInformationLayout;
    private PaymentInformation paymentInformation;

    @BindView(R.id.buttonCancelSuscription)
    Button buttonCancelSuscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_information);

        ButterKnife.bind(this);

        getPaymentInformation();


    }

    private void getPaymentInformation() {
        //PAYMENT INFORMATION

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        String urlPayment = Constants.URL_ENDPOINT_PAYMENT + UserController.getUser().getIdUser();
        StringRequest stringRequestPayment = new StringRequest(Request.Method.GET, urlPayment, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                paymentInformation = PaymentController.getPaymentObject(response);

                if (paymentInformation != null) {

                    textViewDescriptionPaymentLabel.setText(paymentInformation.getDescription());

                    if (paymentInformation.getCode() == Constants.ACTIVE_SUSCRIPTION) {

                        paymentInformationLayout.setVisibility(View.VISIBLE);
                        testPeriodLayout.setVisibility(View.GONE);

                        textViewDescriptionStatus.setText(paymentInformation.getDescriptionCard());
                        textViewDatePayment.setText("Fecha: " + paymentInformation.getInitDate() + " a " + paymentInformation.getFinishDate());
                        buttonCancelSuscription.setVisibility(View.VISIBLE);

                        if (paymentInformation.getCardNumber() != null) {

                            holderLabel.setText(paymentInformation.getHolderName());
                            cardLabel.setText(paymentInformation.getCardNumber());
                            amountLabel.setText("$ " + paymentInformation.getAmount() + " MXN");

                            layoutPayPalInformation.setVisibility(View.GONE);
                            layoutCardInformation.setVisibility(View.VISIBLE);

                        } else {

                            if (paymentInformation.getCancelled() == Constants.CANCELLED) {

                                textViewDescriptionPaymentLabel.setText("Sin suscripción");

                                buttonCancelSuscription.setVisibility(View.GONE);
                                layoutPayPalInformation.setVisibility(View.GONE);
                                layoutCardInformation.setVisibility(View.GONE);


                            } else {

                                //SUSCRIPCION CON PAYPAL
                                layoutPayPalInformation.setVisibility(View.VISIBLE);
                                layoutCardInformation.setVisibility(View.GONE);

                            }

                        }

                    } else if (paymentInformation.getCode() == Constants.TEST_PERIOD) {

                        testPeriodLayout.setVisibility(View.VISIBLE);
                        paymentInformationLayout.setVisibility(View.GONE);

                        textViewDateTest.setText("Tu período de prueba finaliza \n" + paymentInformation.getInitDate() + " a " + paymentInformation.getFinishDate());

                    } else if (paymentInformation.getCode() == Constants.PAYMENT_REQUIRED) {

                        testPeriodLayout.setVisibility(View.VISIBLE);
                        paymentInformationLayout.setVisibility(View.GONE);
                        textViewDateTest.setText("Suscríbete para poder disfrutar de muchos beneficios.");

                    }

                } else {

                    new SweetAlertDialog(PaymentInformationActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Oops")
                            .setContentText("Algo salió mal")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismissWithAnimation();
                                    PaymentInformationActivity.this.finish();

                                }
                            })
                            .show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                new SweetAlertDialog(PaymentInformationActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Oops")
                        .setContentText("Algo salió mal")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                sweetAlertDialog.dismissWithAnimation();
                                PaymentInformationActivity.this.finish();

                            }
                        })
                        .show();

                customProgressDialog.dismissCustomProgressDialog();

            }
        });


        MySingleton.getInstance(PaymentInformationActivity.this).addToRequestQueue(stringRequestPayment);
        customProgressDialog.showCustomProgressDialog();
    }


    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));

    }

    @OnClick(R.id.buttonSuscription)
    public void suscriptionSereyd() {

        startActivity(new Intent(this, PaymentActivitySereyd.class));

    }

    @OnClick(R.id.buttonCancelSuscription)
    public void cancelSuscription() {

        new SweetAlertDialog(PaymentInformationActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Cancelar suscripción")
                .setContentText("¿Estás seguro de cancelar la suscripción?")
                .setConfirmText("Cancelar suscripción")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();
                        requestForCancelSuscription();

                    }
                })
                .show();

    }

    private void requestForCancelSuscription() {


        if (this.paymentInformation != null) {

            if (this.paymentInformation.getCardNumber() != null) {

                final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CANCEL_SUSCRIPTION_ENDPOINT + UserController.getUser().getIdUser(), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.w("Mensaje", response);
                        customProgressDialog.dismissCustomProgressDialog();
                        getPaymentInformation();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        new SweetAlertDialog(PaymentInformationActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops!")
                                .setContentText(getString(R.string.algo_salio_mal))
                                .show();

                        customProgressDialog.dismissCustomProgressDialog();
                    }
                }) {

                };

                MySingleton.getInstance(this).addToRequestQueue(stringRequest);
                customProgressDialog.showCustomProgressDialog();

            } else {
                new SweetAlertDialog(PaymentInformationActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Suscripción con PAYPAL")
                        .setContentText("No es necesario cancelar la suscripción.")
                        .show();
            }
        }


    }
}
