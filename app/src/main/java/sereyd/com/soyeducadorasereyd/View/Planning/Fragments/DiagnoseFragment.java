package sereyd.com.soyeducadorasereyd.View.Planning.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.DiagnoseController;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.Controllers.TestController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.GenericClass.RecyclerViewDiagnoseSelection;
import sereyd.com.soyeducadorasereyd.Models.Diagnose;
import sereyd.com.soyeducadorasereyd.Models.DiagnoseSelection;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.TestModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.DiagnoseActivity;
import sereyd.com.soyeducadorasereyd.View.Planning.Adapter.DiagnoseAdapter;
import sereyd.com.soyeducadorasereyd.View.Planning.Adapter.DiagnoseSelectionAdapter;

public class DiagnoseFragment extends Fragment implements RecyclerViewDiagnoseSelection {


    @BindView(R.id.spinnerGroupDiagnose)
    Spinner spinnerGroup;

    @BindView(R.id.textViewSeleccionPrueba)
    TextView textViewSeleccionPrueba;

    @BindView(R.id.textViewResultadoGrupo)
    TextView textViewResultadoGrupo;

    @BindView(R.id.buttonStartDiagnose)
    Button buttonStartDiagnose;

    @BindView(R.id.recyclerViewDiagnoses)
    RecyclerView recyclerView;

    @BindView(R.id.recycler_view_diagnose_selection)
    RecyclerView recyclerViewSelection;

    private DiagnoseAdapter diagnoseAdapter;
    private DiagnoseSelectionAdapter diagnoseSelectionAdapter;

    ArrayList<DiagnoseSelection> diagnoseSelections;
    ArrayList<MyGroup> groupArrayList;

    //RECYCLER VIEW
    List<MyGroup> groupToPopulate = new ArrayList<>();
    int positionSelectForDiagnostic = -1;

    public DiagnoseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_diagnose, container, false);
        groupArrayList = GroupController.getCourses();
        setupView(view);
        return view;

    }

    private void setupView(View view) {

        ButterKnife.bind(this, view);

        //SPINNER
        ArrayList<String> stringList = new ArrayList<>();
        stringList.add(getContext().getResources().getString(R.string.grupo_el));
        for (MyGroup mygrop : groupArrayList) {
            stringList.add(groupArrayList.indexOf(mygrop) + 1, groupArrayList.get(groupArrayList.indexOf(mygrop)).getGrade() + groupArrayList.get(groupArrayList.indexOf(mygrop)).getGroupLetter());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, stringList);
        spinnerGroup.setAdapter(adapter);
        textViewSeleccionPrueba.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewResultadoGrupo.setTypeface(FontUtil.getRegularFont(getContext()));
        buttonStartDiagnose.setTypeface(FontUtil.getRegularFont(getContext()));

        diagnoseAdapter = new DiagnoseAdapter(groupToPopulate, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(diagnoseAdapter);

        //RECYCLER VIEW SELECTION
        diagnoseSelections = new ArrayList<>();
        diagnoseSelectionAdapter = new DiagnoseSelectionAdapter(diagnoseSelections, getActivity(), this);
        RecyclerView.LayoutManager mLayoutManagerSelection = new LinearLayoutManager(getContext());
        recyclerViewSelection.setLayoutManager(mLayoutManagerSelection);
        recyclerViewSelection.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSelection.setAdapter(diagnoseSelectionAdapter);
    }

    @Override
    public void onStart() {

        super.onStart();
        if (!getUserVisibleHint()) {
            return;
        }

        groupToPopulate.clear();
        diagnoseAdapter.notifyDataSetChanged();

        fetchGroupDiagnose();
        requestForAvailableDiagnostics();

    }

    private void fetchGroupDiagnose() {

        List<MyGroup> myGroups = GroupController.getCourses();

        for (final MyGroup myGroup : myGroups) {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getActivity());
            customProgressDialog.showCustomProgressDialog();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_DIAGNOSTIC_PENDING + myGroup.getIdGpr(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();
                    List<Diagnose> diagnoseList = DiagnoseController.getListDiagnosticPending(response, myGroup);

                    if (diagnoseList != null) {

                        groupToPopulate.add(myGroup);
                        DiagnoseController.saveListDiagnose(diagnoseList, myGroup.getId());
                        diagnoseAdapter.notifyDataSetChanged();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    customProgressDialog.dismissCustomProgressDialog();
                }
            });

            MySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
        }

    }


    private void requestForAvailableDiagnostics() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getActivity());
        customProgressDialog.showCustomProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_DIAGNOSTIC, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                List<DiagnoseSelection> list = DiagnoseController.getDiagnoseForSelection(response);
                if (list != null) {

                    diagnoseSelections.clear();
                    diagnoseSelections.addAll(list);
                    diagnoseSelectionAdapter.notifyDataSetChanged();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getResources().getString(R.string.error_general))
                        .show();

            }
        });

        MySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);

    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onStart();
        }
    }

    @OnClick(R.id.buttonStartDiagnose)
    public void buttonStartClicked(View view) {

        if (spinnerGroup.getSelectedItemPosition() != Constants.NO_ELEMENTS && positionSelectForDiagnostic != -1) {

            MyGroup myGroup = groupArrayList.get(spinnerGroup.getSelectedItemPosition() - 1);
            int numberOfStudentsInGroupToStart = StudentController.getStudentNumberInGroup(myGroup.getId());
            if (numberOfStudentsInGroupToStart > Constants.NO_ELEMENTS) {

                if (!DiagnoseController.incompleteDiagnoseByGroup(myGroup.getId())) {

                    downloadAndCreateGroupDiagnostic(myGroup, diagnoseSelections.get(positionSelectForDiagnostic).getIdDiagnose());

                } else {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getResources().getString(R.string.grupo_nofinalizado))
                            .show();
                }
            } else {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getResources().getString(R.string.grupo_noalumnos))
                        .show();
            }

        } else {

            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops!")
                    .setContentText(getResources().getString(R.string.grupo_ele) + "\n" + getResources().getString(R.string.test_selection))
                    .show();
        }

    }

    private void downloadAndCreateGroupDiagnostic(final MyGroup myGroup, final String idDiagnose) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getActivity());
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_TEST + idDiagnose, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                List<TestModel> testModelList = TestController.getTestFromResponse(response);

                if (testModelList != null) {

                    requestForAssociateGroupDiagnose(myGroup, testModelList, idDiagnose);

                } else {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getResources().getString(R.string.error_general))
                            .show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getResources().getString(R.string.error_general))
                        .show();

            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);

    }

    private void requestForAssociateGroupDiagnose(final MyGroup myGroup, final List<TestModel> testModelList, final String idDiagnose) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getActivity());
        customProgressDialog.showCustomProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_GROUP_DIAGNOSTIC, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                Diagnose diagnose = DiagnoseController.getDiagnoseCreationResponse(response);

                if (diagnose != null) {

                    diagnose.setMyGroup(myGroup);
                    diagnose.save();

                    Intent i = new Intent(getContext(), DiagnoseActivity.class);
                    i.putExtra("diagnose", diagnose.getId());
                    i.putExtra("group", myGroup.getId());
                    i.putExtra("test", (Serializable) testModelList);
                    startActivity(i);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                error.printStackTrace();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getResources().getString(R.string.error_general))
                        .show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_group", String.valueOf(myGroup.getIdGpr()));
                params.put("id_diagnostic", idDiagnose);
                return params;
            }
        };

        MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);

    }

    @Override
    public void recyclerViewListClicked(View v, int position) {

        positionSelectForDiagnostic = position;

    }
}
