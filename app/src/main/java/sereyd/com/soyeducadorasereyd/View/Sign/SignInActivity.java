package sereyd.com.soyeducadorasereyd.View.Sign;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Models.User;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.HandlerVolleyError;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.MainMenuActivity;

public class SignInActivity extends AppCompatActivity implements Validator.ValidationListener {


    @NotEmpty(messageResId = R.string.field_required)
    @Email(messageResId = R.string.email_required)
    @BindView(R.id.editTextUserEmail)
    EditText editTextUserEmail;
    @BindView(R.id.textViewTermsAndCons)
    TextView textViewTermsAndCons;
    @BindView(R.id.forgotPassword)
    TextView textViewForgotPassword;
    @Password(min = 6, messageResId = R.string.field_required)
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    @BindView(R.id.buttonSignIn)
    Button buttonSignIn;

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        setCustomViews();

    }

    @OnClick(R.id.textViewTermsAndCons)
    public void terms() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TERMS_URL));
        startActivity(browserIntent);
    }

    @OnClick(R.id.forgotPassword)
    public void forgotPassword() {
        Intent browserIntent = new Intent(this, RestorePasswordActivity.class);
        startActivity(browserIntent);
    }

    private void setCustomViews() {
        ButterKnife.bind(this);
        editTextUserEmail.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextPassword.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        buttonSignIn.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewTermsAndCons.setTypeface(FontUtil.getHairlineFont(getApplicationContext()));
        textViewForgotPassword.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @OnClick(R.id.buttonSignIn)
    public void signIn(View view) {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {



        final String email = editTextUserEmail.getText().toString();
        final String password = editTextPassword.getText().toString();

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                final User user = UserController.getUserFromLogin(response);

                if (user != null) {

                    downloadUserContentForGroupStudents(user);

                } else {
                    new SweetAlertDialog(SignInActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getResources().getString(R.string.error_general))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(SignInActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(HandlerVolleyError.getErrorMessage(error, SignInActivity.this))
                        .show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };

        MySingleton.getInstance(SignInActivity.this).addToRequestQueue(stringRequest);

    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {

            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void downloadUserContentForGroupStudents(User userAux) {

        UserController.saveSession(userAux, SignInActivity.this);

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        String url = Constants.URL_ENDPOINT_GROUP_USER + UserController.getUser().getIdUser();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                List<MyGroup> list = GroupController.getListFromResponse(response);

                if (list != null) {

                    GroupController.saveListGroupInDatabase(list);
                    List<MyGroup> myGroups = GroupController.getCourses();
                    for (int i = 0; i < myGroups.size(); i++) {

                        List<Student> studentList = StudentController.getListStudentForGroup(response, myGroups.get(i));
                        if (studentList != null) {
                            StudentController.saveListStudentInDatabase(studentList, myGroups.get(i).getId());
                        }

                    }

                    startActivity(new Intent(getApplicationContext(), MainMenuActivity.class));
                    finish();

                } else {

                    startActivity(new Intent(getApplicationContext(), MainMenuActivity.class));
                    finish();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);


    }
}
