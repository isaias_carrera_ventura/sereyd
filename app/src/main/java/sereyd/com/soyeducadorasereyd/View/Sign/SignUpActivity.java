package sereyd.com.soyeducadorasereyd.View.Sign;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.HandlerVolleyError;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class SignUpActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.buttonSignUp)
    Button buttonSignIn;

    @BindView(R.id.textViewTermsAndCons)
    TextView textViewTermsAndCons;
    @BindView(R.id.textviewnu)
    TextView textviewnu;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @Email(messageResId = R.string.email_required)
    @BindView(R.id.editTextEmail)
    EditText editTextUserEmail;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextName)
    EditText editTextName;

/*  @Password(min = 6, messageResId = R.string.field_required)
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;

    @ConfirmPassword
    @BindView(R.id.editTextPasswordConfirm)
    EditText editTextPasswordConfirm;*/
/*    @BindView(R.id.imageViewSignUp)
    CircleImageView imageSignUp;*/

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setCustomView();
        validator = new Validator(this);
        validator.setValidationListener(this);

    }

    private void setCustomView() {

        ButterKnife.bind(this);
        buttonSignIn.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewTermsAndCons.setTypeface(FontUtil.getHairlineFont(getApplicationContext()));
        editTextUserEmail.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        /*editTextPassword.setTypeface(FontUtil.getRegularFont(getApplicationContext()));*/
        editTextName.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textviewnu.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        /*editTextPasswordConfirm.setTypeface(FontUtil.getRegularFont(getApplicationContext()));*/

    }

    @Override
    public void onValidationSucceeded() {

        final String name = editTextName.getText().toString();
        final String email = editTextUserEmail.getText().toString();
        /*final String password = editTextPassword.getText().toString();*/

        String url = Constants.URL_ENDPOINT_USER;

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(getResources().getString(R.string.registro_exitoso))
                        .setContentText(getResources().getString(R.string.registro_exitoso_descr))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                finish();
                            }
                        })
                        .show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

                String body = HandlerVolleyError.getErrorUpdateResponse(error, SignUpActivity.this);
                
                new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(body != null ? body : HandlerVolleyError.getErrorMessage(error, SignUpActivity.this))
                        .show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("email", email);
                /*params.put("password", password);*/
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {

            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.buttonSignUp)
    public void registerUser(View view) {

        validator.validate();

    }


    @OnClick(R.id.textViewTermsAndCons)
    public void terms() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TERMS_URL));
        startActivity(browserIntent);
    }

}
