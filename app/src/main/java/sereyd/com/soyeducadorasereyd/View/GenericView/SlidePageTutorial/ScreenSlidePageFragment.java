package sereyd.com.soyeducadorasereyd.View.GenericView.SlidePageTutorial;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.InicioActivity;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;

/**
 * Created by isaiascarreraventura on 15/01/17.
 */
public class ScreenSlidePageFragment extends Fragment {

    private int limitPositionEndFragments = 3;

    @BindView(R.id.textViewTutorial)
    TextView textViewTutorial;

    @BindView(R.id.imageViewTutorial)
    ImageView imageView;

    @BindView(R.id.buttonSkip)
    Button button;

    int[] textTutorial = {
            R.string.tut_uno,
            R.string.tut_dos,
            R.string.tut_tres,
            R.string.tut_cuatro
    };

    int[] imageTutorial = {
            R.mipmap.tut_uno,
            R.mipmap.tut_dos,
            R.mipmap.tut_tres,
            R.mipmap.tut_cuatro,
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        int positionFragment = this.getArguments().getInt("position");

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_screen_slide_page, container, false);
        ButterKnife.bind(this, rootView);

        textViewTutorial.setText(textTutorial[positionFragment]);
        imageView.setBackgroundResource(imageTutorial[positionFragment]);
        textViewTutorial.setTypeface(FontUtil.getRegularFont(getContext()));

        if (positionFragment == limitPositionEndFragments) {

            button.setVisibility(View.VISIBLE);
            button.setTypeface(FontUtil.getRegularFont(getContext()));

            rootView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(getContext(), InicioActivity.class);
                    startActivity(i);
                    getActivity().finish();


                }
            });

        } else {
            button.setVisibility(View.INVISIBLE);
        }

        return rootView;
    }

}
