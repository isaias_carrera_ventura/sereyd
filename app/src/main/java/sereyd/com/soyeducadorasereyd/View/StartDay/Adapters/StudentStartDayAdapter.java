package sereyd.com.soyeducadorasereyd.View.StartDay.Adapters;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.AssistModel;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.CircleNetworkImageView;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;


public class StudentStartDayAdapter extends RecyclerView.Adapter<StudentStartDayAdapter.MyViewHolder> {

    private List<Student> studentList;
    private Context context;
    private List<AssistModel> assistModels;

    public StudentStartDayAdapter(List<Student> studentList, Context context, List<AssistModel> assistModels) {
        this.studentList = studentList;
        this.context = context;
        this.assistModels = assistModels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_student_start_day, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.textViewStudentNameStart.setText(studentList.get(position).getName() + " " + studentList.get(position).getLastName());
        holder.radioGroupConduct.setTag(position);
        ImageLoader mImageLoader = MySingleton.getInstance(context).getImageLoader();
        final String url = studentList.get(position).getImageUrl();
        mImageLoader.get(url, ImageLoader.getImageListener(holder.imageView, R.mipmap.person_gray, R.mipmap.person_gray));
        holder.imageView.setImageUrl(url, mImageLoader);
        /*Picasso.with(context).load(studentList.get(position).getImageUrl()).into(holder.imageView);*/


        holder.switchAssist.setTag(position);

        switch (assistModels.get(position).getAssist().getAssistStatus()) {
            case 0:
                holder.switchAssist.setChecked(false);
                break;
            case 1:
                holder.switchAssist.setChecked(true);
                break;
            default:
                holder.switchAssist.setChecked(false);
                break;

        }

        switch (assistModels.get(position).getAssist().getConudcT()) {

            case 0:
                holder.radioGroupConduct.check(R.id.radioButtonBad);
                break;
            case 1:
                holder.radioGroupConduct.check(R.id.radioButtonNormal);
                break;
            case 2:
                holder.radioGroupConduct.check(R.id.radioButtonGood);
                break;
            default:
                holder.radioGroupConduct.clearCheck();
                break;

        }

    }

    public List<AssistModel> getListResult() {
        return assistModels;
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewStudentNameStart)
        TextView textViewStudentNameStart;

        @BindView(R.id.textViewConducta)
        TextView textViewConducta;

        @BindView(R.id.imageViewStudent)
        CircleNetworkImageView imageView;

        @BindView(R.id.conductRadioGroup)
        RadioGroup radioGroupConduct;

        @BindView(R.id.switch1)
        Switch switchAssist;

       /* @BindView(R.id.textViewStudentNameStart)
        TextView textViewStudentNameStart;
        @BindView(R.id.textViewAsistencia)
        TextView textViewAsistencia;
        @BindView(R.id.textViewConducta)
        TextView textViewConducta;
        @BindView(R.id.imageViewStudent)
        ImageView imageView;
        @BindView(R.id.assistRadioGroup)
        RadioGroup radioGroupAssist;

        @BindView(R.id.conductRadioGroup)
        RadioGroup radioGroupConduct;*/

        public MyViewHolder(View view) {

            super(view);

            ButterKnife.bind(this, view);
            textViewConducta.setTypeface(FontUtil.getRegularFont(context));
            textViewStudentNameStart.setTypeface(FontUtil.getRegularFont(context));
            switchAssist.setTypeface(FontUtil.getRegularFont(context));
            radioGroupConduct.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {

                    int tag = (int) radioGroup.getTag();

                    //assistModels.get(tag).getAssist().setIdStudent(studentList.get(tag).getId_Student());

                    switch (checkedId) {

                        case R.id.radioButtonBad:
                            assistModels.get(tag).getAssist().setConductT(Constants.BAD);
                            break;

                        case R.id.radioButtonNormal:
                            assistModels.get(tag).getAssist().setConductT(Constants.NORMAL);
                            break;

                        case R.id.radioButtonGood:
                            assistModels.get(tag).getAssist().setConductT(Constants.GOOD);
                            break;

                        default:
                            assistModels.get(tag).getAssist().setConductT(Constants.UNDEFINED);
                            break;
                    }

                }
            });

            switchAssist.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    int tag = (int) compoundButton.getTag();
                    if (b) {
                        assistModels.get(tag).getAssist().setAssist(Constants.ASSIST);
                    } else {
                        assistModels.get(tag).getAssist().setAssist(Constants.NO_ASSIST);
                    }

                }
            });

            /*textViewStudentNameStart.setTypeface(FontUtil.getRegularFont(context));
            textViewConducta.setTypeface(FontUtil.getRegularFont(context));
            textViewAsistencia.setTypeface(FontUtil.getRegularFont(context));

            radioGroupAssist.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {

                    int tag = (int) radioGroup.getTag();

                    switch (checkedId) {
                        case R.id.radioButtonAssist:
                            assistModels.get(tag).getAssist().setAssist(Constants.ASSIST);
                            break;

                        case R.id.radioButtonNotAssist:
                            assistModels.get(tag).getAssist().setAssist(Constants.NO_ASSIST);
                            break;

                        default:
                            assistModels.get(tag).getAssist().setAssist(Constants.UNDEFINED);
                            break;
                    }

                }
            });

            radioGroupConduct.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {

                    int tag = (int) radioGroup.getTag();

                    //assistModels.get(tag).getAssist().setIdStudent(studentList.get(tag).getId_Student());

                    switch (checkedId) {

                        case R.id.radioButtonBad:
                            assistModels.get(tag).getAssist().setConductT(Constants.BAD);
                            break;

                        case R.id.radioButtonNormal:
                            assistModels.get(tag).getAssist().setConductT(Constants.NORMAL);
                            break;

                        case R.id.radioButtonGood:
                            assistModels.get(tag).getAssist().setConductT(Constants.GOOD);
                            break;

                        default:
                            assistModels.get(tag).getAssist().setConductT(Constants.UNDEFINED);
                            break;
                    }

                }
            });*/

        }
    }

}