package sereyd.com.soyeducadorasereyd.View.StartDay.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.AssistController;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.AssistModel;
import sereyd.com.soyeducadorasereyd.Models.AssistObject;
import sereyd.com.soyeducadorasereyd.Models.Cooperation;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.CustomDateSereyd;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.GroupView.Alarm.AddAlarmActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Alarm.AlarmActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Cooperation.AddCooperationActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Cooperation.CooperationActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Group.MainGroupMenuActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Homework.AddHomeworkActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Homework.HomeworkActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.StudentV.GroupDetailOptionsActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.StudentV.StudentDetailActivity;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.DiagnoseActivity;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.EvaluationActivity;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.PlanningMainActivity;
import sereyd.com.soyeducadorasereyd.View.StartDay.Adapters.StudentStartDayAdapter;

public class StartDayEvaluatingActivity extends AppCompatActivity {

    @BindView(R.id.containerWrap)
    RelativeLayout containerWrap;
    @BindView(R.id.toolbarStartDayEvaluation)
    Toolbar toolbar;
    @BindView(R.id.recyclerViewStudentStartDay)
    RecyclerView recyclerViewStudentStartDay;
    @BindView(R.id.startDayEvaluationTitleView)
    TextView textViewTitleView;
    @BindView(R.id.textViewDateStartDay)
    TextView textViewDate;
    @BindView(R.id.menu)
    FloatingActionMenu actionMenu;
    @BindView(R.id.viewTransparent)
    View viewTransparent;

    private StudentStartDayAdapter mAdapter;

    Long idGroup;
    List<AssistModel> assistModels;
    ArrayList<Student> studentArrayList;
    MyGroup myGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_day_evaluating);
        idGroup = getIntent().getExtras().getLong("group");
        myGroup = MyGroup.findById(MyGroup.class, idGroup);
        studentArrayList = StudentController.getStudents(idGroup);
        setCustomView();

    }

    private void setCustomView() {

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        textViewTitleView.setText(getResources().getString(R.string.grupo) + ": " + myGroup.getGrade() + myGroup.getGroupLetter());
        textViewTitleView.setTypeface(FontUtil.getRegularFont(this));
        textViewDate.setTypeface(FontUtil.getBoldItalicFont(this));
        textViewDate.setText(CustomDateSereyd.getDate());

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }

        assistModels = new ArrayList<>();

        if (AssistController.getListForBackup(myGroup) != null) {

            assistModels = AssistController.getListForBackup(myGroup);

        } else {

            for (int i = 0; i < studentArrayList.size(); i++) {

                assistModels.add(new AssistModel(String.valueOf(myGroup.getIdGpr()), new AssistObject(studentArrayList.get(i).getId_Student(), Constants.ASSIST, Constants.UNDEFINED)));

            }

        }


        mAdapter = new StudentStartDayAdapter(studentArrayList, this, assistModels);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewStudentStartDay.setLayoutManager(mLayoutManager);
        recyclerViewStudentStartDay.setItemAnimator(new DefaultItemAnimator());
        recyclerViewStudentStartDay.setAdapter(mAdapter);

        actionMenu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                if (opened) {
                    viewTransparent.setVisibility(View.VISIBLE);
                } else {
                    viewTransparent.setVisibility(View.INVISIBLE);
                }
            }
        });

    }


    @OnClick(R.id.buttonSend)
    public void sendListToServer(View view) {

        final List<AssistModel> assistModels = mAdapter.getListResult();
        boolean isListComplete = true;

        for (int i = 0; i < assistModels.size(); i++) {
            if (assistModels.get(i).getAssist().getConudcT() == Constants.UNDEFINED || assistModels.get(i).getAssist().getAssistStatus() == Constants.UNDEFINED) {
                isListComplete = false;
            }
        }

        if (isListComplete) {

            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id_group", myGroup.getIdGpr());
                JSONArray jsonArray = new JSONArray();

                for (int i = 0; i < assistModels.size(); i++) {

                    AssistObject assistObject = assistModels.get(i).getAssist();
                    JSONObject jsonObjectItem = new JSONObject();
                    jsonObjectItem.put("id_student", assistObject.getIdStudent());
                    jsonObjectItem.put("attendance", assistObject.getAssistStatus());
                    jsonObjectItem.put("behavior", assistObject.getConudcT());

                    jsonArray.put(jsonObjectItem);
                }

                jsonObject.put("list", jsonArray);

                final CustomProgressDialog customProgressDialog = new CustomProgressDialog(StartDayEvaluatingActivity.this);
                customProgressDialog.showCustomProgressDialog();

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.URL_ENDPOINT_DAY_PLANNING, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        customProgressDialog.dismissCustomProgressDialog();
                        try {

                            int responseInt = response.getInt("error");

                            if (responseInt == 0) {

                                AssistController.removeList(assistModels, myGroup);
                                updateListForRemovall();

                                new SweetAlertDialog(StartDayEvaluatingActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(getString(R.string.aviso))
                                        .setContentText(getString(R.string.lista_correct))
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismissWithAnimation();
                                                StartDayEvaluatingActivity.this.finish();
                                            }
                                        })
                                        .setConfirmText(getString(R.string.dialog_ok))
                                        .show();

                            } else {
                                new SweetAlertDialog(StartDayEvaluatingActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText(getString(R.string.ops))
                                        .setContentText(getString(R.string.algo_salio_mal))
                                        .setConfirmText(getString(R.string.dialog_ok))
                                        .show();

                            }

                        } catch (Exception e) {

                            new SweetAlertDialog(StartDayEvaluatingActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText(getString(R.string.ops))
                                    .setContentText(getString(R.string.algo_salio_mal))
                                    .setConfirmText(getString(R.string.dialog_ok))
                                    .show();

                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        customProgressDialog.dismissCustomProgressDialog();
                        new SweetAlertDialog(StartDayEvaluatingActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.algo_salio_mal))
                                .setConfirmText(getString(R.string.dialog_ok))
                                .show();

                    }
                });

                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                MySingleton.getInstance(StartDayEvaluatingActivity.this).addToRequestQueue(jsonObjectRequest);

            } catch (Exception e) {
                new SweetAlertDialog(StartDayEvaluatingActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .setConfirmText(getString(R.string.dialog_ok))
                        .show();
            }

        } else {
            new SweetAlertDialog(StartDayEvaluatingActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.lista_incomplete))
                    .setContentText(getString(R.string.complete_all_list))
                    .setConfirmText(getString(R.string.dialog_ok))
                    .show();
        }

    }

    private void updateListForRemovall() {

        assistModels.clear();

        if (AssistController.getListForBackup(myGroup) != null) {

            assistModels.addAll(AssistController.getListForBackup(myGroup));

        } else {

            for (int i = 0; i < studentArrayList.size(); i++) {

                assistModels.add(new AssistModel(String.valueOf(myGroup.getIdGpr()), new AssistObject(studentArrayList.get(i).getId_Student(), Constants.ASSIST, Constants.UNDEFINED)));

            }

        }

        mAdapter.notifyDataSetChanged();


    }


    @OnClick(R.id.floatActionButtonAlertDiary)
    public void alertDiary() {
        Intent i = new Intent(this, AlarmActivity.class);
        i.putExtra("group", idGroup);
        startActivity(i);
    }

    @OnClick(R.id.floatActionButtonCooperationDiary)
    public void cooperationDiary() {
        Intent i = new Intent(this, CooperationActivity.class);
        i.putExtra("group", idGroup);
        startActivity(i);
    }

    @OnClick(R.id.floatActionButtonHomeworkDiary)
    public void homeworkDiary() {
        Intent i = new Intent(this, HomeworkActivity.class);
        i.putExtra("group", idGroup);
        startActivity(i);
    }

    @OnClick(R.id.floatActionButtonPlanning)
    public void planningDiary() {
        startActivity(new Intent(this, PlanningMainActivity.class));
    }

    @OnClick(R.id.floatActionButtonDiary)
    public void diary() {
        startActivity(new Intent(this, DiaryActivity.class));
    }

    @Override
    public void onBackPressed() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.aviso_diagnostico))
                .setContentText(getString(R.string.question_diagnostico_finish))
                .setConfirmText(getString(R.string.later_save))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                        AssistController.saveListForBackup(assistModels, myGroup);
                        NavUtils.navigateUpFromSameTask(StartDayEvaluatingActivity.this);

                    }
                })
                .setCancelText("Salir")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();
                        NavUtils.navigateUpFromSameTask(StartDayEvaluatingActivity.this);

                    }
                })
                .show();

        //super.onBackPressed();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
