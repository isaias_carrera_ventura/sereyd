package sereyd.com.soyeducadorasereyd.View.Forum;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.ForumController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Diary;
import sereyd.com.soyeducadorasereyd.Models.ForumQuestionModel;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.HandlerVolleyError;
import sereyd.com.soyeducadorasereyd.View.EditProfileActivity;
import sereyd.com.soyeducadorasereyd.View.Forum.Adapters.ForumAdapter;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.StartDay.Adapters.DiaryAdapter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForumListActivity extends AppCompatActivity {

    @BindView(R.id.recyclerQuestions)
    RecyclerView recyclerQuestions;
    @BindView(R.id.buttonDoQuestion)
    Button buttonDoQuestion;

    ForumAdapter mAdapter;
    ArrayList<ForumQuestionModel> forumQuestionModelArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_list);
        ButterKnife.bind(this);
        setupView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getForumQuestions();
    }

    private void getForumQuestions() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_DISCUSSION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                List<ForumQuestionModel> forumQuestionModels = ForumController.getDiscussionList(response);
                if (forumQuestionModels != null) {

                    forumQuestionModelArrayList.clear();
                    forumQuestionModelArrayList.addAll(forumQuestionModels);
                    mAdapter.notifyDataSetChanged();

                } else {

                    new SweetAlertDialog(ForumListActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                new SweetAlertDialog(ForumListActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();

                customProgressDialog.dismissCustomProgressDialog();
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();

    }

    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));

    }

    private void setupView() {

        buttonDoQuestion.setTypeface(FontUtil.getRegularFont(this));

        forumQuestionModelArrayList = new ArrayList<>();
        mAdapter = new ForumAdapter(forumQuestionModelArrayList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerQuestions.setLayoutManager(mLayoutManager);
        recyclerQuestions.setItemAnimator(new DefaultItemAnimator());
        recyclerQuestions.setAdapter(mAdapter);


    }

    @OnClick(R.id.buttonDoQuestion)
    public void makeQuestion() {
        startActivity(new Intent(this, QuestionForumActivity.class));
    }
}
