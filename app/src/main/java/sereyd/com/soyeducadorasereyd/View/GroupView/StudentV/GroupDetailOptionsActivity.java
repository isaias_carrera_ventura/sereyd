package sereyd.com.soyeducadorasereyd.View.GroupView.StudentV;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.clans.fab.FloatingActionMenu;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.GroupView.Alarm.AddAlarmActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Alarm.AlarmActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Cooperation.AddCooperationActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Cooperation.CooperationActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Homework.AddHomeworkActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Homework.HomeworkActivity;

public class GroupDetailOptionsActivity extends AppCompatActivity {


    @BindView(R.id.students)
    RecyclerView studentsRecyclerView;
    @BindView(R.id.textViewCooperation)
    TextView textViewCooperation;
    @BindView(R.id.textViewAlert)
    TextView textViewAlert;
    @BindView(R.id.textViewHomework)
    TextView textViewHomework;
    @BindView(R.id.grupoTitleView)
    TextView textviewGrupoTitleView;
    @BindView(R.id.textViewNoStudent)
    TextView textViewNoStudent;

    @BindView(R.id.menu)
    FloatingActionMenu buttonMenu;
    @BindView(R.id.toolbarGroupDetail)
    Toolbar toolbar;

    //Students
    StudentAdapter studentAdapter;
    List<Student> studentList;
    MyGroup myGroup;
    Long idGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_student);
        ButterKnife.bind(this);
        setUpView();

        idGroup = getIntent().getExtras().getLong("group");
        myGroup = MyGroup.findById(MyGroup.class, idGroup);

        textviewGrupoTitleView.setText(getResources().getString(R.string.grupo) + ": " + myGroup.getGrade() + myGroup.getGroupLetter());

        //Students
        studentList = StudentController.getStudents(idGroup);
        studentAdapter = new StudentAdapter(this, studentList);
        studentsRecyclerView.setAdapter(studentAdapter);
        studentsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        setRecyclerView();

    }

    @Override
    protected void onStart() {
        super.onStart();
        createRequestForGroup();
    }

    private void createRequestForGroup() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();
        String url = Constants.URL_ENDPOINT_GROUP + myGroup.getIdGpr();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                List<Student> studentListFromServer = StudentController.getStudentListFromResponse(response);
                if (studentListFromServer != null) {

                    studentList.clear();
                    StudentController.saveListStudentInDatabase(studentListFromServer, idGroup);
                    studentList.addAll(StudentController.getStudents(idGroup));
                    studentAdapter.notifyDataSetChanged();

                    setRecyclerView();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    public void setRecyclerView() {

        if (studentList.size() > 0) {

            studentsRecyclerView.setVisibility(View.VISIBLE);
            textViewNoStudent.setVisibility(View.INVISIBLE);

        } else {

            studentsRecyclerView.setVisibility(View.INVISIBLE);
            textViewNoStudent.setVisibility(View.VISIBLE);

        }

    }

    @Override
    protected void onRestart() {

        super.onRestart();
        setRecyclerView();

    }

    private void setUpView() {

        textViewAlert.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewCooperation.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewHomework.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textviewGrupoTitleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewNoStudent.setTypeface(FontUtil.getHairlineFont(getApplicationContext()));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == Constants.ADD_ACTIVITY_RESULT) {

                saveStudentToDisk();

            } else if (requestCode == Constants.EDIT_ACTIVITY_RESULT) {

                saveExistingStudentChanges(data);

            }
        }
    }

    private void saveExistingStudentChanges(Intent data) {

        int positionRecyclerChanged = data.getIntExtra("positionRecycler", -1);
        Student studentInDisk = studentList.get(positionRecyclerChanged);
        Student newStudent = (Student) data.getSerializableExtra("student");
        studentInDisk.setName(newStudent.getName());
        studentInDisk.setLastName(newStudent.getLastName());
        studentInDisk.setBirthday(newStudent.getBirthday());
        studentInDisk.setSex(newStudent.getSex());
        studentInDisk.setEmergencyContact(newStudent.getEmergencyContact());
        studentInDisk.setMyGroup(myGroup);
        studentInDisk.save();
        studentList.set(positionRecyclerChanged, studentInDisk);
        studentAdapter.notifyDataSetChanged();

    }

    private void saveStudentToDisk() {

        studentList.clear();
        studentList.addAll(StudentController.getStudents(idGroup));
        studentAdapter.notifyDataSetChanged();

    }


    @OnClick(R.id.floatActionButtonAlert)
    public void actionFloatButtonAddAlert(View view) {

        Intent intent = new Intent(getApplicationContext(), AddAlarmActivity.class);
        intent.putExtra("group", idGroup);
        startActivity(intent);
        buttonMenu.close(false);

    }

    @OnClick(R.id.buttonAvisos)
    public void listAlert(View view) {

        Intent intent = new Intent(getApplicationContext(), AlarmActivity.class);
        intent.putExtra("group", idGroup);
        startActivity(intent);

    }

    @OnClick(R.id.floatActionButtonCooperation)
    public void actionFloatButtonAddCooperation(View view) {

        Intent intent = new Intent(getApplicationContext(), AddCooperationActivity.class);
        intent.putExtra("group", idGroup);
        startActivity(intent);
        buttonMenu.close(false);

    }

    @OnClick(R.id.buttonCooperation)
    public void listCooperation(View view) {

        Intent intent = new Intent(getApplicationContext(), CooperationActivity.class);
        intent.putExtra("group", idGroup);
        startActivity(intent);

    }


    @OnClick(R.id.floatActionButtonStudent)
    public void actionFloatButtonAddStudent(View view) {

        Intent intent = new Intent(getApplicationContext(), AddStudentActivity.class);
        intent.putExtra("group", idGroup);
        startActivityForResult(intent, Constants.ADD_ACTIVITY_RESULT);
        buttonMenu.close(false);

    }

    @OnClick(R.id.floatActionButtonHomework)
    public void actionFloatButtonAddHomework(View view) {

        Intent intent = new Intent(getApplicationContext(), AddHomeworkActivity.class);
        intent.putExtra("group", idGroup);
        startActivityForResult(intent, Constants.ADD_ACTIVITY_RESULT);
        buttonMenu.close(false);

    }

    @OnClick(R.id.buttonHomework)
    public void listHomework(View view) {

        Intent intent = new Intent(getApplicationContext(), HomeworkActivity.class);
        intent.putExtra("group", idGroup);
        startActivityForResult(intent, Constants.ADD_ACTIVITY_RESULT);

    }

}
