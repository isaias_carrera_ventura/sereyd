package sereyd.com.soyeducadorasereyd.View;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.PlanningController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class PaymentActivitySereyd extends AppCompatActivity {

    @BindView(R.id.buttonSendMonthly)
    Button buttonMonthly;
    @BindView(R.id.textViewPaymentContentMonthly)
    TextView textViewMonthly;
    @BindView(R.id.textViewPaymentTypeMonthly)
    TextView textViewPaymentTypeMonthly;
    @BindView(R.id.textViewUSDMonthly)
    TextView textViewUSDMonthly;

    @BindView(R.id.buttonSendAnnually)
    Button buttonAnnually;
    @BindView(R.id.textViewPaymentContentAnnually)
    TextView textViewAnnually;
    @BindView(R.id.textViewUSDAnnually)
    TextView textViewUSDAnnually;
    @BindView(R.id.textViewPaymentTypeAnnually)
    TextView textViewPaymentTypeAnnually;

    @BindView(R.id.buttonSendMonthlyCard)
    Button buttonSendMonthlyCard;
    @BindView(R.id.buttonSendAnnuallyCard)
    Button buttonSendAnnuallyCard;

    int paymentTypeSuscription = -1;

    PayPalConfiguration m_configuation;
    Intent m_service;
    int m_paypalRequestCode = 999; //or any number
    int m_openPayRequestCode = 777;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_payment);

        paypalConfiguration();

        ButterKnife.bind(this);

        textViewUSDMonthly.setTypeface(FontUtil.getRegularFont(this));
        textViewMonthly.setTypeface(FontUtil.getRegularFont(this));
        buttonMonthly.setTypeface(FontUtil.getRegularFont(this));
        textViewPaymentTypeMonthly.setTypeface(FontUtil.getBoldFont(this));
        buttonAnnually.setTypeface(FontUtil.getRegularFont(this));
        textViewAnnually.setTypeface(FontUtil.getRegularFont(this));
        textViewUSDAnnually.setTypeface(FontUtil.getRegularFont(this));
        textViewPaymentTypeAnnually.setTypeface(FontUtil.getBoldFont(this));
        buttonSendMonthlyCard.setTypeface(FontUtil.getRegularFont(this));
        buttonSendAnnuallyCard.setTypeface(FontUtil.getRegularFont(this));
    }

    private void paypalConfiguration() {

        m_configuation = new PayPalConfiguration()
                .acceptCreditCards(false)
                .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
                .clientId(getString(R.string.paypal_credential_id_live));

        /*m_configuation = new PayPalConfiguration()
                .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                .acceptCreditCards(false)
                .clientId(getString(R.string.paypal_credential_id_sandbox));*/

        m_service = new Intent(this, PayPalService.class);
        m_service.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, m_configuation);
        startService(m_service);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == m_paypalRequestCode) {

                PaymentConfirmation confirmation = data.getParcelableExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirmation != null) {

                    String state = confirmation.getProofOfPayment().getState();
                    if (state.equals("approved")) {

                        requestForPaymentApprovedToServer();
                    } else {

                        new SweetAlertDialog(PaymentActivitySereyd.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.algo_salio_mal))
                                .show();

                    }

                } else {
                    new SweetAlertDialog(PaymentActivitySereyd.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }

            }

            if (requestCode == m_openPayRequestCode) {

                requestForPaymentApprovedToServer();

            }

        }


    }

    @OnClick({R.id.buttonSendMonthly, R.id.buttonSendAnnually})
    public void doPayment(View view) {

        PayPalPayment payPalPayment;
        if (view.getId() == R.id.buttonSendMonthly) {
            payPalPayment = new PayPalPayment(new BigDecimal("6.90"), "USD", "Suscripción a Sereyd", PayPalPayment.PAYMENT_INTENT_SALE);
            paymentTypeSuscription = Constants.SUSCRIPTION_MONTH;
        } else {
            payPalPayment = new PayPalPayment(new BigDecimal("69.0"), "USD", "Suscripción a Sereyd", PayPalPayment.PAYMENT_INTENT_SALE);
            paymentTypeSuscription = Constants.SUSCRIPTION_YEAR;
        }

        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, m_configuation);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment);
        startActivityForResult(intent, m_paypalRequestCode);

    }


    @OnClick({R.id.buttonSendMonthlyCard, R.id.buttonSendAnnuallyCard})
    public void doPaymentCard(View view) {

        if (view.getId() == R.id.buttonSendMonthlyCard) {
            paymentTypeSuscription = Constants.SUSCRIPTION_MONTH;
        } else {
            paymentTypeSuscription = Constants.SUSCRIPTION_YEAR;
        }

        Intent intent = new Intent(this, OpenPayPaymentActivity.class);
        intent.putExtra("type", paymentTypeSuscription);
        startActivityForResult(intent, m_openPayRequestCode);

    }


    private void requestForPaymentApprovedToServer() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_PAYMENT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                if (PlanningController.isPaymentCorrect(response)) {

                    new SweetAlertDialog(PaymentActivitySereyd.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.aviso))
                            .setContentText(getString(R.string.pago_correcto) + "\n" + getString(R.string.approved_payment))
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                }
                            })
                            .show();


                } else {

                    new SweetAlertDialog(PaymentActivitySereyd.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(PaymentActivitySereyd.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_user", UserController.getUser().getIdUser());
                params.put("type", String.valueOf(paymentTypeSuscription));
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();

    }


}
