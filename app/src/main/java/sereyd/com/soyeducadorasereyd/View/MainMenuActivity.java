package sereyd.com.soyeducadorasereyd.View;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.User;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.CircleNetworkImageView;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.Forum.ForumListActivity;
import sereyd.com.soyeducadorasereyd.View.GroupView.Group.MainGroupMenuActivity;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.PlanningMainActivity;
import sereyd.com.soyeducadorasereyd.View.Resources.Activities.ResourcesListActivity;
import sereyd.com.soyeducadorasereyd.View.ShopSereyd.WebShopActivity;
import sereyd.com.soyeducadorasereyd.View.StartDay.Activities.StartDayActivity;

public class MainMenuActivity extends AppCompatActivity {

    @BindView(R.id.imageViewProfile)
    CircleNetworkImageView imageProfile;
    @BindView(R.id.textViewUserName)
    TextView textViewUserName;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textViewGroup)
    TextView textViewGroup;
    @BindView(R.id.textViewPlanning)
    TextView textViewPlanning;
    @BindView(R.id.textViewResource)
    TextView textViewResource;
    @BindView(R.id.textViewShop)
    TextView textViewShop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_menu);
        ButterKnife.bind(this);
        setViewForUser();
        setSupportActionBar(toolbar);

        try {

            ImageLoader mImageLoader = MySingleton.getInstance(this).getImageLoader();
            String url = UserController.getUser().getLinkImage();
            mImageLoader.get(url, ImageLoader.getImageListener(imageProfile, R.mipmap.icon_person_o, R.mipmap.icon_person_o));
            imageProfile.setImageUrl(url, mImageLoader);
            imageProfile.setDrawingCacheEnabled(false);
            textViewUserName.setText(UserController.getUser().getName());

            getSupportActionBar().setDisplayShowTitleEnabled(false);

        } catch (Exception ignored) {

        }

    }

    @Override
    protected void onResume() {

        super.onResume();

        try {

            textViewUserName.setText(UserController.getUser().getName());
            try {


                ImageLoader mImageLoader = MySingleton.getInstance(this).getImageLoader();
                final String url = UserController.getUser().getLinkImage();
                mImageLoader.get(url, ImageLoader.getImageListener(imageProfile,
                        R.mipmap.icon_person_o, R.mipmap.icon_person_o));
                imageProfile.setImageUrl(url, mImageLoader);
                imageProfile.setDrawingCacheEnabled(false);
                textViewUserName.setText(UserController.getUser().getName());


            } catch (Exception ignored) {

            }

        } catch (Exception ignored) {

        }


    }

    public void setViewForUser() {

        textViewUserName.setTypeface(FontUtil.getBoldFont(getApplicationContext()));
        textViewGroup.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewPlanning.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewResource.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewShop.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.configuration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(getApplicationContext(), ConfigurationActivity.class));
                break;
            case R.id.action_chat:
                startActivity(new Intent(getApplicationContext(), ForumListActivity.class));
                break;
            default:
                return super.onOptionsItemSelected(item);
        }


        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.buttonGroup)
    public void groupMenu(View view) {
        startActivity(new Intent(getApplicationContext(), MainGroupMenuActivity.class));
    }

    @OnClick(R.id.buttonShop)
    public void openWebShop(View view) {
        /*startActivity(new Intent(getApplicationContext(), WebShopActivity.class));*/
        // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.URL_SHOP));
        startActivity(intent);

    }

    @OnClick(R.id.buttonPlanning)
    public void openPlanning(View view) {
        startActivity(new Intent(getApplicationContext(), PlanningMainActivity.class));
    }

    @OnClick(R.id.buttonResources)
    public void buttonResources(View view) {

        startActivity(new Intent(getApplicationContext(), ResourcesListActivity.class));

    }

    @OnClick(R.id.buttonEdit)
    public void editProfile(View view) {

        startActivity(new Intent(getApplicationContext(), EditProfileActivity.class));

    }

    @OnClick(R.id.buttonSignIn)
    public void startDay(View view) {

        if (GroupController.getCourses().size() > 0) {
            startActivity(new Intent(getApplicationContext(), StartDayActivity.class));
        } else {
            new SweetAlertDialog(MainMenuActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops!")
                    .setContentText(getResources().getString(R.string.sin_grupos))
                    .show();
        }

    }

}