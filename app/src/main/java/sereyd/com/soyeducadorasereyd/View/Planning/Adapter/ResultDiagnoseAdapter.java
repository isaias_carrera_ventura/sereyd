package sereyd.com.soyeducadorasereyd.View.Planning.Adapter;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.Models.TestModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;

/**
 * Created by chaycv on 18/04/17.
 */

public class ResultDiagnoseAdapter extends RecyclerView.Adapter<ResultDiagnoseAdapter.MyViewHolder> {

    private List<TestModel> questionList;
    Context context;

    public ResultDiagnoseAdapter(List<TestModel> moviesList, Context context) {
        this.questionList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_question, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.textViewQuestionType.setText(questionList.get(position).getNameTest());
        holder.textViewQuestion.setText(questionList.get(position).getQuestionString());
        holder.radioGroup.setTag(position);

        switch (questionList.get(position).getResult()) {
            case 0:
                holder.radioGroup.check(R.id.noDiagnose);
                break;
            case 1:
                holder.radioGroup.check(R.id.siDiagnose);
                break;
            case 2:
                holder.radioGroup.check(R.id.process);
                break;
            default:
                holder.radioGroup.clearCheck();
                break;
        }
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    public List<TestModel> getListResult() {
        return questionList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.radioGroupAnswer)
        RadioGroup radioGroup;

        @BindView(R.id.textViewQuestion)
        TextView textViewQuestion;

        @BindView(R.id.textViewQuestionType)
        TextView textViewQuestionType;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            textViewQuestionType.setTypeface(FontUtil.getRegularFont(context));
            textViewQuestion.setTypeface(FontUtil.getRegularFont(context));
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                    int tag = (int) group.getTag();

                    if (checkedId == R.id.noDiagnose) {
                        questionList.get(tag).setResult(0);
                    } else if (checkedId == R.id.siDiagnose) {
                        questionList.get(tag).setResult(1);
                    } else if (checkedId == R.id.process) {
                        questionList.get(tag).setResult(2);
                    } else {
                        questionList.get(tag).setResult(Constants.UNDEFINED);
                    }

                }
            });
        }

    }


}
