package sereyd.com.soyeducadorasereyd.View.GroupView.StudentV;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.CircleNetworkImageView;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

/**
 * Created by isaiascarreraventura on 02/01/17.
 */
public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {

    List<Student> studentList;
    Context contextAdapter;

    public StudentAdapter(Context context, List<Student> students) {
        studentList = students;
        contextAdapter = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return contextAdapter;
    }


    @Override
    public StudentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_student, parent, false);

        // Return a new holder instance
        return new ViewHolder(contactView);

    }

    @Override
    public void onBindViewHolder(StudentAdapter.ViewHolder holder, int position) {

        Student student = studentList.get(position);
        TextView textViewName = holder.nameTextView;
        TextView textViewLastName = holder.textViewLastName;
        textViewLastName.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewLastName.setText(student.getLastName());
        textViewName.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewName.setText(student.getName());
        /*Picasso.with(getContext()).load(student.getImageUrl()).into(holder.imageViewStudent);*/
        ImageLoader mImageLoader = MySingleton.getInstance(getContext()).getImageLoader();
        final String url = student.getImageUrl();
        mImageLoader.get(url, ImageLoader.getImageListener(holder.imageViewStudent, R.mipmap.person_gray, R.mipmap.person_gray));
        holder.imageViewStudent.setImageUrl(url, mImageLoader);

    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.student_name)
        TextView nameTextView;
        @BindView(R.id.buttonRemoveStudent)
        Button buttonRemoveStudent;
        @BindView(R.id.buttonEditStudent)
        Button buttonEditStudent;
        @BindView(R.id.student_lastname)
        TextView textViewLastName;
        @BindView(R.id.imageViewStudent)
        CircleNetworkImageView imageViewStudent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @OnClick(R.id.buttonRemoveStudent)
        public void removeStudentButtonClicked(View view) {

            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                removeStudent(position);
            }
        }

        @OnClick(R.id.buttonEditStudent)
        public void editStudentButtonClicked(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                Intent intent = new Intent(contextAdapter, AddStudentActivity.class);
                intent.putExtra("studentToEdit", studentList.get(position).getId());
                intent.putExtra("positionRecycler", position);
                ((Activity) contextAdapter).startActivityForResult(intent, Constants.EDIT_ACTIVITY_RESULT);
            }
        }

        private void removeStudent(final int position) {


            new SweetAlertDialog(contextAdapter, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(contextAdapter.getResources().getString(R.string.aviso))
                    .setContentText(contextAdapter.getResources().getString(R.string.pregunta_alumno))
                    .setCancelText(contextAdapter.getResources().getString(R.string.cancelar))
                    .setConfirmText(contextAdapter.getResources().getString(R.string.eliminar))
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            sweetAlertDialog.dismissWithAnimation();
                            setDeleteRequestToApi(position);

                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    })
                    .show();

        }

        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {

                Intent intent = new Intent(getContext(), StudentDetailActivity.class);
                intent.putExtra("student", studentList.get(position).getId());
                contextAdapter.startActivity(intent);

            }

        }
    }

    private void setDeleteRequestToApi(final int position) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(contextAdapter);
        customProgressDialog.showCustomProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, Constants.URL_ENDPOINT_STUDENT + studentList.get(position).getId_Student(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                if (StudentController.deleteStudentFromServer(response)) {

                    studentList.get(position).delete();
                    studentList.remove(position);
                    notifyItemRemoved(position);
                    new SweetAlertDialog(contextAdapter, SweetAlertDialog.SUCCESS_TYPE)
                            .setContentText(contextAdapter.getString(R.string.alumno_eliminado))
                            .setTitleText(contextAdapter.getString(R.string.aviso))
                            .show();

                } else {
                    new SweetAlertDialog(contextAdapter, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(contextAdapter.getString(R.string.ops))
                            .setContentText(contextAdapter.getString(R.string.algo_salio_mal))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(contextAdapter, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(contextAdapter.getString(R.string.ops))
                        .setContentText(contextAdapter.getString(R.string.algo_salio_mal))
                        .show();
            }
        });

        MySingleton.getInstance(contextAdapter).addToRequestQueue(stringRequest);

    }
}
