package sereyd.com.soyeducadorasereyd.View.GroupView.Group;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class MainGroupMenuActivity extends AppCompatActivity {

    ArrayList<MyGroup> groups;

    @BindView(R.id.groups)
    RecyclerView groupsRecyclerView;

    @BindView(R.id.floatActionButtonGroup)
    FloatingActionButton button;

    @BindView(R.id.misGrupos)
    TextView textViewTitleView;

    @BindView(R.id.toolbarMainGroup)
    Toolbar toolbar;

    @BindView(R.id.textViewNoGroup)
    TextView textViewNoGroup;

    GroupAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_group);

        setCustomView();

        // Initialize groups
        groups = GroupController.getCourses();
        // Create adapter passing in the sample user data
        adapter = new GroupAdapter(this, groups);
        // Attach the adapter to the recyclerview to populate items
        groupsRecyclerView.setAdapter(adapter);
        // Set layout manager to position the items
        groupsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        // That's all!
        setRecyclerView();
        requestForUserGroups();

    }

    private void requestForUserGroups() {


        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        String url = Constants.URL_ENDPOINT_GROUP_USER + UserController.getUser().getIdUser();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                List<MyGroup> list = GroupController.getListFromResponse(response);
                if (list != null) {

                    groups.clear();
                    GroupController.saveListGroupInDatabase(list);
                    groups.addAll(GroupController.getCourses());
                    adapter.notifyDataSetChanged();

                    setRecyclerView();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    protected void onRestart() {

        super.onRestart();
        setRecyclerView();
        Intent intent = getIntent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        startActivity(intent);

    }

    public void setRecyclerView() {

        if (groups.size() > 0) {

            groupsRecyclerView.setVisibility(View.VISIBLE);
            textViewNoGroup.setVisibility(View.INVISIBLE);

        } else {

            groupsRecyclerView.setVisibility(View.INVISIBLE);
            textViewNoGroup.setVisibility(View.VISIBLE);

        }

    }

    private void setCustomView() {

        ButterKnife.bind(this);
        textViewTitleView.setTypeface(FontUtil.getRegularFont(this));
        textViewNoGroup.setTypeface(FontUtil.getHairlineFont(this));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }

    }

    @OnClick(R.id.floatActionButtonGroup)
    public void actionFloatButtonAddGroup(View view) {

        startActivityForResult(new Intent(getApplicationContext(), AddGroupActivity.class), Constants.ADD_ACTIVITY_RESULT);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            if (requestCode == Constants.ADD_ACTIVITY_RESULT) {

                saveGroupToRecycler(data);

            } else if (requestCode == Constants.EDIT_ACTIVITY_RESULT) {

                int positionRecyclerChanged = data.getIntExtra("positionRecycler", -1);
                editGroup(positionRecyclerChanged, data);

            }
        }
    }

    private void editGroup(int positionRecyclerChanged, Intent data) {

        MyGroup groupInDisk = groups.get(positionRecyclerChanged);
        MyGroup newGroup = (MyGroup) data.getSerializableExtra("group");
        groupInDisk.setSchool(newGroup.getSchool());
        groupInDisk.setGrade(newGroup.getGrade());
        groupInDisk.setGroupLetter(newGroup.getGroupLetter());
        groupInDisk.setPeriod(newGroup.getPeriod());
        groupInDisk.save();
        groups.set(positionRecyclerChanged, groupInDisk);
        adapter.notifyDataSetChanged();
        Toast.makeText(getApplicationContext(), R.string.grupo_editado, Toast.LENGTH_SHORT).show();

    }


    private void saveGroupToRecycler(Intent data) {

        MyGroup newGroup = (MyGroup) data.getSerializableExtra("group");
        groups.add(newGroup);
        adapter.notifyDataSetChanged();
        groupsRecyclerView.scrollToPosition(groups.size() - 1);

    }
}
