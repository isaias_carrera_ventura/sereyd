package sereyd.com.soyeducadorasereyd.View.Planning.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.DiagnoseController;
import sereyd.com.soyeducadorasereyd.Controllers.TestController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Diagnose;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.TestModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.DiagnoseActivity;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.GraphDiagnoseActivity;

/**
 * Created by isaiascarreraventura on 07/03/17.
 * Project SoyeducadoraSEREYD
 */

public class DiagnoseAdapter extends RecyclerView.Adapter<DiagnoseAdapter.MyViewHolder> {

    private List<MyGroup> groupList;
    private Context context;

    public DiagnoseAdapter(List<MyGroup> diagnose, Context context) {

        this.groupList = diagnose;
        this.context = context;

    }

    public Context getContext() {
        return context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_diagnose, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final MyGroup myGroup = groupList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (DiagnoseController.incompleteDiagnoseByGroup(groupList.get(position).getId())) {

                    Diagnose diagnose = DiagnoseController.getIncompleteDiagnose(myGroup.getId());
                    downloadForListQuestion(diagnose, myGroup, diagnose.getDiagnoseId());

                } else {

                    Intent intent = new Intent(getContext(), GraphDiagnoseActivity.class);
                    intent.putExtra("group", myGroup.getId());
                    getContext().startActivity(intent);

                }


            }
        });


        holder.notFinished.setVisibility(DiagnoseController.incompleteDiagnoseByGroup(groupList.get(position).getId()) ? View.VISIBLE : View.INVISIBLE);
        holder.title.setText(getContext().getResources().getString(R.string.diagnostico) + ": " + myGroup.getGrade() + myGroup.getGroupLetter());

    }

    private void downloadForListQuestion(final Diagnose diagnose, final MyGroup myGroup, int diagnoseId) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getContext());
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_TEST + diagnoseId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                List<TestModel> testModelList = TestController.getTestFromResponse(response);

                if (testModelList != null) {

                    Intent intent = new Intent(getContext(), DiagnoseActivity.class);
                    intent.putExtra("group", myGroup.getId());
                    intent.putExtra("diagnose", diagnose.getId());
                    intent.putExtra("test", (Serializable) testModelList);
                    getStudentListPendingToEvaluate(intent, diagnose, myGroup);


                } else {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getContext().getString(R.string.error_general))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getContext().getString(R.string.error_general))
                        .show();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);


    }

    public void getStudentListPendingToEvaluate(final Intent intent, final Diagnose diagnose, final MyGroup myGroup) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getContext());
        customProgressDialog.showCustomProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_PENDING_STUDENT_TEST + diagnose.getIdGroupDiagnose(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                List<String> stringList = TestController.getListStudentForPending(response);
                if (stringList != null) {

                    intent.putExtra("studentsID", (Serializable) stringList);
                    getContext().startActivity(intent);

                } else {

                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getContext().getString(R.string.error_general))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getContext().getString(R.string.error_general))
                        .show();
            }
        });

        MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);

    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.notFinished)
        TextView notFinished;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            title.setTypeface(FontUtil.getRegularFont(getContext()));
            notFinished.setTypeface(FontUtil.getRegularFont(getContext()));
        }


    }

}
