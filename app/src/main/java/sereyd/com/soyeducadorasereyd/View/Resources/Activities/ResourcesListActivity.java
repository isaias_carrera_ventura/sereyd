package sereyd.com.soyeducadorasereyd.View.Resources.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.ResourceController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.ResourceItemFile;
import sereyd.com.soyeducadorasereyd.Models.ResourcesItem;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Resources.Adapter.AdapterResourceContainer;
import sereyd.com.soyeducadorasereyd.View.Resources.Adapter.ResourceItemFileAdapter;

public class ResourcesListActivity extends AppCompatActivity {

    @BindView(R.id.resourcesTextview)
    TextView resourcesTextview;
    @BindView(R.id.toolbarResourcesDetail)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerViewResource;
    List<ResourcesItem> itemResourceAuxes = new ArrayList<>();
    AdapterResourceContainer adapterResourceContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resources_list);
        setupView();
    }

    private void setupView() {

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }

        resourcesTextview.setTypeface(FontUtil.getRegularFont(ResourcesListActivity.this));

        recyclerViewResource = (RecyclerView) findViewById(R.id.recycler_view);

        adapterResourceContainer = new AdapterResourceContainer(itemResourceAuxes, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewResource.setLayoutManager(mLayoutManager);
        recyclerViewResource.setItemAnimator(new DefaultItemAnimator());
        recyclerViewResource.setAdapter(adapterResourceContainer);

        getResourcesFromServer();

    }

    private void getResourcesFromServer() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_RESOURCE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customProgressDialog.dismissCustomProgressDialog();

                List<ResourcesItem> resourcesItemList = ResourceController.getListItem(response);
                if (resourcesItemList != null) {

                    itemResourceAuxes.clear();
                    itemResourceAuxes.addAll(resourcesItemList);
                    adapterResourceContainer.notifyDataSetChanged();

                    getResourcesForUser();

                } else {

                    new SweetAlertDialog(ResourcesListActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(ResourcesListActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();

                loadAlternativeViewForResources();

            }
        });

        MySingleton.getInstance(ResourcesListActivity.this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();
    }


    private void loadAlternativeViewForResources() {

        List<ResourceItemFile> resourceItemFilesDownloaded = ResourceItemFile.listAll(ResourceItemFile.class);

        for (int i = 0; i < resourceItemFilesDownloaded.size(); i++) {

            if (!ResourceController.isResourceFileInDisk(resourceItemFilesDownloaded.get(i), this)) {
                resourceItemFilesDownloaded.remove(i);
            }

        }

        if (resourceItemFilesDownloaded.size() > 0) {

            ResourceItemFileAdapter mAdapter = new ResourceItemFileAdapter(resourceItemFilesDownloaded, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerViewResource.setLayoutManager(mLayoutManager);
            recyclerViewResource.setItemAnimator(new DefaultItemAnimator());
            recyclerViewResource.setAdapter(mAdapter);
        }

    }


    private void getResourcesForUser() {

        try {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_RESOURCE_USER + UserController.getUser().getIdUser(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    customProgressDialog.dismissCustomProgressDialog();
                    List<ResourceItemFile> resourceItemFiles = ResourceController.getListItemFile(response);
                    if (resourceItemFiles != null) {
                        ResourceController.addResourceListToDatabase(resourceItemFiles);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    customProgressDialog.dismissCustomProgressDialog();
                }
            });

            MySingleton.getInstance(this).addToRequestQueue(stringRequest);
            customProgressDialog.showCustomProgressDialog();

        } catch (Exception ignored) {

        }


    }
}
