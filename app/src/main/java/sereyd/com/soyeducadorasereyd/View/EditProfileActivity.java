package sereyd.com.soyeducadorasereyd.View;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.User;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.AppHelper;
import sereyd.com.soyeducadorasereyd.Utils.CircleNetworkImageView;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.HandlerVolleyError;
import sereyd.com.soyeducadorasereyd.Utils.VolleyMultipartRequest;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class EditProfileActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.editTextToolbar)
    TextView textView;

    @NotEmpty(trim = true)
    @Email
    @BindView(R.id.editTextEmail)
    EditText editTextUserEmail;

    @NotEmpty(trim = true)
    @BindView(R.id.editTextName)
    EditText editTextName;

    @BindView(R.id.imageViewProfile)
    CircleNetworkImageView circleImageView;

    @BindView(R.id.buttonSave)
    Button button;

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setupView();
        validator = new Validator(this);
        validator.setValidationListener(this);

    }

    private void setupView() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }
        textView.setTypeface(FontUtil.getRegularFont(this));
        editTextName.setTypeface(FontUtil.getRegularFont(this));
        editTextUserEmail.setTypeface(FontUtil.getRegularFont(this));
        button.setTypeface(FontUtil.getRegularFont(this));

        editTextName.setText(UserController.getUser().getName());
        editTextUserEmail.setText(UserController.getUser().getEmail());

        try {


            ImageLoader mImageLoader = MySingleton.getInstance(this).getImageLoader();
            final String url = UserController.getUser().getLinkImage();
            mImageLoader.get(url, ImageLoader.getImageListener(circleImageView,
                    R.mipmap.icon_person_o, R.mipmap.icon_person_o));
            circleImageView.setImageUrl(url, mImageLoader);

        } catch (Exception ignored) {

        }
    }

    @Override
    public void onValidationSucceeded() {

        final Map<String, String> params = new HashMap<>();

        if (!editTextUserEmail.getText().toString().equals(UserController.getUser().getEmail())) {
            params.put("email", editTextUserEmail.getText().toString());
        } else {
            params.put("email", "");
        }


        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, Constants.URL_ENDPOINT_USER + UserController.getUser().getIdUser(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                if (UserController.isUserUpdateCorrectlye(response)) {

                    User user = User.findById(User.class, UserController.getUser().getId());
                    user.setEmail(editTextUserEmail.getText().toString());
                    user.setName(editTextName.getText().toString());

                    user.save();

                    new SweetAlertDialog(EditProfileActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.aviso))
                            .setContentText(getString(R.string.cambios_guardados))
                            .show();

                } else {

                    new SweetAlertDialog(EditProfileActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                String body = HandlerVolleyError.getErrorUpdateResponse(error, EditProfileActivity.this);
                new SweetAlertDialog(EditProfileActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(body != null ? body : HandlerVolleyError.getErrorMessage(error, EditProfileActivity.this))
                        .show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                params.put("name", editTextName.getText().toString());
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {

            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.buttonSave)
    public void save(View view) {
        validator.validate();
    }

    @OnClick(R.id.imageViewProfile)
    public void updateImageProfile(View view) {

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setMaxCropResultSize(1500, 1500)
                .start(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                circleImageView.setImageURI(resultUri);

                final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
                customProgressDialog.showCustomProgressDialog();
                String url = Constants.URL_ENDPOINT_USER_IMAGE + UserController.getUser().getIdUser();
                VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        customProgressDialog.dismissCustomProgressDialog();
                        String resultResponse = new String(response.data);
                        try {

                            JSONObject result = new JSONObject(resultResponse);
                            if (result.getInt("error") == 0) {

                                User user = User.findById(User.class, UserController.getUser().getId());
                                user.setLinkImage(Constants.URL_SERVER + result.getString("path"));
                                user.save();

                                new SweetAlertDialog(EditProfileActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText(getString(R.string.aviso))
                                        .setContentText(getString(R.string.cambios_guardados))
                                        .show();
                            } else {
                                new SweetAlertDialog(EditProfileActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText(getString(R.string.ops))
                                        .setContentText(getString(R.string.algo_salio_mal))
                                        .show();
                                circleImageView.setImageResource(R.mipmap.icon_person_o);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customProgressDialog.dismissCustomProgressDialog();
                        new SweetAlertDialog(EditProfileActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.algo_salio_mal))
                                .show();
                        circleImageView.setImageResource(R.mipmap.icon_person_o);
                    }
                }) {
                    @Override
                    protected Map<String, DataPart> getByteData() {
                        Map<String, DataPart> params = new HashMap<>();
                        params.put("profile_picture", new DataPart("profile_picture.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), circleImageView.getDrawable()), "image/jpeg"));
                        return params;
                    }
                };

                MySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);

            }
        }
    }
}
