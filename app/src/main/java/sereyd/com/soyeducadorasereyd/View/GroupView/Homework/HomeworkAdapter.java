package sereyd.com.soyeducadorasereyd.View.GroupView.Homework;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.HomeworkController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Homework;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

/**
 * Created by isaiascarreraventura on 23/01/17.
 */

public class HomeworkAdapter extends RecyclerView.Adapter<HomeworkAdapter.ViewHolder> {


    private List<Homework> homeworkList;
    private Context contextAdapter;

    public HomeworkAdapter(Context contextAdapter, List<Homework> homeworkList) {
        this.contextAdapter = contextAdapter;
        this.homeworkList = homeworkList;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return contextAdapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_homework, parent, false);

        // Return a new holder instance
        return new HomeworkAdapter.ViewHolder(contactView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Homework homework = homeworkList.get(position);
        TextView textViewTitle = holder.textViewName;
        TextView textViewDescription = holder.textViewDescriptionHomework;
        TextView textViewDate = holder.textViewDate;

        textViewDescription.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewTitle.setTypeface(FontUtil.getRegularFont(getContext()));
        textViewDate.setTypeface(FontUtil.getHairlineFont(getContext()));

        textViewTitle.setText(homework.getName());
        String date = homework.getYear() + "/" + (homework.getMonth()) + "/" + homework.getDay();
        textViewDate.setText("Para el día: " + date);
        textViewDescription.setText(homework.getDescription());

    }

    @Override
    public int getItemCount() {
        return homeworkList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewDetailHomework)
        TextView textViewName;
        @BindView(R.id.textViewDateHomework)
        TextView textViewDate;
        @BindView(R.id.textViewDescriptionHomework)
        TextView textViewDescriptionHomework;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.buttonRemoveHomework)
        public void removeHomeworkButtonClicked(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                removeHomework(position);
            }
        }

        @OnClick(R.id.buttonEditHomework)
        public void editHomeworkButtonClicked(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                editHomework(position);
            }
        }

        private void editHomework(final int position) {

            Intent intent = new Intent(contextAdapter, AddHomeworkActivity.class);
            intent.putExtra("homeworkToEdit", homeworkList.get(position).getId());
            intent.putExtra("positionRecycler", position);
            ((Activity) contextAdapter).startActivityForResult(intent, Constants.EDIT_ACTIVITY_RESULT);

        }


        private void removeHomework(final int position) {

            new SweetAlertDialog(contextAdapter, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getContext().getResources().getString(R.string.aviso))
                    .setContentText(getContext().getResources().getString(R.string.pregunta_tarea))
                    .setConfirmText(getContext().getResources().getString(R.string.dialog_ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            deleteHomework(position);
                        }
                    })
                    .show();
        }

    }

    private void deleteHomework(final int position) {

        final CustomProgressDialog customProgressDialog;
        customProgressDialog = new CustomProgressDialog(getContext());
        customProgressDialog.showCustomProgressDialog();

        String url = Constants.URL_ENDPOINT_HOMEWORK + homeworkList.get(position).getIdHomework();
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                customProgressDialog.dismissCustomProgressDialog();
                if (HomeworkController.deleteHomeworkResponse(response)) {

                    homeworkList.get(position).delete();
                    homeworkList.remove(position);
                    notifyItemRemoved(position);

                    new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                            .setContentText(getContext().getString(R.string.tarea_eliminada))
                            .setTitleText(getContext().getString(R.string.aviso))
                            .show();

                } else {

                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getContext().getString(R.string.ops))
                            .setContentText(getContext().getString(R.string.algo_salio_mal))
                            .show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getContext().getString(R.string.ops))
                        .setContentText(getContext().getString(R.string.algo_salio_mal))
                        .show();

            }
        });

        MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);

    }


}
