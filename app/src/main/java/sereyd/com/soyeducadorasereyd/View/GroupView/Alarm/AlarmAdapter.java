package sereyd.com.soyeducadorasereyd.View.GroupView.Alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.GenericClass.AlarmReceiver;
import sereyd.com.soyeducadorasereyd.Models.AlertReminder;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;

/**
 * Created by isaiascarreraventura on 18/01/17.
 */
public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.ViewHolder> {


    private List<AlertReminder> alertReminderList;
    private Context contextAdapter;

    public AlarmAdapter(Context context, List<AlertReminder> alertReminderList) {
        this.alertReminderList = alertReminderList;
        this.contextAdapter = context;
    }


    @Override
    public AlarmAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View alarmView = inflater.inflate(R.layout.item_alarm, parent, false);

        // Return a new holder instance
        return new ViewHolder(alarmView);
    }

    @Override
    public void onBindViewHolder(AlarmAdapter.ViewHolder holder, int position) {

        // Get the data model based on position
        AlertReminder alertReminder = alertReminderList.get(position);

        // Set item views based on your views and data model
        TextView textViewTitle = holder.textViewDetailAlert;
        textViewTitle.setText(alertReminder.getSubject());
        TextView textViewTime = holder.textViewTimeAlert;
        String timeAlert = String.valueOf(alertReminder.getYear() + "/" + (alertReminder.getMonth() + 1) + "/" + alertReminder.getDay() + " " + alertReminder.getHour() + ":" + alertReminder.getMinute());
        textViewTime.setText(timeAlert);
        TextView textViewDetail = holder.textViewDetailAlertText;
        textViewDetail.setText(alertReminder.getDescription());
        textViewTime.setTypeface(FontUtil.getRegularFont(contextAdapter));
        textViewTitle.setTypeface(FontUtil.getRegularFont(contextAdapter));
        textViewDetail.setTypeface(FontUtil.getRegularFont(contextAdapter));

    }

    @Override
    public int getItemCount() {
        return alertReminderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewDetailAlert)
        TextView textViewDetailAlert;

        @BindView(R.id.textViewTimeAlert)
        TextView textViewTimeAlert;

        @BindView(R.id.textViewDescriptionAlert)
        TextView textViewDetailAlertText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.buttonRemoveAlert)
        public void removeAlertButtonClicked(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                removeAlarm(position);
            }
        }

        @OnClick(R.id.buttonEditAlarm)
        public void editAlarmButtonClicked(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                editAlarm(position);
            }
        }

        private void editAlarm(int position) {

            Intent intent = new Intent(contextAdapter, AddAlarmActivity.class);
            intent.putExtra("alarmToEdit", alertReminderList.get(position).getId());
            intent.putExtra("positionRecycler", position);
            ((Activity) contextAdapter).startActivityForResult(intent, Constants.EDIT_ACTIVITY_RESULT);

        }

        private void removeAlarm(final int position) {

            new SweetAlertDialog(contextAdapter, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(contextAdapter.getResources().getString(R.string.aviso))
                    .setContentText(contextAdapter.getResources().getString(R.string.pregunta_recordatorio))
                    .setConfirmText(contextAdapter.getResources().getString(R.string.dialog_ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {


                            int idAlert = alertReminderList.get(position).getId().intValue();
                            Intent alarmIntent = new Intent(contextAdapter, AlarmReceiver.class);
                            alarmIntent.putExtra("idAlarmToTrigger", idAlert);

                            PendingIntent pi = PendingIntent.getBroadcast(contextAdapter, idAlert, alarmIntent, 0);
                            AlarmManager manager = (AlarmManager) contextAdapter.getSystemService(Context.ALARM_SERVICE);
                            manager.cancel(pi);
                            pi.cancel();

                            alertReminderList.get(position).delete();
                            alertReminderList.remove(position);
                            notifyItemRemoved(position);

                            new SweetAlertDialog(contextAdapter, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(contextAdapter.getResources().getString(R.string.aviso))
                                    .setContentText(contextAdapter.getResources().getString(R.string.recordatorio_eliminado))
                                    .setConfirmText(contextAdapter.getResources().getString(R.string.dialog_ok))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();

                            sDialog.dismissWithAnimation();
                        }
                    })
                    .show();
        }
    }


}
