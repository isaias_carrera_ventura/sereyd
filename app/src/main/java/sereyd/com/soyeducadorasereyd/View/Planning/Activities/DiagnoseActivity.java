package sereyd.com.soyeducadorasereyd.View.Planning.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.DiagnoseController;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.Controllers.TestController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Diagnose;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Models.TestModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Fragments.DiagnoseStudentFragment;

public class DiagnoseActivity extends AppCompatActivity {

    @BindView(R.id.toolbarDiagnose)
    Toolbar toolbar;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.diagnoseTitleView)
    TextView titleView;

    Long diagnoseId;
    Long myGroupId;

    List<Student> studentArrayList;
    List<TestModel> testModelList;
    List<String> idStudentList;
    private Diagnose diagnose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnose);

        diagnoseId = getIntent().getExtras().getLong("diagnose");
        diagnose = DiagnoseController.getDiagnoseById(diagnoseId);
        myGroupId = getIntent().getExtras().getLong("group");
        idStudentList = (List<String>) getIntent().getExtras().getSerializable("studentsID");
        testModelList = (List<TestModel>) getIntent().getExtras().getSerializable("test");

        if (idStudentList == null) {

            studentArrayList = StudentController.getStudents(myGroupId);

        } else {

            studentArrayList = TestController.getStudentArrayInDatabaseForPendingDiagnose(idStudentList, myGroupId);

        }

        setCustomView();

    }

    private void setCustomView() {

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager.beginFakeDrag();
        setupViewPager(viewPager);
        titleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

    }


    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (Student student : studentArrayList) {

            Bundle bundle = new Bundle();
            bundle.putLong("idStudent", student.getId());
            bundle.putInt("position", studentArrayList.indexOf(student));
            bundle.putLong("diagnose", diagnoseId);
            bundle.putLong("group", myGroupId);
            bundle.putSerializable("test", (Serializable) testModelList);

            DiagnoseStudentFragment diagnoseFragment = new DiagnoseStudentFragment();
            diagnoseFragment.setArguments(bundle);
            adapter.addFragment(diagnoseFragment, "SAMPLE");

        }

        viewPager.setAdapter(adapter);

    }


    public void selectPage(int page) {

        if (page == studentArrayList.size()) {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(DiagnoseActivity.this);
            customProgressDialog.showCustomProgressDialog();

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, Constants.URL_ENDPOINT_TEST_FINISHED + diagnose.getIdGroupDiagnose(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    if (DiagnoseController.getStatusForCompletionDiagnose(response)) {

                        Diagnose diagnose = DiagnoseController.getDiagnoseById(diagnoseId);
                        diagnose.setState(Constants.COMPLETE);
                        diagnose.save();

                        new SweetAlertDialog(DiagnoseActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(getString(R.string.aviso))
                                .setContentText(getString(R.string.diagnostico_terminado))
                                .setConfirmText(getString(R.string.dialog_ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();

                                        finish();

                                    }
                                })
                                .show();

                    } else {

                        new SweetAlertDialog(DiagnoseActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.algo_salio_mal))
                                .show();

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(DiagnoseActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();

                }
            }) {

            };

            MySingleton.getInstance(DiagnoseActivity.this).addToRequestQueue(stringRequest);


        } else {

            for (int i = 0; i < testModelList.size(); i++) {
                testModelList.get(i).setResult(Constants.UNDEFINED);
            }
            viewPager.setCurrentItem(page, true);

        }
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                navigateUpFromActivity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void navigateUpFromActivity() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.aviso_diagnostico))
                .setContentText(getString(R.string.question_diagnostico_finish))
                .setConfirmText(getString(R.string.later))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        NavUtils.navigateUpFromSameTask(DiagnoseActivity.this);

                    }
                })
                .show();

    }

    @Override
    public void onBackPressed() {

        //super.onBackPressed();
        navigateUpFromActivity();

    }
}
