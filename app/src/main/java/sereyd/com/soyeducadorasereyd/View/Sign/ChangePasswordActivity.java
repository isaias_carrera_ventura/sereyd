package sereyd.com.soyeducadorasereyd.View.Sign;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.HandlerVolleyError;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class ChangePasswordActivity extends AppCompatActivity implements Validator.ValidationListener {

    @ConfirmPassword
    @BindView(R.id.editTextConfirmPassword)
    EditText editTextConfirmPassword;

    @Password(messageResId = R.string.field_required)
    @BindView(R.id.editTextNewPassword)
    EditText editTextNewPassword;

    @BindView(R.id.buttonRestore)
    Button buttonRestore;

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        setupView();
    }

    private void setupView() {

        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        editTextConfirmPassword.setTypeface(FontUtil.getRegularFont(this));
        editTextNewPassword.setTypeface(FontUtil.getRegularFont(this));
        buttonRestore.setTypeface(FontUtil.getRegularFont(this));

    }

    @OnClick(R.id.buttonRestore)
    public void buttonRestore(View view) {

        validator.validate();

    }

    @Override
    public void onValidationSucceeded() {

        final String password = editTextNewPassword.getText().toString();
        String url = Constants.URL_RESTORE_PASSWORD_CHANGE + UserController.getUser().getIdUser();

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                try {

                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getInt("error") == 0) {

                        new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Contraseña cambiada")
                                .setContentText("Cambiaste tu contraseña con éxito")
                                .show();

                        editTextConfirmPassword.getText().clear();
                        editTextNewPassword.getText().clear();

                    } else {
                        new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops!")
                                .setContentText("Algo salió mal\nNo pudimos cambiar tu contraseña.")
                                .show();
                    }

                } catch (Exception e) {
                    new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText("Algo salió mal")
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

                String body = HandlerVolleyError.getErrorUpdateResponse(error, ChangePasswordActivity.this);
                new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(body != null ? body : HandlerVolleyError.getErrorMessage(error, ChangePasswordActivity.this))
                        .show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("password", password);
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {

            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
