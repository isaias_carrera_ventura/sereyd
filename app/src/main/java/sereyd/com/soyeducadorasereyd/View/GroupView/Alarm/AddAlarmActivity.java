package sereyd.com.soyeducadorasereyd.View.GroupView.Alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.GenericClass.AlarmReceiver;
import sereyd.com.soyeducadorasereyd.Models.AlertReminder;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;

public class AddAlarmActivity extends Activity implements Validator.ValidationListener {

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.textViewAvisoJunta)
    TextView textViewAvisoJunta;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextAlertName)
    EditText editTextAlertName;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextAlertSubject)
    EditText editTextAlertDescription;

    @Pattern(regex = "^((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])$")
    @NotEmpty(trim = true)
    @BindView(R.id.editTextDayAlert)
    EditText editTextAlertDay;

    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.buttonAlertRegister)
    Button button;

    @Pattern(regex = "^([01]?[0-9]|2[0-3]):[0-5][0-9]$")
    @NotEmpty(trim = true, messageResId = R.string.field_required)
    @BindView(R.id.editTextTimeAlert)
    EditText editTextAlertTime;

    private Validator validator;

    private Calendar calendar;
    private int year, month, day, hour, minute;

    //PARAM TO SET CALENDAR
    private int yearAlarm, monthAlarm, dayAlarm, hourAlarm, minuteAlarm;

    MyGroup myGroup;
    Long idGroup;

    private Long idToEdit;
    private int positonRecyclerToReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_alarm);
        ButterKnife.bind(this);
        setCustomView();
        setCalendar();

        //GET GROUP TO ADJUNT ALARM
        idGroup = getIntent().getExtras().getLong("group");
        myGroup = MyGroup.findById(MyGroup.class, idGroup);

        idToEdit = getIntent().getLongExtra("alarmToEdit", -1);
        positonRecyclerToReturn = getIntent().getIntExtra("positionRecycler", -1);

        //Editon
        if (idToEdit != -1 && positonRecyclerToReturn != -1) {
            try {

                setAlarmToViewDetail(AlertReminder.findById(AlertReminder.class, idToEdit));

            } catch (Exception e) {

                new SweetAlertDialog(AddAlarmActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setConfirmText(getString(R.string.algo_salio_mal))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                finish();
                            }
                        })
                        .show();

            }
        }

    }

    private void setAlarmToViewDetail(AlertReminder alertReminder) {


        monthAlarm = alertReminder.getMonth() + 1;
        yearAlarm = alertReminder.getYear();
        dayAlarm = alertReminder.getDay();

        hourAlarm = alertReminder.getHour();
        minuteAlarm = alertReminder.getMinute();

        editTextAlertDay.setText(alertReminder.getYear() + "/" + (alertReminder.getMonth() + 1) + "/" + alertReminder.getDay());
        editTextAlertTime.setText(alertReminder.getHour() + ":" + alertReminder.getMinute());
        editTextAlertName.setText(alertReminder.getSubject());
        editTextAlertDescription.setText(alertReminder.getDescription());

    }

    private void setCalendar() {

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

    }

    private void setCustomView() {

        textViewAvisoJunta.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextAlertName.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextAlertDescription.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextAlertDay.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextAlertTime.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        button.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        validator = new Validator(this);
        validator.setValidationListener(this);

        editTextAlertDay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                    setDate();
                }

                return true;
            }
        });


        editTextAlertDay.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    setDate();
                }
            }
        });

        editTextAlertTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                    setTime();
                }

                return true;
            }
        });

        editTextAlertTime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    setTime();
                }
            }
        });

    }

    private void setTime() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(AddAlarmActivity.this, R.style.datepicker, myTimeListener, hour, minute, true);
        timePickerDialog.show();
    }

    private TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int i, int i1) {

            Date date = new Date();
            date.setHours(i);
            date.setMinutes(i1);
            SimpleDateFormat HHMM = new SimpleDateFormat("hh:mm");
            String time = HHMM.format(date);
            editTextAlertTime.setText(time);

            hourAlarm = i;
            minuteAlarm = i1;
        }
    };

    private void setDate() {

        DatePickerDialog dialog = new DatePickerDialog(this, R.style.datepicker, myDateListener, year, month, day);
        dialog.setTitle("");
        dialog.getDatePicker().setBackgroundColor(Color.WHITE);
        dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        dialog.show();

    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // arg1 = year arg2 = month arg3 = day
                    String date = arg1 + "/" + (arg2 + 1) + "/" + arg3;
                    editTextAlertDay.setText(date);

                    //SET PARAMS TO WORK WITH
                    yearAlarm = arg1;
                    monthAlarm = arg2;
                    dayAlarm = arg3;

                }
            };


    @OnClick(R.id.buttonAlertRegister)
    public void registerAlert(View view) {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {

        //Create alarm
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, monthAlarm);
        cal.set(Calendar.YEAR, yearAlarm);
        cal.set(Calendar.DAY_OF_MONTH, dayAlarm);
        cal.set(Calendar.HOUR_OF_DAY, hourAlarm);
        cal.set(Calendar.MINUTE, minuteAlarm);
        cal.set(Calendar.SECOND, 0);

        if (idToEdit == -1) {

            createAlarm(cal);

        } else {

            //DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Calendar calToday = Calendar.getInstance();
            Date dateCurrent = calToday.getTime();
            Date dateToSet = cal.getTime();

            boolean isCalendarCorrect = dateToSet.after(dateCurrent);

            if (isCalendarCorrect) {

                AlertReminder alertReminder = AlertReminder.findById(AlertReminder.class, idToEdit);
                Long idAlert = alertReminder.getId();

                //CANCEL ALARM
                Intent alarmIntent = new Intent(this, AlarmReceiver.class);
                alarmIntent.putExtra("idAlarmToTrigger", idAlert);
                PendingIntent pi = PendingIntent.getBroadcast(this, idAlert.intValue(), alarmIntent, 0);
                AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                manager.cancel(pi);
                pi.cancel();

                //CREATE NEW ALARM
                alarmIntent = new Intent(AddAlarmActivity.this, AlarmReceiver.class);
                alarmIntent.putExtra("idAlarmToTrigger", idAlert);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(AddAlarmActivity.this, idAlert.intValue(), alarmIntent, 0);
                manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);

                //RETURN ALARM TO UPDATE RECYCLER WITH NEW DATA AND SAVE IN ACTIVITY LIST
                AlertReminder newAlertReminder = new AlertReminder();
                newAlertReminder.setYear(yearAlarm);
                newAlertReminder.setMonth(monthAlarm);
                newAlertReminder.setDay(dayAlarm);
                newAlertReminder.setHour(hourAlarm);
                newAlertReminder.setMinute(minuteAlarm);
                newAlertReminder.setSubject(editTextAlertName.getText().toString());
                newAlertReminder.setDescription(editTextAlertDescription.getText().toString());

                Intent returnIntent = new Intent();
                returnIntent.putExtra("alarm", newAlertReminder);
                returnIntent.putExtra("positionRecycler", positonRecyclerToReturn);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();

            } else {

                new SweetAlertDialog(AddAlarmActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.date_incorrect))
                        .setConfirmText(getString(R.string.dialog_ok))
                        .show();

            }

        }

    }

    private void createAlarm(Calendar c) {

        Calendar calToday = Calendar.getInstance();
        Date dateCurrent = calToday.getTime();
        Date dateToSet = c.getTime();

        boolean isCalendarCorrect = dateToSet.after(dateCurrent);

        if (isCalendarCorrect) {
            AlertReminder alertReminder = new AlertReminder(yearAlarm, monthAlarm, dayAlarm, hourAlarm, minuteAlarm, editTextAlertName.getText().toString(), editTextAlertDescription.getText().toString(), myGroup);
            alertReminder.save();
            Long idAlert = alertReminder.getId();

            Intent alarmIntent = new Intent(AddAlarmActivity.this, AlarmReceiver.class);
            alarmIntent.putExtra("idAlarmToTrigger", idAlert);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(AddAlarmActivity.this, idAlert.intValue(), alarmIntent, 0);

            AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            manager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);

            new SweetAlertDialog(AddAlarmActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText(getString(R.string.recordatorio_creado))
                    .setConfirmText(getString(R.string.dialog_ok))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            finish();
                        }
                    })
                    .show();
        } else {
            new SweetAlertDialog(AddAlarmActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.date_incorrect))
                    .setConfirmText(getString(R.string.dialog_ok))
                    .show();

        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
