package sereyd.com.soyeducadorasereyd.View.Resources.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.ResourcesItem;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.DrawableResourceRandom;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.Resources.Activities.ResourcesListDetailActivity;

/**
 * Created by chaycv on 31/07/17.
 */

public class AdapterResourceContainer extends RecyclerView.Adapter<AdapterResourceContainer.MyViewHolder> {

    private List<ResourcesItem> itemResourceAuxes;
    private Context context;

    public AdapterResourceContainer(List<ResourcesItem> moviesList, Context context) {
        this.itemResourceAuxes = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_resource_container, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ResourcesItem item = itemResourceAuxes.get(position);
        holder.title.setText(item.getName());
        holder.title.setBackground(DrawableResourceRandom.getDrawableForRandom(context));
        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ResourcesListDetailActivity.class);
                intent.putExtra("id_resource", item.getId());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return itemResourceAuxes.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Button title;

        public MyViewHolder(View view) {
            super(view);
            title = (Button) view.findViewById(R.id.nameResource);
            title.setTypeface(FontUtil.getRegularFont(context));
        }
    }


}