package sereyd.com.soyeducadorasereyd.View.GenericView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import sereyd.com.soyeducadorasereyd.InicioActivity;
import sereyd.com.soyeducadorasereyd.View.GenericView.SlidePageTutorial.TutorialActivity;

/**
 * Created by isaiascarreraventura on 15/01/17.
 */
public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean tutorial = sharedPreferences.getBoolean("tutorialView", false);
        Intent intent;
        if (tutorial) {
            intent = new Intent(this, InicioActivity.class);
        } else {
            intent = new Intent(this, TutorialActivity.class);
        }

        startActivity(intent);
        finish();

    }
}
