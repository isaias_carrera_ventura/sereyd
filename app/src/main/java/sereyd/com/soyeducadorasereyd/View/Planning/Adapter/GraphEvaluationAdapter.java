package sereyd.com.soyeducadorasereyd.View.Planning.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.snatik.storage.Storage;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.EvaluationController;
import sereyd.com.soyeducadorasereyd.Controllers.PlanningController;
import sereyd.com.soyeducadorasereyd.Controllers.ResourceController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Evaluation;
import sereyd.com.soyeducadorasereyd.Models.ResultModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.InputStreamVolleyRequest;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.PlanningDetailActivity;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.PlanningViewActivity;

/**
 * Created by chaycv on 30/08/17.
 */

public class GraphEvaluationAdapter extends RecyclerView.Adapter<GraphEvaluationAdapter.MyViewHolder> {

    private List<ResultModel> resultList;
    private Context context;
    private List<Evaluation> evaluations;
    Activity activity;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public GraphEvaluationAdapter(Activity activity, Context context, List<ResultModel> diagnoseList, List<Evaluation> evaluations) {
        this.resultList = diagnoseList;
        this.evaluations = evaluations;
        this.context = context;
        this.activity = activity;
    }

    public Context getContext() {
        return this.context;
    }

    @Override
    public GraphEvaluationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_graph_evaluation, parent, false);
        return new GraphEvaluationAdapter.MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }

    @Override
    public void onBindViewHolder(GraphEvaluationAdapter.MyViewHolder holder, final int position) {

        holder.progressBar.setProgress(resultList.get(position).getPercent());
        holder.textTitle.setText(resultList.get(position).getName());
        holder.textViewPercentage.setText(resultList.get(position).getPercent() + "%");
        holder.textViewdateEvaluation.setText(resultList.get(position).getLevel());
        holder.buttonExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // Check if we have write permission
                int permission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if (permission != PackageManager.PERMISSION_GRANTED) {

                    // We don't have permission so prompt the user
                    ActivityCompat.requestPermissions(
                            activity,
                            PERMISSIONS_STORAGE,
                            REQUEST_EXTERNAL_STORAGE);

                } else {

                    new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Exportar evaluación")
                            .setContentText("Se exportará tu evaluación a Excel")
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    requestForDownloadPlanningFile(position);
                                }
                            })
                            .setCancelText("Cancelar")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .show();


                }

            }
        });

    }

    private void requestForDownloadPlanningFile(final int position) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                        activity,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );


            } else {

                new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getContext().getResources().getString(R.string.aviso))
                        .setContentText(getContext().getResources().getString(R.string.access_files))
                        .setConfirmText("Si").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", activity.getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);


                    }
                }).show();


            }

        } else {


            final Storage storage = new Storage(getContext());
            final String path = storage.getExternalStorageDirectory();
            final String newDir = path + File.separator + "evaluations";
            storage.createDirectory(newDir);

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getContext());
            customProgressDialog.showCustomProgressDialog();

            final String url = Constants.URL_EVALUATION_FILE + evaluations.get(position).getEvaluationGroupId();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    try {

                        JSONObject jsonObject = new JSONObject(response);

                        if (jsonObject.getInt("error") == 0) {

                            final CustomProgressDialog customProgressDialog2 = new CustomProgressDialog(getContext());
                            customProgressDialog2.showCustomProgressDialog();
                            String urlFile = Constants.URL_SERVER + "/" + jsonObject.getString("path");
                            //DOWNLOAD File
                            InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, urlFile,
                                    new Response.Listener<byte[]>() {
                                        @Override
                                        public void onResponse(byte[] response) {

                                            customProgressDialog2.dismissCustomProgressDialog();
                                            try {
                                                if (response != null) {

                                                    storage.createFile(EvaluationController.getFileExcelPathForPlanning(newDir, evaluations.get(position)), response);
                                                    //Attach file
                                                    String filePathExcel = EvaluationController.getFileAbsoluteFileExcelPathForEvaluation(evaluations.get(position), getContext());
                                                    File fileExcel = new File(filePathExcel);
                                                    Uri pathExcel = Uri.fromFile(fileExcel);

                                                    //Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                                                    // set the type to 'email'
                                                    emailIntent.setType("vnd.android.cursor.dir/email");
                                                    String to[] = {};
                                                    emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                                                    // the attachment
                                                    emailIntent.putExtra(Intent.EXTRA_STREAM, pathExcel);

                                                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Evaluación");
                                                    context.startActivity(Intent.createChooser(emailIntent, "Enviar evaluación vía e-mail..."));

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                                        .setTitleText(getContext().getResources().getString(R.string.ops))
                                                        .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                                                        .show();
                                            }
                                        }
                                    }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    error.printStackTrace();
                                    customProgressDialog2.dismissCustomProgressDialog();
                                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(getContext().getResources().getString(R.string.ops))
                                            .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                                            .show();
                                }
                            }, null);

                            request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            MySingleton.getInstance(getContext()).addToRequestQueue(request);


                        } else {
                            new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText(getContext().getResources().getString(R.string.ops))
                                    .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                                    .show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getContext().getResources().getString(R.string.ops))
                                .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                                .show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getContext().getResources().getString(R.string.ops))
                            .setContentText(getContext().getResources().getString(R.string.algo_salio_mal))
                            .show();
                }
            });

            MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);

        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewNameEvaluation)
        TextView textTitle;

        @BindView(R.id.textViewPercetage)
        TextView textViewPercentage;

        @BindView(R.id.progressBarEvaluation)
        ProgressBar progressBar;

        @BindView(R.id.dateEvaluation)
        TextView textViewdateEvaluation;

        @BindView(R.id.buttonExportEvaluation)
        Button buttonExport;

        public MyViewHolder(View view) {

            super(view);
            ButterKnife.bind(this, view);

            textTitle.setTypeface(FontUtil.getRegularFont(getContext()));
            textViewPercentage.setTypeface(FontUtil.getRegularFont(getContext()));
            textViewdateEvaluation.setTypeface(FontUtil.getRegularFont(getContext()));
        }

    }

}
