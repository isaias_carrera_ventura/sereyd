package sereyd.com.soyeducadorasereyd.View.Planning.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.snatik.storage.Storage;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.PaymentController;
import sereyd.com.soyeducadorasereyd.Controllers.PlanningController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.PaymentObjectSereyd;
import sereyd.com.soyeducadorasereyd.Models.Planning;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.CircleNetworkImageView;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.InputStreamVolleyRequest;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.PaymentActivitySereyd;

public class PlanningDetailActivity extends AppCompatActivity {

    @BindView(R.id.textViewDescriptionPlanning)
    TextView textViewDescriptionPlanning;
    @BindView(R.id.planningTitleView)
    TextView planningTitleView;
    @BindView(R.id.buttonStartPlanning)
    Button buttonStartPlanning;
    @BindView(R.id.toolbarPlanningDetail)
    Toolbar toolbar;
    @BindView(R.id.premiumIndicator)
    ImageView imageViewPremiumIndicator;
    @BindView(R.id.imagePlanning)
    NetworkImageView imageView;
    @BindView(R.id.planeacionTextview)
    TextView textViewName;
    Planning planning;

    int actionPlanning = 20;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning_detail);
        setCustomView();
        setSupportActionBar(toolbar);

        planning = (Planning) getIntent().getSerializableExtra("planning");

        try {

            ImageLoader mImageLoader = MySingleton.getInstance(this).getImageLoader();
            String url = planning.getUrlImage();
            mImageLoader.get(url, ImageLoader.getImageListener(imageView, R.mipmap.logoapp, R.mipmap.logoapp));
            imageView.setImageUrl(url, mImageLoader);

        } catch (Exception ignored) {

        }

        setupViewWithPlanning(planning);

        if (PlanningController.isPlanningInDatabase(planning)) {
            buttonStartPlanning.setText(getString(R.string.ver_plan));
        }


    }

    private void setupViewWithPlanning(Planning planning) {

        textViewDescriptionPlanning.setText(planning.getDescription());
        planningTitleView.setText(planning.getName());
        imageViewPremiumIndicator.setVisibility(planning.getType() == Constants.PREMIUM ? View.VISIBLE : View.GONE);

    }

    private void setCustomView() {

        ButterKnife.bind(this);
        textViewDescriptionPlanning.setMovementMethod(new ScrollingMovementMethod());
        textViewDescriptionPlanning.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        buttonStartPlanning.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        planningTitleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewName.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

    }

    @OnClick(R.id.buttonStartPlanning)
    public void actionPlanning(View view) {

        if (PlanningController.isPlanningInDatabase(planning)) {

            if (!PlanningController.isPlanningFilePDFInDisk(planning, this)) {

                new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Aviso")
                        .setContentText(getString(R.string.suggesst_planning_download))
                        .setConfirmText("Descargar")
                        .setCancelText("Ver")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                sweetAlertDialog.dismissWithAnimation();
                                Intent i = new Intent(PlanningDetailActivity.this, PlanningViewActivity.class);
                                i.putExtra("planning", planning);
                                startActivity(i);

                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {

                                sDialog.dismissWithAnimation();
                                requestForDownloadPlanningFile();


                            }
                        })
                        .show();

            } else {

                Intent i = new Intent(this, PlanningViewActivity.class);
                i.putExtra("planning", planning);
                startActivity(i);

            }

        } else {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
            customProgressDialog.showCustomProgressDialog();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_PAYMENT + UserController.getUser().getIdUser(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    try {

                        PaymentObjectSereyd paymentObjectSereyd = PaymentController.getPaymentStatus(response);
                        if (paymentObjectSereyd != null) {

                            if (paymentObjectSereyd.getCodeError() == Constants.ACTIVE_SUSCRIPTION) {

                                if (paymentObjectSereyd.getPlanningCount() > 0) {

                                    //TODO: Download planning
                                    suggestAssociatePlanningForUser(paymentObjectSereyd.getPlanningCount());

                                } else {

                                    new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                                            .setTitleText("Oops!")
                                            .setContentText("Espera tu siguiente fecha de corte")
                                            .setConfirmText("Ok")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {

                                                    sDialog.dismissWithAnimation();

                                                }
                                            })
                                            .show();

                                }

                            } else if (paymentObjectSereyd.getCodeError() == Constants.TEST_PERIOD) {

                                if (paymentObjectSereyd.getPlanningCount() > 0) {

                                    if (planning.getType() != Constants.PREMIUM) {

                                        suggestAssociatePlanningForUser(paymentObjectSereyd.getPlanningCount());

                                    } else {

                                        new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText(getString(R.string.ops))
                                                .setContentText("Este contenido es PREMIUM, suscríbete para poder acceder a más planeaciones y recursos.")
                                                .show();

                                    }

                                } else {

                                    new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                                            .setTitleText("Oops!")
                                            .setContentText("Tu periodo de prueba ha finalizado " + getString(R.string.playment_sugg))
                                            .setConfirmText("Suscribir")
                                            .setCancelText("Cancelar")
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sweetAlertDialog.dismissWithAnimation();
                                                }
                                            })
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {

                                                    sDialog.dismissWithAnimation();
                                                    startActivity(new Intent(PlanningDetailActivity.this, PaymentActivitySereyd.class));

                                                }
                                            })
                                            .show();

                                }

                            } else if (paymentObjectSereyd.getCodeError() == Constants.PAYMENT_REQUIRED) {

                                String errorDescription = "Tu suscripción ha caducado, vuélvete PREMIUM y disfruta de tus planeaciones y recursos.";
                                new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Oops!")
                                        .setContentText(errorDescription)
                                        .setConfirmText("Suscribir")
                                        .setCancelText("Cancelar")
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismissWithAnimation();
                                            }
                                        })
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {

                                                sDialog.dismissWithAnimation();
                                                startActivity(new Intent(PlanningDetailActivity.this, PaymentActivitySereyd.class));

                                            }
                                        })
                                        .show();


                            }

                        } else {

                            new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText(getString(R.string.ops))
                                    .setContentText(getString(R.string.algo_salio_mal))
                                    .show();

                        }


                    } catch (Exception e) {
                        new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.algo_salio_mal))
                                .show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    customProgressDialog.dismissCustomProgressDialog();

                    new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();


                }
            });

            MySingleton.getInstance(this).addToRequestQueue(stringRequest);

        }


    }

    private void suggestAssociatePlanningForUser(int remaining) {

        new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Aviso")
                .setContentText(getString(R.string.buy_planning) + "\n" + getString(R.string.faltantes) + remaining)
                .setConfirmText("Si")
                .setCancelText("Cancelar")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                        requestForDownloadPlanning();

                    }
                })
                .show();
    }

    @OnClick(R.id.buttonPreviewPlanning)
    public void previewPlanning(View view) {

        Intent intent = new Intent(this, PreviewPlanningActivity.class);
        intent.putExtra("planning", planning);
        startActivity(intent);

    }

    private void requestForDownloadPlanning() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_PLANIFICATION_USER_DOWNLOAD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                if (PlanningController.asignPlanningToDatabase(planning, response)) {

                    new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText(getString(R.string.aviso))
                            .setContentText(getString(R.string.download_success))
                            .show();

                    buttonStartPlanning.setText(getString(R.string.ver_plan));

                } else {

                    new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", UserController.getUser().getIdUser());
                params.put("id_planification", String.valueOf(planning.getIdPlanning()));
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == actionPlanning) {
            if (resultCode == Activity.RESULT_OK) {

                int result = data.getIntExtra("result", -1);
                switch (result) {

                    case Constants.EXPORT_OPTION:
                        break;

                    case Constants.SHARE_OPTION:

                        shareByEmail();

                        break;

                    default:
                        break;
                }

            }
        }
    }

    private void shareByEmail() {

        if (PlanningController.isPlanningInDatabase(planning)
                && PlanningController.isPlanningFilePDFInDisk(planning, this) && PlanningController.isPlanningFileWordInDisk(planning, this)) {

            String filePathPDF = PlanningController.getFileAbsoluteFilePDFPathForPlanning(planning, this);
            File filePDF = new File(filePathPDF);
            Uri pathPDF = Uri.fromFile(filePDF);

            String filePathWord = PlanningController.getFileAbsoluteFileWordPathForPlanning(planning, this);
            File fileWord = new File(filePathWord);
            Uri pathWord = Uri.fromFile(fileWord);

            //Intent emailIntent = new Intent(Intent.ACTION_SEND);
            Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            // set the type to 'email'
            emailIntent.setType("vnd.android.cursor.dir/email");
            String to[] = {};
            emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
            // the attachment

            ArrayList<Uri> uriList = new ArrayList<>();
            uriList.add(pathPDF);
            uriList.add(pathWord);
            emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriList);

            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Planeación");
            startActivity(Intent.createChooser(emailIntent, "Enviar planeación vía e-mail..."));

        } else {


            new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.adquirir_download))
                    .show();

        }

    }

    @OnClick(R.id.buttonActionDownload)
    public void buttonActionDownload(View view) {

        if (PlanningController.isPlanningInDatabase(planning)) {

            if (PlanningController.isPlanningFilePDFInDisk(planning, this) && PlanningController.isPlanningFileWordInDisk(planning, this)) {

                new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.file_alredy_downloaded))
                        .show();

            } else {

                // Check if we have write permission
                int permission = ActivityCompat.checkSelfPermission(PlanningDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if (permission != PackageManager.PERMISSION_GRANTED) {

                    // We don't have permission so prompt the user
                    ActivityCompat.requestPermissions(
                            PlanningDetailActivity.this,
                            PERMISSIONS_STORAGE,
                            REQUEST_EXTERNAL_STORAGE
                    );

                } else {

                    requestForDownloadPlanningFile();

                }


            }

        } else {

            new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(getString(R.string.download_first_planning))
                    .show();

        }

    }

    @OnClick(R.id.buttonAction)
    public void buttonAction(View view) {

        Intent i = new Intent(this, ActionPlanningActivity.class);
        startActivityForResult(i, actionPlanning);

    }

    private void requestForDownloadPlanningFile() {

        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(PlanningDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(PlanningDetailActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                        PlanningDetailActivity.this,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );


            } else {

                new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getString(R.string.aviso))
                        .setContentText(getString(R.string.access_files))
                        .setConfirmText("Si").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);


                    }
                }).show();


            }

        } else {
            new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.aviso))
                    .setContentText(getString(R.string.download_file_planning))
                    .setConfirmText("Si")
                    .setCancelText("Cancelar")
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {

                            sDialog.dismissWithAnimation();

                            final Storage storage = new Storage(getApplicationContext());
                            final String path = storage.getExternalStorageDirectory();
                            final String newDir = path + File.separator + "plannings";
                            storage.createDirectory(newDir);
                            String mUrl = planning.getUrlPDF();

                            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(PlanningDetailActivity.this);
                            customProgressDialog.showCustomProgressDialog();

                            InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, mUrl,
                                    new Response.Listener<byte[]>() {

                                        @Override
                                        public void onResponse(final byte[] responsePDF) {

                                            try {

                                                if (responsePDF != null) {

                                                    String mUrlWord = planning.getUrlWord();

                                                    //DOWNLOAD WORD
                                                    InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, mUrlWord,
                                                            new Response.Listener<byte[]>() {

                                                                @Override
                                                                public void onResponse(byte[] response) {

                                                                    customProgressDialog.dismissCustomProgressDialog();
                                                                    try {
                                                                        if (response != null) {

                                                                            storage.createFile(PlanningController.getFilePDFPathForPlanning(newDir, planning), responsePDF);
                                                                            storage.createFile(PlanningController.getFileWordPathForPlanning(newDir, planning), response);

                                                                            new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                                                    .setTitleText(getString(R.string.aviso))
                                                                                    .setContentText(getString(R.string.download_success))
                                                                                    .show();

                                                                        }
                                                                    } catch (Exception e) {
                                                                        new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                                                .setTitleText(getString(R.string.ops))
                                                                                .setContentText(getString(R.string.algo_salio_mal))
                                                                                .show();
                                                                    }
                                                                }
                                                            }, new Response.ErrorListener() {

                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            error.printStackTrace();

                                                            customProgressDialog.dismissCustomProgressDialog();
                                                            new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                                    .setTitleText(getString(R.string.ops))
                                                                    .setContentText(getString(R.string.algo_salio_mal))
                                                                    .show();
                                                        }
                                                    }, null);
                                                    request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                                    MySingleton.getInstance(PlanningDetailActivity.this).addToRequestQueue(request);

                                                }
                                            } catch (Exception e) {

                                                customProgressDialog.dismissCustomProgressDialog();
                                                new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                        .setTitleText(getString(R.string.ops))
                                                        .setContentText(getString(R.string.algo_salio_mal))
                                                        .show();
                                            }
                                        }
                                    }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();
                                    customProgressDialog.dismissCustomProgressDialog();
                                    new SweetAlertDialog(PlanningDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(getString(R.string.ops))
                                            .setContentText(getString(R.string.algo_salio_mal))
                                            .show();
                                }
                            }, null);
                            request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            MySingleton.getInstance(PlanningDetailActivity.this).addToRequestQueue(request);

                        }
                    })
                    .show();


        }


    }


}
