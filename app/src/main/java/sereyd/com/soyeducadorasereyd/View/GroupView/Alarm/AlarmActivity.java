package sereyd.com.soyeducadorasereyd.View.GroupView.Alarm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Models.AlertReminder;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;

public class AlarmActivity extends AppCompatActivity {


    Long idGroup;

    @BindView(R.id.alertsRecycleView)
    RecyclerView alertsRecycler;
    @BindView(R.id.toolbarAlarm)
    Toolbar toolbar;
    @BindView(R.id.misAvisos)
    TextView textViewTitleView;
    List<AlertReminder> alertReminderList = new ArrayList<>();
    AlarmAdapter alarmAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_alarm);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }
        textViewTitleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        idGroup = getIntent().getExtras().getLong("group");
        //MyGroup myGroup = MyGroup.findById(MyGroup.class, idGroup);
        //alertReminderList = AlertReminder.getAlerts(idGroup);
        alarmAdapter = new AlarmAdapter(this, alertReminderList);
        alertsRecycler.setAdapter(alarmAdapter);
        alertsRecycler.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onStart() {

        super.onStart();
        alertReminderList.clear();
        alertReminderList.addAll(AlertReminder.getAlerts(idGroup));
        alarmAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == Constants.EDIT_ACTIVITY_RESULT) {

                int positionRecyclerChanged = data.getIntExtra("positionRecycler", -1);
                AlertReminder newAlertReminder = (AlertReminder) data.getSerializableExtra("alarm");

                AlertReminder alertReminderInDisk = alertReminderList.get(positionRecyclerChanged);
                alertReminderInDisk.setYear(newAlertReminder.getYear());
                alertReminderInDisk.setMonth(newAlertReminder.getMonth());
                alertReminderInDisk.setDay(newAlertReminder.getDay());
                alertReminderInDisk.setHour(newAlertReminder.getHour());
                alertReminderInDisk.setMinute(newAlertReminder.getMinute());
                alertReminderInDisk.setSubject(newAlertReminder.getSubject());
                alertReminderInDisk.setDescription(newAlertReminder.getDescription());
                alertReminderInDisk.save();

                alertReminderList.set(positionRecyclerChanged, alertReminderInDisk);
                alarmAdapter.notifyDataSetChanged();

                new SweetAlertDialog(AlarmActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(getString(R.string.aviso))
                        .setContentText(getString(R.string.recordatorio_editado))
                        .setConfirmText(getString(R.string.dialog_ok))
                        .show();

            }
        }
    }
}
