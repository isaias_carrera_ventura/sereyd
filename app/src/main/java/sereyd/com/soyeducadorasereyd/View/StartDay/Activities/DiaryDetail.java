package sereyd.com.soyeducadorasereyd.View.StartDay.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.Models.Diary;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;

public class DiaryDetail extends AppCompatActivity {

    Long idDiary;
    Diary diary;

    @BindView(R.id.textViewTitleDetailDairy)
    TextView textViewTitleDetailDairy;

    @BindView(R.id.textViewDiaryEntrance)
    TextView textViewDiaryEntrance;

    @BindView(R.id.textViewDiarySubject)
    TextView textViewDiarySubject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_diary_detail);

        diary = (Diary) getIntent().getExtras().get("diary");
        setupView(diary);

    }

    private void setupView(Diary diary) {
        ButterKnife.bind(this);
        textViewTitleDetailDairy.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewTitleDetailDairy.setText(getResources().getString(R.string.diario) + ": " + diary.getDate());
        textViewDiaryEntrance.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewDiaryEntrance.setText(getResources().getString(R.string.diario) + ": " + diary.getTitle());
        textViewDiarySubject.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewDiarySubject.setText(diary.getText());
    }
}
