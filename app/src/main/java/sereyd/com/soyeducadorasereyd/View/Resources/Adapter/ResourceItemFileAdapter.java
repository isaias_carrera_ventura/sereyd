package sereyd.com.soyeducadorasereyd.View.Resources.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sereyd.com.soyeducadorasereyd.Controllers.ResourceController;
import sereyd.com.soyeducadorasereyd.Models.ResourceItemFile;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.Resources.Activities.ResourceViewActivity;

/**
 * Created by chaycv on 04/08/17.
 */

public class ResourceItemFileAdapter extends RecyclerView.Adapter<ResourceItemFileAdapter.MyViewHolder> {

    private Context context;
    private List<ResourceItemFile> itemFiles;


    public ResourceItemFileAdapter(List<ResourceItemFile> moviesList, Context context) {
        this.itemFiles = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_resource, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final ResourceItemFile itemFile = itemFiles.get(position);
        holder.title.setText(itemFile.getName());
        holder.imageViewDownload.setVisibility(ResourceController.isResourceInDatabase(itemFile) ? View.VISIBLE : View.INVISIBLE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ResourceViewActivity.class);
                intent.putExtra("resource", itemFile);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return itemFiles.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView imageViewDownload;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.textViewResource);
            imageViewDownload = (ImageView) view.findViewById(R.id.planning_download_indicator);
            title.setTypeface(FontUtil.getRegularFont(context));
        }
    }
}