package sereyd.com.soyeducadorasereyd.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import mx.openpay.android.Openpay;
import mx.openpay.android.OperationCallBack;
import mx.openpay.android.OperationResult;
import mx.openpay.android.exceptions.OpenpayServiceException;
import mx.openpay.android.exceptions.ServiceUnavailableException;
import mx.openpay.android.model.Card;
import mx.openpay.android.validation.CardValidator;
import sereyd.com.soyeducadorasereyd.Controllers.PlanningController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class OpenPayPaymentActivity extends AppCompatActivity implements OperationCallBack {

    Openpay openpay;
    int paymentTypeSuscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_pay_payment);
        ButterKnife.bind(this);
        paymentTypeSuscription = getIntent().getExtras().getInt("type");
        this.openpay = new Openpay("mbljemtdh8osybgrddpt", "pk_3ac1af73f8bb48f49260ea17dee82c3d", true);

    }

    @OnClick(R.id.buttonPayment)
    public void doPayment(View view) {

        this.addToken();
    }

    private void addToken() {

        Card card = new Card();
        boolean isValid = true;
        String errorDescription = "";

        final EditText holderNameEt = ((EditText) this.findViewById(R.id.holder_name));
        final String holderName = holderNameEt.getText().toString();
        card.holderName(holderName);

        if (!CardValidator.validateHolderName(holderName)) {
            errorDescription += this.getString(R.string.invalid_holder_name) + "\n";
            holderNameEt.setError(this.getString(R.string.invalid_holder_name));
            isValid = false;
        }

        final EditText cardNumberEt = ((EditText) this.findViewById(R.id.card_number));
        final String cardNumber = cardNumberEt.getText().toString();
        card.cardNumber(cardNumber);
        if (!CardValidator.validateNumber(cardNumber)) {
            errorDescription += this.getString(R.string.invalid_card_number) + "\n";
            cardNumberEt.setError(this.getString(R.string.invalid_card_number));
            isValid = false;
        }

        EditText cvv2Et = ((EditText) this.findViewById(R.id.cvv2));
        String cvv = cvv2Et.getText().toString();
        card.cvv2(cvv);
        if (!CardValidator.validateCVV(cvv, cardNumber)) {
            errorDescription += this.getString(R.string.invalid_cvv) + "\n";
            cvv2Et.setError(this.getString(R.string.invalid_cvv));
            isValid = false;
        }

        Integer year = this.getInteger(((Spinner) this.findViewById(R.id.year_spinner)).getSelectedItem().toString());

        Integer month = this.getInteger(((Spinner) this.findViewById(R.id.month_spinner)).getSelectedItem().toString());

        if (!CardValidator.validateExpiryDate(month, year)) {

            errorDescription += "Fecha inválida" + "\n";
            isValid = false;

        }

        card.expirationMonth(month);
        card.expirationYear(year);

        if (isValid) {

            Toast.makeText(this, "Procesando pago...", Toast.LENGTH_SHORT).show();
            openpay.createToken(card, this);

        } else {

            new SweetAlertDialog(OpenPayPaymentActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getString(R.string.ops))
                    .setContentText(errorDescription)
                    .show();

        }

    }


    private Integer getInteger(final String number) {

        try {

            return Integer.valueOf(number);

        } catch (NumberFormatException nfe) {

            return 0;

        }

    }

    @Override
    public void onError(OpenpayServiceException error) {


        int desc = 0;
        String msg = null;

        switch (error.getErrorCode()) {
            case 3001:
                desc = R.string.declined;
                msg = this.getString(desc);
                break;
            case 3002:
                desc = R.string.expired;
                msg = this.getString(desc);
                break;
            case 3003:
                desc = R.string.insufficient_funds;
                msg = this.getString(desc);
                break;
            case 3004:
                desc = R.string.stolen_card;
                msg = this.getString(desc);
                break;
            case 3005:
                desc = R.string.suspected_fraud;
                msg = this.getString(desc);
                break;

            case 2002:
                desc = R.string.already_exists;
                msg = this.getString(desc);
                break;
            default:
                desc = R.string.error_creating_card;
                msg = error.getDescription();
        }

        new SweetAlertDialog(OpenPayPaymentActivity.this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(getString(R.string.ops))
                .setContentText(msg)
                .show();

    }

    @Override
    public void onCommunicationError(ServiceUnavailableException error) {

        new SweetAlertDialog(OpenPayPaymentActivity.this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(getString(R.string.ops))
                .setContentText("No nos pudimos comunicar con el servidor, inténtalo de nuevo.")
                .show();

    }


    @Override
    public void onSuccess(OperationResult operationResult) {

        try {

            final String idDevice = openpay.getDeviceCollectorDefaultImpl().setup(this);
            String tokenResult = operationResult.getResult().toString();
            int initPositionT = tokenResult.indexOf("=") + 1;
            int lastPositionT = tokenResult.indexOf(",");
            final String tokenString = tokenResult.substring(initPositionT, lastPositionT);

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_PAYMENT_CARD + UserController.getUser().getIdUser(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    if (PlanningController.isPaymentCorrect(response)) {

                        Intent data = new Intent();
                        setResult(RESULT_OK, data);
                        finish();

                    } else {

                        new SweetAlertDialog(OpenPayPaymentActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getString(R.string.ops))
                                .setContentText(getString(R.string.algo_salio_mal))
                                .show();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    customProgressDialog.dismissCustomProgressDialog();

                    new SweetAlertDialog(OpenPayPaymentActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();

                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("type", String.valueOf(paymentTypeSuscription));
                    params.put("token_id", tokenString);
                    params.put("device_session_id", idDevice);
                    return params;
                }
            };

            MySingleton.getInstance(this).addToRequestQueue(stringRequest);
            customProgressDialog.showCustomProgressDialog();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
