package sereyd.com.soyeducadorasereyd.View.ShopSereyd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.TrustManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.SsX509TrustManager;
import sereyd.com.soyeducadorasereyd.Utils.SslHttpClient;

public class WebShopActivity extends AppCompatActivity {

    private static String TAG = "TAG";
/*

    @BindView(R.id.webview)
    AdvancedWebView mWebView;
*/

    @BindView(R.id.webViewShop)
    WebView webView;

    @BindView(R.id.progressBarWebView)
    ProgressBar progressBar;

    @BindView(R.id.toolbarWebView)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web_shop);
        ButterKnife.bind(this);

        setUpWebViewShop();

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setUpWebViewShop() {

        setSupportActionBar(toolbar);

        progressBar.setMax(100);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(Constants.URL_SHOP);
        /*webView.setWebViewClient(new MyWebViewClientDeprecated());*/
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                progressBar.setProgress(newProgress);

                if (newProgress == 100) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
        progressBar.setProgress(0);

    }



}
