package sereyd.com.soyeducadorasereyd.View.Planning.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.PlanningController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Planning;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Adapter.PlanningAdapter;


public class PlanningFragment extends Fragment {

    private static final String TAG = "RecyclerViewFragment";
    //private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private static final int ROM_NUMBER = 2;
    protected RecyclerView mRecyclerView;
    private List<Planning> planningList = new ArrayList<>();
    protected RecyclerView.LayoutManager mLayoutManager;
    private PlanningAdapter planningAdapter;

    public PlanningFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_planning, container, false);
        rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerPlanning);


        //Search View
        setHasOptionsMenu(true);
        //Get planning in database
        planningList = PlanningController.getPlanningInDatabase();

        planningAdapter = new PlanningAdapter(getActivity(), planningList);
        mLayoutManager = new GridLayoutManager(getActivity().getApplication(), ROM_NUMBER);
        mRecyclerView.setAdapter(planningAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        return rootView;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {

        super.onStart();
        if (!getUserVisibleHint()) {
            return;
        }

        makeRequestToServerForUserPlanning();

    }

    private void makeRequestToServerForUserPlanning() {

        if (UserController.getUser() != null) {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getContext());
            customProgressDialog.showCustomProgressDialog();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_PLANIFICATION_USER + UserController.getUser().getIdUser(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();
                    List<Planning> planningListFromServer = PlanningController.getPlanningFromResponse(response);

                    if (planningListFromServer != null && PlanningController.getPlanningInDatabase().size() != planningListFromServer.size()) {

                        PlanningController.savePlanningListToDatabase(planningListFromServer);
                        planningList.clear();
                        planningList.addAll(PlanningController.getPlanningInDatabase());
                        planningAdapter.notifyDataSetChanged();

                    }

                    makeRequestToServer();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    customProgressDialog.dismissCustomProgressDialog();
                   /* new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();*/

                    makeRequestToServer();

                }
            });

            MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
        }


    }

    public void makeRequestToServer() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getContext());
        customProgressDialog.showCustomProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_PLANIFICATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                List<Planning> planningListFromServer = PlanningController.getPlanningFromResponse(response);
                if (planningListFromServer != null) {

                    planningList.clear();
                    planningList.addAll(PlanningController.getListForMissingPlanning(planningListFromServer));
                    planningAdapter.notifyDataSetChanged();

                } else {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        });

        MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);

    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onStart();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_search, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);

        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);

        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(getResources().getColor(R.color.colorWhite));
        searchAutoComplete.setTextColor(getResources().getColor(R.color.colorWhite));


        ImageView searchIcon = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
        searchIcon.setImageResource(R.mipmap.ic_action_search);


        search(searchView);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);

    }


    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                planningAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }
}