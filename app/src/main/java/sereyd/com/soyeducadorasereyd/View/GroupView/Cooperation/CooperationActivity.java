package sereyd.com.soyeducadorasereyd.View.GroupView.Cooperation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.Controllers.CooperationController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Cooperation;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

public class CooperationActivity extends AppCompatActivity {

    MyGroup myGroup;
    Long idGroup;

    @BindView(R.id.misCooperaciones)
    TextView titleView;

    @BindView(R.id.cooperationsRecycleView)
    RecyclerView cooperationRecyclerView;

    @BindView(R.id.toolbarCooperaciones)
    Toolbar toolbar;

    List<Cooperation> cooperationList;
    CooperationAdapter cooperationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_cooperation);

        ButterKnife.bind(this);

        titleView.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }

        idGroup = getIntent().getExtras().getLong("group");
        myGroup = MyGroup.findById(MyGroup.class, idGroup);

        cooperationList = CooperationController.getCooperations(idGroup);
        cooperationAdapter = new CooperationAdapter(this, cooperationList);
        cooperationRecyclerView.setAdapter(cooperationAdapter);
        cooperationRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onResume() {
        super.onResume();
        requestForGroupCooperations();
    }

    private void requestForGroupCooperations() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_GROUP + myGroup.getIdGpr(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        customProgressDialog.dismissCustomProgressDialog();
                        List<Cooperation> list = CooperationController.getListFromResponse(response);
                        if (list != null) {

                            cooperationList.clear();
                            CooperationController.saveListCooperationInDatabase(list, idGroup);
                            list = CooperationController.getCooperations(idGroup);
                            cooperationList.addAll(list);
                            cooperationAdapter.notifyDataSetChanged();

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();

            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == Constants.EDIT_ACTIVITY_RESULT) {

                int positionRecyclerChanged = data.getIntExtra("positionRecycler", -1);
                Cooperation newCooperation = (Cooperation) data.getSerializableExtra("cooperation");

                Cooperation cooperationInDisk = cooperationList.get(positionRecyclerChanged);
                cooperationInDisk.setConcept(newCooperation.getConcept());
                cooperationInDisk.setAmmount(newCooperation.getAmmount());
                cooperationInDisk.setDescription(newCooperation.getDescription());
                cooperationInDisk.save();

                Toast.makeText(getApplicationContext(), R.string.cooperacion_editada, Toast.LENGTH_SHORT).show();
                cooperationList.set(positionRecyclerChanged, cooperationInDisk);
                cooperationAdapter.notifyDataSetChanged();

            }
        }
    }

}
