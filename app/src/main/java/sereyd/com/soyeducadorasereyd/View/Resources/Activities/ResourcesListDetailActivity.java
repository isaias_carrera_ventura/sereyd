package sereyd.com.soyeducadorasereyd.View.Resources.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.ResourceController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.ResourceItemFile;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Resources.Adapter.ResourceItemFileAdapter;

public class ResourcesListDetailActivity extends AppCompatActivity {

    @BindView(R.id.resourcesTextview)
    TextView resourcesTextview;
    @BindView(R.id.toolbarResourcesDetail)
    Toolbar toolbar;
    @BindView(R.id.resourcesRecycler)
    RecyclerView recyclerView;

    String idResource;
    private ResourceItemFileAdapter mAdapter;
    private List<ResourceItemFile> resourceItemFilesFromServer = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resources_list_detail);
        setupView();
        idResource = getIntent().getExtras().getString("id_resource");

        mAdapter = new ResourceItemFileAdapter(resourceItemFilesFromServer, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    protected void onStart() {

        super.onStart();
        requestForResourcesInSection(idResource);

    }

    private void requestForResourcesInSection(String idResource) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.showCustomProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_RESOURCE + "/" + idResource, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                customProgressDialog.dismissCustomProgressDialog();
                List<ResourceItemFile> resourceItemFiles = ResourceController.getListItemFile(response);
                if (resourceItemFiles != null) {

                    resourceItemFilesFromServer.clear();
                    resourceItemFilesFromServer.addAll(resourceItemFiles);
                    mAdapter.notifyDataSetChanged();

                } else {
                    new SweetAlertDialog(ResourcesListDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getString(R.string.ops))
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(ResourcesListDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(getString(R.string.ops))
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void setupView() {

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }

        resourcesTextview.setTypeface(FontUtil.getRegularFont(this));

    }
}