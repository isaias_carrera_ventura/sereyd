package sereyd.com.soyeducadorasereyd.View;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.InicioActivity;
import sereyd.com.soyeducadorasereyd.Models.PaymentInformation;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Sign.ChangePasswordActivity;

public class ConfigurationActivity extends AppCompatActivity {

    @BindView(R.id.toolbarConfig)
    Toolbar toolbarConfig;
    @BindView(R.id.configTitleView)
    TextView textViewConfig;
    @BindView(R.id.textViewCerrarSesion)
    TextView textViewCerrarSesion;
    @BindView(R.id.textViewAvisoPrivacidad)
    TextView textViewAvisoPrivacidad;
    @BindView(R.id.textViewFAQ)
    TextView textViewFAQ;
    @BindView(R.id.textViewInfoPayment)
    TextView textViewInfoPayment;
    @BindView(R.id.textViewChangePassword)
    TextView textViewChangePassword;

    /*
    @BindView(R.id.paymentDescriptionLabel)
    TextView textViewDescriptionStatus;
    @BindView(R.id.textViewDescriptionPaymentLabel)
    TextView textViewDescriptionPaymentLabel;
    @BindView(R.id.textViewDatePayment)
    TextView textViewDatePayment;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_configuration);
        setupView();

    }

    private void setupView() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbarConfig);
        textViewConfig.setTypeface(FontUtil.getRegularFont(this));
        textViewCerrarSesion.setTypeface(FontUtil.getRegularFont(this));
        textViewFAQ.setTypeface(FontUtil.getRegularFont(this));
        textViewAvisoPrivacidad.setTypeface(FontUtil.getRegularFont(this));
        textViewChangePassword.setTypeface(FontUtil.getRegularFont(this));
        textViewInfoPayment.setTypeface(FontUtil.getRegularFont(this));
    }

    private void closeSession() {
        UserController.deleteSession(getApplicationContext());
        Intent intent = new Intent(getApplicationContext(), InicioActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.cerrarSesion)
    public void cerrarSesionMethod(View view) {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(this.getResources().getString(R.string.aviso))
                .setContentText(this.getResources().getString(R.string.cerrar_sesion_pregunta))
                .setConfirmText(this.getResources().getString(R.string.dialog_ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        closeSession();
                    }
                })
                .setCancelText(this.getResources().getString(R.string.cancelar))
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();

    }

    @OnClick(R.id.faqs)
    public void faq() {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.FAQ_URL));
        startActivity(browserIntent);

    }

    @OnClick(R.id.infoPayment)
    public void infoPayment() {
        startActivity(new Intent(this, PaymentInformationActivity.class));
    }

    @OnClick(R.id.terms)
    public void terms() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TERMS_URL));
        startActivity(browserIntent);
    }

    @OnClick(R.id.changePassword)
    public void changePassword(View view) {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }
}
