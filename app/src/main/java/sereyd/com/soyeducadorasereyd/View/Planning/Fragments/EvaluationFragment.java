package sereyd.com.soyeducadorasereyd.View.Planning.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.EvaluationController;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.GenericClass.RecyclerViewDiagnoseSelection;
import sereyd.com.soyeducadorasereyd.Models.Evaluation;
import sereyd.com.soyeducadorasereyd.Models.EvaluationSelection;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.TestModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.Planning.Activities.EvaluationActivity;
import sereyd.com.soyeducadorasereyd.View.Planning.Adapter.EvaluationAdapter;
import sereyd.com.soyeducadorasereyd.View.Planning.Adapter.EvaluationSelectionAdapter;

/**
 * Created by isaiascarreraventura on 27/02/17.
 * Project SoyeducadoraSEREYD
 */

public class EvaluationFragment extends Fragment implements RecyclerViewDiagnoseSelection {


    @BindView(R.id.spinnerGroupEvaluation)
    Spinner spinnerGroup;
    @BindView(R.id.textViewSeleccionPrueba)
    TextView textViewSeleccionPrueba;
    @BindView(R.id.textViewResultadoGrupo)
    TextView textViewResultadoGrupo;
    @BindView(R.id.buttonStartEvaluation)
    Button buttonStartEvaluation;

    @BindView(R.id.recycler_view_evaluation_selection)
    RecyclerView recyclerViewSelection;

    @BindView(R.id.recyclerViewEvaluations)
    RecyclerView recyclerViewEvaluations;
    EvaluationAdapter evaluationAdapter;
    List<MyGroup> groupToPopulate = new ArrayList<>();

    ArrayList<EvaluationSelection> evaluationSelections;
    ArrayList<MyGroup> groupArrayList;
    private EvaluationSelectionAdapter diagnoseSelectionAdapter;
    private int positionSelectForEvaluation = -1;

    public EvaluationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_evaluation, container, false);
        groupArrayList = GroupController.getCourses();
        setupView(view);
        return view;
    }

    private void setupView(View view) {

        ButterKnife.bind(this, view);
        textViewSeleccionPrueba.setTypeface(FontUtil.getRegularFont(getActivity()));
        textViewResultadoGrupo.setTypeface(FontUtil.getRegularFont(getActivity()));
        buttonStartEvaluation.setTypeface(FontUtil.getRegularFont(getActivity()));
        //SPINNER
        ArrayList<String> stringList = new ArrayList<>();
        stringList.add(getContext().getResources().getString(R.string.grupo_el));
        for (MyGroup mygrop : groupArrayList) {
            stringList.add(groupArrayList.indexOf(mygrop) + 1, groupArrayList.get(groupArrayList.indexOf(mygrop)).getGrade() + groupArrayList.get(groupArrayList.indexOf(mygrop)).getGroupLetter());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, stringList);
        spinnerGroup.setAdapter(adapter);

        //EVALUATIONS
        evaluationAdapter = new EvaluationAdapter(groupToPopulate, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewEvaluations.setLayoutManager(mLayoutManager);
        recyclerViewEvaluations.setItemAnimator(new DefaultItemAnimator());
        recyclerViewEvaluations.setAdapter(evaluationAdapter);

        //RECYCLER VIEW SELECTION
        evaluationSelections = new ArrayList<>();
        diagnoseSelectionAdapter = new EvaluationSelectionAdapter(evaluationSelections, getActivity(), this);
        RecyclerView.LayoutManager mLayoutManagerSelection = new LinearLayoutManager(getContext());
        recyclerViewSelection.setLayoutManager(mLayoutManagerSelection);
        recyclerViewSelection.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSelection.setAdapter(diagnoseSelectionAdapter);


    }


    @Override
    public void onStart() {

        super.onStart();
        if (!getUserVisibleHint()) {
            return;
        }

        groupToPopulate.clear();
        evaluationAdapter.notifyDataSetChanged();

        requestForAvailableEvaluations();
        fetchGroupEvaluation();

    }

    private void fetchGroupEvaluation() {

        List<MyGroup> myGroups = GroupController.getCourses();

        for (final MyGroup myGroup : myGroups) {

            final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getActivity());
            customProgressDialog.showCustomProgressDialog();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_EVALUATION_PENDING + myGroup.getIdGpr(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    List<Evaluation> evaluationList = EvaluationController.getListEvaluationPending(response, myGroup);

                    if (evaluationList != null) {

                        groupToPopulate.add(myGroup);
                        EvaluationController.saveListEvaluation(evaluationList, myGroup.getId());
                        evaluationAdapter.notifyDataSetChanged();

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    customProgressDialog.dismissCustomProgressDialog();
                }
            });

            MySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);


        }

    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onStart();
        }
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {

        positionSelectForEvaluation = position;

    }

    private void requestForAvailableEvaluations() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getActivity());
        customProgressDialog.showCustomProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_EVALUATION_USER + UserController.getUser().getIdUser(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();
                List<EvaluationSelection> list = EvaluationController.getEvaluationForSelection(response);
                if (list != null) {

                    if (list.size() > 0) {

                        evaluationSelections.clear();
                        evaluationSelections.addAll(list);
                        diagnoseSelectionAdapter.notifyDataSetChanged();
                    } else {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops!")
                                .setContentText(getResources().getString(R.string.no_evaluation))
                                .show();
                    }

                } else {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getResources().getString(R.string.error_general))
                            .show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getResources().getString(R.string.error_general))
                        .show();

            }
        });

        MySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);

    }

    @OnClick(R.id.buttonStartEvaluation)
    public void startEvaluation(View view) {

        if (spinnerGroup.getSelectedItemPosition() != Constants.NO_ELEMENTS && positionSelectForEvaluation != -1) {

            MyGroup myGroup = groupArrayList.get(spinnerGroup.getSelectedItemPosition() - 1);
            downloadAndCreateGroupEvaluation(myGroup, evaluationSelections.get(positionSelectForEvaluation).getIdEvaluation());

            /*MyGroup myGroup = groupArrayList.get(spinnerGroup.getSelectedItemPosition() - 1);
            int numberOfStudentsInGroupToStart = StudentController.getStudentNumberInGroup(myGroup.getId());
            if (numberOfStudentsInGroupToStart > NO_ELEMENTS) {

                if (!Diagnose.incompleteDiagnoseByGroup(myGroup.getId())) {

                    downloadAndCreateGroupDiagnostic(myGroup, diagnoseSelections.get(positionSelectForDiagnostic).getIdDiagnose());

                } else {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getResources().getString(R.string.grupo_nofinalizado))
                            .show();
                }
            } else {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getResources().getString(R.string.grupo_noalumnos))
                        .show();
            }*/

        } else {

            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops!")
                    .setContentText(getResources().getString(R.string.grupo_ele) + "\n" + getResources().getString(R.string.test_selection))
                    .show();
        }

    }

    private void downloadAndCreateGroupEvaluation(final MyGroup myGroup, final String idEvaluation) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getActivity());
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_ENDPOINT_EVALUATION + idEvaluation, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                List<TestModel> testModelList = EvaluationController.getEvaluationTestFromResponse(response);
                if (testModelList != null) {

                    requestForAssociateGroupEvaluation(myGroup, testModelList, idEvaluation);


                } else {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getResources().getString(R.string.error_general))
                            .show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getResources().getString(R.string.error_general))
                        .show();

            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);

    }

    private void requestForAssociateGroupEvaluation(final MyGroup myGroup, final List<TestModel> testModelList, final String idEvaluation) {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(getActivity());
        customProgressDialog.showCustomProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_GROUP_EVALUATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                Evaluation evaluation = EvaluationController.getEvaluationAssociateForGroup(response);

                if (evaluation != null) {

                    evaluation.setMyGroup(myGroup);
                    evaluation.save();

                    Intent i = new Intent(getContext(), EvaluationActivity.class);
                    i.putExtra("evaluation", evaluation.getId());
                    i.putExtra("group", myGroup.getId());
                    i.putExtra("test", (Serializable) testModelList);
                    startActivity(i);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                error.printStackTrace();
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getResources().getString(R.string.error_general))
                        .show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_group", String.valueOf(myGroup.getIdGpr()));
                params.put("id_evaluation", idEvaluation);
                return params;
            }
        };

        MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

}
