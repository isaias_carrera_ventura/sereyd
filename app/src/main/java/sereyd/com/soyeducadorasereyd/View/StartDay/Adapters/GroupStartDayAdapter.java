package sereyd.com.soyeducadorasereyd.View.StartDay.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.StartDay.Activities.StartDayEvaluatingActivity;

/**
 * Created by chaycv on 20/04/17.
 */

public class GroupStartDayAdapter extends RecyclerView.Adapter<GroupStartDayAdapter.MyViewHolder> {

    private List<MyGroup> groupList;
    private Context context;

    public GroupStartDayAdapter(List<MyGroup> moviesList, Context context) {
        this.groupList = moviesList;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_group_start_day, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        //Populate view

        holder.textViewNameGroup.setText(groupList.get(position).getGrade() + groupList.get(position).getGroupLetter());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Long idGroup = groupList.get(position).getId();

                if (StudentController.getStudentNumberInGroup(idGroup) > 0) {

                    Intent i = new Intent(getContext(), StartDayEvaluatingActivity.class);
                    i.putExtra("group", idGroup);
                    getContext().startActivity(i);
                    ((Activity) getContext()).finish();

                } else {
                    Toast.makeText(getContext(), R.string.grupo_noalumnos, Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewGroupName)
        TextView textViewNameGroup;


        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            textViewNameGroup.setTypeface(FontUtil.getRegularFont(getContext()));

        }

    }

}
