package sereyd.com.soyeducadorasereyd.View.StartDay.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.StartDay.Adapters.GroupStartDayAdapter;

public class StartDayActivity extends AppCompatActivity {

    @BindView(R.id.textViewComienzaTuDia)
    TextView textViewComienza;
    @BindView(R.id.textViewSelectGrupo)
    TextView textViewSelectGpr;
    @BindView(R.id.textViewFraseDia)
    TextView textViewFraseDia;
    @BindView(R.id.imageViewChanginIcon)
    ImageView imageViewChanginIcon;
    @BindView(R.id.recycler_view_grupo)
    RecyclerView recyclerViewGrupo;

    int[] imageChanginId = {R.mipmap.icon_c_uno,
            R.mipmap.icon_c_dos,
            R.mipmap.icon_c_tres,
            R.mipmap.icon_c_cuatro,
            R.mipmap.icon_c_cinco};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_day);
        setUpView();
    }

    private void setUpView() {

        ButterKnife.bind(this);
        textViewComienza.setTypeface(FontUtil.getBoldFont(this));
        textViewSelectGpr.setTypeface(FontUtil.getHairlineFont(this));
        textViewFraseDia.setTypeface(FontUtil.getRegularFont(this));
        textViewFraseDia.setText(getMessageForWelcome());
        imageViewChanginIcon.setBackgroundResource(imageChanginId[getRandomImage()]);
        List<MyGroup> list = MyGroup.listAll(MyGroup.class);
        GroupStartDayAdapter mAdapter = new GroupStartDayAdapter(list, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewGrupo.setLayoutManager(mLayoutManager);
        recyclerViewGrupo.setItemAnimator(new DefaultItemAnimator());
        recyclerViewGrupo.setAdapter(mAdapter);

    }

    @OnClick(R.id.buttonCloseStart)
    public void closeActivity(View view) {
        this.finish();
    }

    public String getMessageForWelcome() {

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            return getResources().getString(R.string.buenos_dias);

        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            return getResources().getString(R.string.buenas_tardes);

        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            return getResources().getString(R.string.buenas_tardes);

        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            return getResources().getString(R.string.buenas_noches);
        }

        return null;

    }

    public int getRandomImage() {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(5);
    }
}
