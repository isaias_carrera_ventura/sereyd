package sereyd.com.soyeducadorasereyd.View.Forum.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.ForumQuestionModel;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.Forum.DetailQuestionForumActivity;

/**
 * Created by chaycv on 18/12/17.
 */

public class ForumAdapter extends RecyclerView.Adapter<ForumAdapter.MyViewHolder> {

    private List<ForumQuestionModel> forumQuestionModels;
    private Context context;

    public ForumAdapter(List<ForumQuestionModel> diaryList, Context context) {
        this.forumQuestionModels = diaryList;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public ForumAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_question_forum, parent, false);

        return new ForumAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final ForumAdapter.MyViewHolder holder, final int position) {

        holder.titleQuestion.setText(forumQuestionModels.get(position).getTitle());
        holder.contentQuestion.setText(forumQuestionModels.get(position).getQuestion());

        ImageLoader mImageLoader = MySingleton.getInstance(getContext()).getImageLoader();
        final String url = forumQuestionModels.get(position).getImageUser();
        mImageLoader.get(url, ImageLoader.getImageListener(holder.imageViewProfile, R.mipmap.person_gray, R.mipmap.person_gray));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), DetailQuestionForumActivity.class);
                intent.putExtra("question", forumQuestionModels.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return forumQuestionModels.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contentQuestion)
        TextView contentQuestion;
        @BindView(R.id.titleQuestion)
        TextView titleQuestion;

        @BindView(R.id.imageViewProfile)
        NetworkImageView imageViewProfile;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            contentQuestion.setTypeface(FontUtil.getRegularFont(getContext()));
            titleQuestion.setTypeface(FontUtil.getRegularFont(getContext()));
        }

    }


}
