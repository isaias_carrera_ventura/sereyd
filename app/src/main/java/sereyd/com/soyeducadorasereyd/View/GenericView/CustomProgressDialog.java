package sereyd.com.soyeducadorasereyd.View.GenericView;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import sereyd.com.soyeducadorasereyd.R;

/**
 * Created by isaiascarreraventura on 23/12/16.
 */
public class CustomProgressDialog {

    private ProgressDialog progressDialog;

    public CustomProgressDialog(Context context) {
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(false);
    }


    public void showCustomProgressDialog(){

        this.progressDialog.show();
        this.progressDialog.setContentView(R.layout.custom_progress_dialog);
        this.progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

    public void dismissCustomProgressDialog(){
        // dismiss the dialog after the file was downloaded
        this.progressDialog.dismiss();
    }

}

