package sereyd.com.soyeducadorasereyd.View.GroupView.Cooperation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.CooperationController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.Cooperation;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;

/**
 * Created by isaiascarreraventura on 22/01/17.
 */

public class CooperationAdapter extends RecyclerView.Adapter<CooperationAdapter.ViewHolder> {

    List<Cooperation> cooperationList;
    Context contextAdapter;

    public CooperationAdapter(Context contextAdapter, List<Cooperation> cooperationList) {
        this.cooperationList = cooperationList;
        this.contextAdapter = contextAdapter;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return contextAdapter;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_cooperation, parent, false);

        // Return a new holder instance
        return new CooperationAdapter.ViewHolder(contactView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // Get the data model based on position
        Cooperation cooperation = cooperationList.get(position);

        TextView title = holder.textViewDetail;
        TextView ammount = holder.textViewAmmount;
        TextView description = holder.textViewDescriptionCooperation;

        title.setTypeface(FontUtil.getRegularFont(contextAdapter));
        title.setTypeface(FontUtil.getRegularFont(contextAdapter));
        description.setTypeface(FontUtil.getRegularFont(contextAdapter));

        title.setText(cooperation.getConcept());
        ammount.setText("$ " + cooperation.getAmmount());
        description.setText(cooperation.getDescription());

    }

    @Override
    public int getItemCount() {
        return cooperationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textViewDetailCooperation)
        TextView textViewDetail;
        @BindView(R.id.textViewAmmountCooperation)
        TextView textViewAmmount;
        @BindView(R.id.textViewDescriptionCooperation)
        TextView textViewDescriptionCooperation;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.buttonRemoveCooperation)
        public void removeCooperationButtonClicked(View view) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {

                new SweetAlertDialog(contextAdapter, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getContext().getResources().getString(R.string.aviso))
                        .setContentText(getContext().getResources().getString(R.string.pregunta_cooperacion))
                        .setConfirmText(getContext().getResources().getString(R.string.dialog_ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                removeCooperation(position);
                            }
                        })
                        .show();

            }
        }

        private void removeCooperation(final int position) {

            final CustomProgressDialog customProgressDialog;
            customProgressDialog = new CustomProgressDialog(getContext());
            customProgressDialog.showCustomProgressDialog();

            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, Constants.URL_ENDPOINT_COOPERATION + cooperationList.get(position).getId_cooperation(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    customProgressDialog.dismissCustomProgressDialog();

                    if (CooperationController.deleteCooperationResponse(response)) {

                        cooperationList.get(position).delete();
                        cooperationList.remove(position);
                        notifyItemRemoved(position);

                        new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                                .setContentText(getContext().getString(R.string.cooperacion_eliminada))
                                .setTitleText(getContext().getString(R.string.aviso))
                                .show();
                    } else {

                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getContext().getString(R.string.ops))
                                .setContentText(getContext().getString(R.string.algo_salio_mal))
                                .show();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    customProgressDialog.dismissCustomProgressDialog();
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getContext().getString(R.string.ops))
                            .setContentText(getContext().getString(R.string.algo_salio_mal))
                            .show();
                }
            });

            MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);


        }

        @OnClick(R.id.buttonEditCooperation)
        public void editCooperationButtonClicked(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                editCooperation(position);
            }
        }

        void editCooperation(final int position) {

            Intent intent = new Intent(contextAdapter, AddCooperationActivity.class);
            intent.putExtra("cooperationToEdit", cooperationList.get(position).getId());
            intent.putExtra("positionRecycler", position);
            ((Activity) contextAdapter).startActivityForResult(intent, Constants.EDIT_ACTIVITY_RESULT);
        }

    }
}
