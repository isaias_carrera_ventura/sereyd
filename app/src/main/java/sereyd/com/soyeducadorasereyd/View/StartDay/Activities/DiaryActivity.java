package sereyd.com.soyeducadorasereyd.View.StartDay.Activities;

import android.os.Bundle;
import android.support.v4.util.LogWriter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.api.client.json.Json;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.DiaryController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.InicioActivity;
import sereyd.com.soyeducadorasereyd.Models.Diary;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.R;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.CustomDateSereyd;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.HandlerVolleyError;
import sereyd.com.soyeducadorasereyd.View.GenericView.CustomProgressDialog;
import sereyd.com.soyeducadorasereyd.View.StartDay.Adapters.DiaryAdapter;

public class DiaryActivity extends AppCompatActivity implements Validator.ValidationListener {

    @BindView(R.id.toolbarDiary)
    Toolbar toolbar;
    @BindView(R.id.startDayEvaluationTitleView)
    TextView textViewTitle;
    @BindView(R.id.textViewDateDiary)
    TextView textViewDateDiary;
    @BindView(R.id.textViewDiaryHistory)
    TextView textViewDiaryHistory;

    @BindView(R.id.editTextTitleDiary)
    @NotEmpty(trim = true)
    EditText editTextTitle;

    @NotEmpty(trim = true)
    @BindView(R.id.editTextDiarySubject)
    EditText editTextSubject;

    @BindView(R.id.buttonDiaryRegister)
    Button buttonREgister;
    private Validator validator;

    @BindView(R.id.recyclerViewDiaries)
    RecyclerView recyclerViewDiaries;
    DiaryAdapter mAdapter;
    ArrayList<Diary> diaries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);

        validator = new Validator(this);
        validator.setValidationListener(this);

        setupView();

        diaries = new ArrayList<>();
        mAdapter = new DiaryAdapter(diaries, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewDiaries.setLayoutManager(mLayoutManager);
        recyclerViewDiaries.setItemAnimator(new DefaultItemAnimator());
        recyclerViewDiaries.setAdapter(mAdapter);

        getDiaryEntries();

    }

    private void setupView() {

        ButterKnife.bind(this);

        textViewTitle.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewDateDiary.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextTitle.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        editTextSubject.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        buttonREgister.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textViewDateDiary.setText(CustomDateSereyd.getDate());
        textViewDiaryHistory.setTypeface(FontUtil.getRegularFont(getApplicationContext()));

    }

    @Override
    public void onValidationSucceeded() {

        final CustomProgressDialog customProgressDialog = new CustomProgressDialog(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_DIARY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                customProgressDialog.dismissCustomProgressDialog();

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("error") == 0) {

                        new SweetAlertDialog(DiaryActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(getString(R.string.add_diary))
                                .show();

                        editTextSubject.getText().clear();
                        editTextTitle.getText().clear();

                        getDiaryEntries();

                    } else {

                        new SweetAlertDialog(DiaryActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops!")
                                .setContentText(getString(R.string.algo_salio_mal))
                                .show();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    new SweetAlertDialog(DiaryActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText(getString(R.string.algo_salio_mal))
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                customProgressDialog.dismissCustomProgressDialog();
                new SweetAlertDialog(DiaryActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(getString(R.string.algo_salio_mal))
                        .show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("title", editTextTitle.getText().toString());
                params.put("description", editTextSubject.getText().toString());
                params.put("id_user", UserController.getUser().getIdUser());
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        customProgressDialog.showCustomProgressDialog();

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.buttonDiaryRegister)
    public void vaidate() {
        validator.validate();
    }

    public void getDiaryEntries() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_DIARY + "/" + UserController.getUser().getIdUser(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                List<Diary> diariesServer = DiaryController.getListDiaryFromResponse(response);

                if (diariesServer != null) {

                    diaries.clear();
                    diaries.addAll(diariesServer);
                    mAdapter.notifyDataSetChanged();

                    DiaryController.saveListForDiary(diariesServer);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                List<Diary> diariesInDisk = DiaryController.getListDiaryInDisk();
                if (diariesInDisk != null) {

                    diaries.clear();
                    diaries.addAll(diariesInDisk);
                    mAdapter.notifyDataSetChanged();


                }
                error.printStackTrace();

            }
        });

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

}
