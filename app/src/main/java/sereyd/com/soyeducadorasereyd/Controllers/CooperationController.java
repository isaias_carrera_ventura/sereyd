package sereyd.com.soyeducadorasereyd.Controllers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.Cooperation;
import sereyd.com.soyeducadorasereyd.Models.Homework;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;

/**
 * Created by chaycv on 27/06/17.
 */

public class CooperationController {

    public static void saveCooperationToDatabase(Cooperation cooperation) {
        cooperation.save();
    }

    public static ArrayList<Cooperation> getCooperations(Long id) {
        return (ArrayList<Cooperation>) Cooperation.find(Cooperation.class, "MY_GROUP = ?", String.valueOf(id));
    }

    public static Cooperation getCooperationFromResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONObject jsonObjectCooperation = jsonObject.getJSONObject("cooperation");
            String idCooperation = String.valueOf(jsonObjectCooperation.getInt("id_cooperation"));
            String concept = jsonObjectCooperation.getString("concept");
            String description = jsonObjectCooperation.getString("description");
            Double amount = Double.valueOf(jsonObjectCooperation.getString("amount"));

            return new Cooperation(idCooperation, concept, description, amount, null);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static List<Cooperation> getListFromResponse(String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                JSONArray jsonArray = jsonObjectData.getJSONArray("cooperations");
                List<Cooperation> cooperationList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObjectCooperation = jsonArray.getJSONObject(i);
                    String idCooperation = jsonObjectCooperation.getString("id_cooperation");
                    String concept = jsonObjectCooperation.getString("concept");
                    String description = jsonObjectCooperation.getString("description");
                    Double amount = Double.valueOf(jsonObjectCooperation.getString("amount"));

                    cooperationList.add(new Cooperation(idCooperation, concept, description, amount, null));

                }

                return cooperationList;

            } else return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void saveListCooperationInDatabase(List<Cooperation> cooperationListFromServer, Long idGroup) {

        MyGroup myGroup = MyGroup.findById(MyGroup.class, idGroup);
        List<Cooperation> cooperationInDisk = getCooperations(idGroup);

        if (cooperationListFromServer.size() > cooperationInDisk.size()) {
            for (int i = 0; i < cooperationListFromServer.size(); i++) {

                boolean isCooperationInDisk = false;
                Cooperation cooperation = cooperationListFromServer.get(i);

                for (int j = 0; j < cooperationInDisk.size(); j++) {

                    if (cooperation.getId_cooperation().equals(cooperationInDisk.get(j).getId_cooperation())) {
                        isCooperationInDisk = true;
                    }

                }

                if (!isCooperationInDisk) {

                    cooperation.setMyGroup(myGroup);
                    saveCooperationToDatabase(cooperation);

                }

            }
        } else if (cooperationListFromServer.size() < cooperationInDisk.size()) {

            for (int i = 0; i < cooperationInDisk.size(); i++) {

                boolean isCooperationInDisk = false;
                Cooperation cooperationInDB = cooperationInDisk.get(i);

                for (int j = 0; j < cooperationListFromServer.size(); j++) {

                    if (cooperationInDB.getId_cooperation().equals(cooperationListFromServer.get(j).getId_cooperation())) {
                        isCooperationInDisk = true;
                    }

                }

                if (!isCooperationInDisk) {

                    cooperationInDB.delete();

                }

            }

        }

        saveListForUpdatedObjects(getCooperations(idGroup), cooperationListFromServer);

    }

    private static void saveListForUpdatedObjects(ArrayList<Cooperation> cooperationInDisk, List<Cooperation> cooperationListFromServer) {

        for (int i = 0; i < cooperationInDisk.size(); i++) {

            Cooperation cooperationToEdit = Cooperation.findById(Cooperation.class, cooperationInDisk.get(i).getId());
            Cooperation cooperationAux = getCooperationFromServerById(cooperationToEdit.getId_cooperation(), cooperationListFromServer);

            if (cooperationAux != null) {

                cooperationToEdit.setAmmount(cooperationAux.getAmmount());
                cooperationToEdit.setConcept(cooperationAux.getConcept());
                cooperationToEdit.setDescription(cooperationAux.getDescription());
                cooperationToEdit.save();

            }
        }

    }

    private static Cooperation getCooperationFromServerById(String idCooperation, List<Cooperation> cooperationList) {

        for (int i = 0; i < cooperationList.size(); i++) {

            if (cooperationList.get(i).getId_cooperation().equals(idCooperation))
                return cooperationList.get(i);

        }

        return null;

    }

    public static boolean deleteCooperationResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {
                return true;
            }

            return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }

    public static boolean editCooperationFromResponse(String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {
                return true;
            }

            return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
}
