package sereyd.com.soyeducadorasereyd.Controllers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 15/06/17.
 */

public class GroupController {


    public static ArrayList<MyGroup> getCourses() {
        return (ArrayList<MyGroup>) MyGroup.findWithQuery(MyGroup.class, "Select * from MY_GROUP");
    }

    public static MyGroup getGroupById(Long id) {

        return MyGroup.findById(MyGroup.class, id);
    }

    public static int[] counterStudentsGender(Long id) {

        List<Student> studentList = StudentController.getStudents(id);
        int[] studentsCounter = new int[3];
        for (Student student : studentList) {
            switch (student.getSex()) {
                case Constants.MALE:
                    studentsCounter[0] += 1;
                    break;
                case Constants.FEMALE:
                    studentsCounter[1] += 1;
                    break;
            }
            studentsCounter[2] += 1;
        }
        return studentsCounter;
    }

    public static MyGroup getGroupFromResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONObject jsonObjectGroup = jsonObject.getJSONObject("group");
                int id_group = jsonObjectGroup.getInt("id_group");
                String school = jsonObjectGroup.getString("school");
                int grade = jsonObjectGroup.getInt("grade");
                String group = jsonObjectGroup.getString("group");
                String period = jsonObjectGroup.getString("period");
                Double percentage;
                try {
                    percentage = jsonObjectGroup.getDouble("attendance_percent");
                } catch (Exception e) {
                    percentage = 0.0;
                }
                return new MyGroup(id_group, school, grade, group, period, percentage);

            }

            throw new Exception();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    public static void saveGroupInDatabase(MyGroup myGroup) {

        myGroup.save();

    }

    public static boolean deleteGroupResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {
                return true;
            }

            return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }


    public static List<MyGroup> getListFromResponse(String response) {

        List<MyGroup> myGroupList = new ArrayList<>();

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject groupObjectJson = jsonArray.getJSONObject(i);
                    JSONObject group = groupObjectJson.getJSONObject("group");


                    int idGroup = group.getInt("id_group");
                    String school = group.getString("school");
                    int grade = group.getInt("grade");
                    String group_number = group.getString("group_number");
                    String period = group.getString("period");

                    Double percentage;
                    try {
                        percentage = group.getDouble("attendance_percent");
                    } catch (Exception e) {
                        percentage = 0.0;
                    }

                    myGroupList.add(new MyGroup(idGroup, school, grade, group_number, period, percentage));

                }

                return myGroupList;

            }

            return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void saveListGroupInDatabase(List<MyGroup> listFromServer) {

        List<MyGroup> listFromDisk = getCourses();

        if (listFromServer.size() > listFromDisk.size()) {

            for (int i = 0; i < listFromServer.size(); i++) {

                boolean isGroupInDisk = false;
                MyGroup myGroupInServer = listFromServer.get(i);

                for (int j = 0; j < listFromDisk.size(); j++) {

                    if (myGroupInServer.getIdGpr() == listFromDisk.get(j).getIdGpr()) {
                        isGroupInDisk = true;
                    }

                }

                if (!isGroupInDisk) {

                    saveGroupInDatabase(myGroupInServer);

                }

            }

        } else if (listFromServer.size() < listFromDisk.size()) {

            for (int i = 0; i < listFromDisk.size(); i++) {

                boolean isGroupInDisk = false;
                MyGroup myGroupInDisk = listFromDisk.get(i);

                for (int j = 0; j < listFromServer.size(); j++) {

                    if (myGroupInDisk.getIdGpr() == listFromServer.get(j).getIdGpr()) {
                        isGroupInDisk = true;
                    }

                }

                if (!isGroupInDisk) {

                    myGroupInDisk.delete();

                }

            }

        }

        saveListForUpdatedObjects(getCourses(), listFromServer);


    }

    private static void saveListForUpdatedObjects(List<MyGroup> listFromDisk, List<MyGroup> listFromServer) {

        for (int i = 0; i < listFromDisk.size(); i++) {

            MyGroup myGroupToEdit = MyGroup.findById(MyGroup.class, listFromDisk.get(i).getId());
            MyGroup myGroupAux = getGroupFromServerById(myGroupToEdit.getIdGpr(), listFromServer);
            if (myGroupAux != null) {

                myGroupToEdit.setSchool(myGroupAux.getSchool());
                myGroupToEdit.setGrade(myGroupAux.getGrade());
                myGroupToEdit.setGroupLetter(myGroupAux.getGroupLetter());
                myGroupToEdit.setPeriod(myGroupAux.getPeriod());
                myGroupToEdit.setAttendancePercent(myGroupAux.getAttendancePercent());
                myGroupToEdit.save();
            }

        }
    }

    private static MyGroup getGroupFromServerById(int idGroup, List<MyGroup> groupList) {

        for (int i = 0; i < groupList.size(); i++) {

            if (groupList.get(i).getIdGpr() == idGroup)
                return groupList.get(i);

        }

        return null;

    }

    public static boolean editResponseFromServer(String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);
            return jsonObject.getInt("error") == 0;

        } catch (Exception e) {
            return false;
        }
    }
}