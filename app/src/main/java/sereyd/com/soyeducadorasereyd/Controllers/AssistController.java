package sereyd.com.soyeducadorasereyd.Controllers;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.AssistBU;
import sereyd.com.soyeducadorasereyd.Models.AssistModel;
import sereyd.com.soyeducadorasereyd.Models.AssistObject;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 03/01/18.
 */

public class AssistController {

    public static void saveListForBackup(List<AssistModel> assistModels, MyGroup myGroup) {

        for (int i = 0; i < assistModels.size(); i++) {

            Student student = StudentController.getStudentById(assistModels.get(i).getAssist().getIdStudent());
            AssistBU.deleteAll(AssistBU.class, "MY_GROUP = ? AND STUDENT = ?", String.valueOf(myGroup.getId()), String.valueOf(student.getId()));

        }

        for (int i = 0; i < assistModels.size(); i++) {

            Student student = StudentController.getStudentById(assistModels.get(i).getAssist().getIdStudent());
            AssistBU assistBU = new AssistBU(myGroup, student, assistModels.get(i).getAssist().getAssistStatus(), assistModels.get(i).getAssist().getConudcT());
            assistBU.save();

        }

    }

    public static void removeList(List<AssistModel> assistModels, MyGroup myGroup) {

        for (int i = 0; i < assistModels.size(); i++) {

            Student student = StudentController.getStudentById(assistModels.get(i).getAssist().getIdStudent());
            AssistBU.deleteAll(AssistBU.class, "MY_GROUP = ? AND STUDENT = ?", String.valueOf(myGroup.getId()), String.valueOf(student.getId()));

        }
    }

    public static List<AssistModel> getListForBackup(MyGroup myGroup) {

        List<AssistBU> list = AssistBU.find(AssistBU.class, "MY_GROUP = ?", String.valueOf(myGroup.getId()));

        if (list.size() > 0) {

            List<AssistModel> assistModels = new ArrayList<>();

            for (int i = 0; i < list.size(); i++) {

                if (list.get(i).getStudent() != null) {

                    String idStudent = list.get(i).getStudent().getId_Student();
                    int assist = list.get(i).getAssistance();
                    int conduct = list.get(i).getConduct();
                    AssistModel assistModel = new AssistModel(String.valueOf(myGroup.getIdGpr()), new AssistObject(idStudent, assist, conduct));
                    assistModels.add(assistModel);

                }
            }

            if (assistModels.size() < StudentController.getStudents(myGroup.getId()).size()) {

                for (int i = assistModels.size(); i < StudentController.getStudents(myGroup.getId()).size(); i++) {

                    AssistModel assistModel = new AssistModel(String.valueOf(myGroup.getIdGpr()), new AssistObject(StudentController.getStudents(myGroup.getId()).get(i).getId_Student(), Constants.ASSIST, Constants.UNDEFINED));
                    assistModels.add(assistModel);
                }

            }

            return assistModels;

        } else {
            return null;
        }


    }

}
