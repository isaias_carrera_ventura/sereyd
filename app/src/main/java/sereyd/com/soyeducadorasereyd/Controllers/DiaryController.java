package sereyd.com.soyeducadorasereyd.Controllers;

import com.google.api.client.json.Json;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.Diary;

/**
 * Created by chaycv on 20/11/17.
 */

public class DiaryController {

    public static List<Diary> getListDiaryFromResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                JSONArray jsonArray = jsonObject.getJSONArray("entries");
                List<Diary> list = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObjectDiary = jsonArray.getJSONObject(i);
                    int idDiary = jsonObjectDiary.getInt("id_diary");
                    String title = jsonObjectDiary.getString("title");
                    String description = jsonObjectDiary.getString("description");
                    String date = jsonObjectDiary.getString("date_creation");
                    Diary diary = new Diary(idDiary, title, description, date);
                    list.add(diary);

                }

                return list;

            }

            return null;


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static List<Diary> getListDiaryInDisk() {


        return Diary.listAll(Diary.class);

    }

    public static void saveListForDiary(List<Diary> diariesServer) {

        List<Diary> listDisk = getListDiaryInDisk();

        for (Diary diaryServer : diariesServer) {

            boolean band = false;

            for (Diary diaryDisk : listDisk) {

                if (diaryServer.getIdDiary() == diaryDisk.getIdDiary()) {

                    band = true;

                }

            }

            if (!band) {

                diaryServer.save();

            }

        }
    }
}
