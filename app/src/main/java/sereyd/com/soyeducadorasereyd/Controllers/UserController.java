package sereyd.com.soyeducadorasereyd.Controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONObject;

import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.AlertReminder;
import sereyd.com.soyeducadorasereyd.Models.AssistBU;
import sereyd.com.soyeducadorasereyd.Models.Cooperation;
import sereyd.com.soyeducadorasereyd.Models.Diagnose;
import sereyd.com.soyeducadorasereyd.Models.Diary;
import sereyd.com.soyeducadorasereyd.Models.Evaluation;
import sereyd.com.soyeducadorasereyd.Models.Homework;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Planning;
import sereyd.com.soyeducadorasereyd.Models.ResourceItemFile;
import sereyd.com.soyeducadorasereyd.Models.Result;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Models.User;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 31/05/17.
 */

public class UserController {


    public static User getUserFromLogin(String jsonString) {

        try {

            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject jsonUser = jsonObject.getJSONObject("user");

            String idUser = jsonUser.getString("id_user");
            String email = jsonUser.isNull("email") ? null : jsonUser.getString("email");
            String name = jsonUser.getString("name");
            String image = jsonUser.isNull("image") ? null : Constants.URL_SERVER + jsonUser.getString("image");

            return new User(name, idUser, email, image);

        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }

    }

    public static User getUserFromFacebookResponse(JSONObject jsonObject) {

        try {

            String idFacebook = jsonObject.getString("id");
            String name = jsonObject.getString("name");
            String imageUrl = "http://graph.facebook.com/" + idFacebook + "/picture?type=large";
            String email = jsonObject.isNull("email") ? null : jsonObject.getString("email");

            return new User(name, idFacebook, email, imageUrl);

        } catch (Exception e) {
            return null;
        }

    }

    public static boolean isUserUpdateCorrectlye(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {
                return true;
            }

            return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public static void saveSession(User user, Context contex) {

        UserController.deleteSession(contex);
        user.save();
        //UserController.saveIdNumerToPreferences(user.save(), PreferenceManager.getDefaultSharedPreferences(contex));

    }


    /*public static void saveIdNumerToPreferences(long idUser, SharedPreferences sharedPreferences) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("idUserSereyd", idUser);
        editor.apply();
    }*/

    /*public static long getIdNumer(SharedPreferences sharedPreferences) {

        return sharedPreferences.getLong("idUserSereyd", -1);

    }*/

    public static void deleteSession(Context context) {

        Student.deleteAll(Student.class);
        User.deleteAll(User.class);
        MyGroup.deleteAll(MyGroup.class);
        AlertReminder.removeAlertsFromDatabase(context);
        AlertReminder.deleteAll(AlertReminder.class);
        Homework.deleteAll(Homework.class);
        Cooperation.deleteAll(Cooperation.class);
        Diagnose.deleteAll(Diagnose.class);
        Result.deleteAll(Result.class);

        PlanningController.deleteFilesForPlanning(context);
        ResourceController.deleteFilesForResource(context);
        EvaluationController.deleteFilesForEvaluation(context);
        DiagnoseController.deleteFilesForDiagnose(context);

        Planning.deleteAll(Planning.class);
        ResourceItemFile.deleteAll(ResourceItemFile.class);
        Diary.deleteAll(Diary.class);

        AssistBU.deleteAll(AssistBU.class);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        //preferences.edit().remove("idUserSereyd").apply();

    }

    public static User getUser() {

        try {

            List<User> list = User.listAll(User.class);
            if (list.size() > 0) {
                return list.get(0);
            }
            return null;

        } catch (Exception e) {
            return null;
        }

    }


}
