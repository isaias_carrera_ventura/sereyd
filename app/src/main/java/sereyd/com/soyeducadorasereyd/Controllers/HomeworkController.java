package sereyd.com.soyeducadorasereyd.Controllers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.Homework;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;

/**
 * Created by chaycv on 19/06/17.
 */

public class HomeworkController {


    public static ArrayList<Homework> getHomeworks(Long id) {
        return (ArrayList<Homework>) Homework.find(Homework.class, "MY_GROUP = ?", String.valueOf(id));
    }

    public static Homework getHomeworkFromResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONObject jsonObjectHomework = jsonObject.getJSONObject("homework");
                String name = jsonObjectHomework.getString("name");
                String[] deadline = jsonObjectHomework.getString("deadline").split("-");
                int year = Integer.parseInt(deadline[0]);
                int month = Integer.parseInt(deadline[1]);
                int day = Integer.parseInt(deadline[2]);
                String description = jsonObjectHomework.getString("description");
                String id_homework = jsonObjectHomework.getString("id_homework");

                return new Homework(id_homework, year, month, day, name, description, null);

            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static List<Homework> getHomeworkListFromResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                JSONArray jsonArray = jsonObjectData.getJSONArray("homeworks");
                List<Homework> homeworkList = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObjectHomework = jsonArray.getJSONObject(i);
                    String id_homework = jsonObjectHomework.getString("id_homework");
                    String name = jsonObjectHomework.getString("name");
                    String description = jsonObjectHomework.getString("description");
                    String[] deadline = jsonObjectHomework.getString("deadline").split("-");
                    int year = Integer.parseInt(deadline[0]);
                    int month = Integer.parseInt(deadline[1]);
                    int day = Integer.parseInt(deadline[2]);
                    homeworkList.add(new Homework(id_homework, year, month, day, name, description, null));

                }

                return homeworkList;

            } else return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    public static void saveListHomeworkInDatabase(List<Homework> homeworkListFromServer, Long idGroup) {


        MyGroup myGroup = MyGroup.findById(MyGroup.class, idGroup);
        List<Homework> homeworkInDisk = getHomeworks(idGroup);

        if (homeworkListFromServer.size() > homeworkInDisk.size()) {
            for (int i = 0; i < homeworkListFromServer.size(); i++) {

                boolean isGroupInDisk = false;
                Homework homework = homeworkListFromServer.get(i);

                for (int j = 0; j < homeworkInDisk.size(); j++) {

                    if (homework.getIdHomework().equals(homeworkInDisk.get(j).getIdHomework())) {
                        isGroupInDisk = true;
                    }

                }

                if (!isGroupInDisk) {

                    homework.setMyGroup(myGroup);
                    addHomeworkToDatabase(homework);

                }

            }
        } else if (homeworkListFromServer.size() < homeworkInDisk.size()) {

            for (int i = 0; i < homeworkInDisk.size(); i++) {

                boolean isGroupInDisk = false;
                Homework homeworkInDB = homeworkInDisk.get(i);

                for (int j = 0; j < homeworkListFromServer.size(); j++) {

                    if (homeworkInDB.getIdHomework().equals(homeworkListFromServer.get(j).getIdHomework())) {
                        isGroupInDisk = true;
                    }

                }

                if (!isGroupInDisk) {

                    homeworkInDB.delete();

                }

            }

        }

        saveListForUpdatedObjects(getHomeworks(idGroup), homeworkListFromServer);

    }

    private static void saveListForUpdatedObjects(ArrayList<Homework> listFromDisk, List<Homework> homeworkListFromServer) {

        for (int i = 0; i < listFromDisk.size(); i++) {

            Homework homeworkToEdit = Homework.findById(Homework.class, listFromDisk.get(i).getId());
            Homework homeworkAux = getHomeworkFromServerById(homeworkToEdit.getIdHomework(), homeworkListFromServer);

            if (homeworkAux != null) {

                homeworkToEdit.setName(homeworkAux.getName());
                homeworkToEdit.setDescription(homeworkAux.getDescription());

                homeworkToEdit.setDay(homeworkAux.getDay());
                homeworkToEdit.setMonth(homeworkAux.getMonth());
                homeworkToEdit.setYear(homeworkAux.getYear());

                homeworkToEdit.save();

            }
        }

    }

    private static Homework getHomeworkFromServerById(String idHomework, List<Homework> homeworkList) {

        for (int i = 0; i < homeworkList.size(); i++) {

            if (homeworkList.get(i).getIdHomework().equals(idHomework))
                return homeworkList.get(i);

        }

        return null;

    }

    private static void addHomeworkToDatabase(Homework homework) {

        homework.save();

    }

    public static boolean editResponseHomeworkFromServer(String respone) {

        try {

            JSONObject jsonObject = new JSONObject(respone);

            return jsonObject.getInt("error") == 0;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean deleteHomeworkResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            return jsonObject.getInt("error") == 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
