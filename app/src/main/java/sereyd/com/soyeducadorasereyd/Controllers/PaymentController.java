package sereyd.com.soyeducadorasereyd.Controllers;

import android.view.View;

import org.json.JSONObject;

import sereyd.com.soyeducadorasereyd.Models.PaymentInformation;
import sereyd.com.soyeducadorasereyd.Models.PaymentObjectSereyd;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 27/09/17.
 */

public class PaymentController {

    public static PaymentObjectSereyd getPaymentStatus(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt("error");
            int errorCode = jsonObject.getInt("code");
            String description = jsonObject.getString("description");
            int resourceCount = jsonObject.getInt("resource_count");
            int planningCount = jsonObject.getInt("planification_count");
            boolean annual = false;
            if (jsonObject.has("annual")) {
                annual = jsonObject.getBoolean("annual");
            }
            return new PaymentObjectSereyd(error, errorCode, description, annual, resourceCount, planningCount);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static PaymentInformation getPaymentObject(String response) {


        try {

            JSONObject jsonObject = new JSONObject(response);
            int code = jsonObject.getInt("code");
            PaymentInformation paymentInformation = new PaymentInformation(code);

            switch (code) {

                case Constants.TEST_PERIOD:

                    paymentInformation.setDescription("Te encuentras en período de prueba");
                    paymentInformation.setInitDate(jsonObject.getString("init_date"));
                    paymentInformation.setFinishDate(jsonObject.getString("finish_date"));

                    break;

                case Constants.ACTIVE_SUSCRIPTION:

                    paymentInformation.setDescription("Tu suscripción está vigente");
                    paymentInformation.setInitDate(jsonObject.getString("init_date"));
                    paymentInformation.setFinishDate(jsonObject.getString("finish_date"));

                    //Info pay
                    JSONObject jsonObjectPaymentInfo = jsonObject.getJSONObject("info_pay");

                    paymentInformation.setCancelled(jsonObjectPaymentInfo.getInt("cancelled"));

                    if (!jsonObjectPaymentInfo.isNull("card_number")) {

                        paymentInformation.setCardNumber(jsonObjectPaymentInfo.getString("card_number"));
                        paymentInformation.setHolderName(jsonObjectPaymentInfo.getString("holder_name"));
                        paymentInformation.setChargeDate(jsonObjectPaymentInfo.getString("charge_date"));
                        paymentInformation.setAmount(jsonObjectPaymentInfo.getDouble("amount"));
                        paymentInformation.setDescriptionCard(jsonObjectPaymentInfo.getString("desc"));

                    } else if (jsonObjectPaymentInfo.getInt("cancelled") == Constants.CANCELLED) {

                        paymentInformation.setDescriptionCard("Cancelaste tu suscripción a SEREYD");

                    } else {
                        paymentInformation.setDescriptionCard("Tu suscripción es con paypal");

                    }

                    break;

                case Constants.PAYMENT_REQUIRED:

                    paymentInformation.setDescription("Tu suscripción ha caducado");

                    break;

            }

            return paymentInformation;


        } catch (Exception e) {

            e.printStackTrace();
            return null;

        }

    }
}
