package sereyd.com.soyeducadorasereyd.Controllers;

import android.content.Context;

import com.snatik.storage.Storage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.Evaluation;
import sereyd.com.soyeducadorasereyd.Models.EvaluationSelection;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Planning;
import sereyd.com.soyeducadorasereyd.Models.TestModel;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 23/08/17.
 */

public class EvaluationController {


    public static List<EvaluationSelection> getEvaluationForSelection(String response) {

        try {

            ArrayList<EvaluationSelection> evaluationSelections = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONArray jsonArray = jsonObject.getJSONArray("evaluations");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectDiagnose = jsonArray.getJSONObject(i);
                    String id = jsonObjectDiagnose.getString("id_evaluation");
                    String name = jsonObjectDiagnose.getString("name");
                    EvaluationSelection evaluationSelection = new EvaluationSelection(id, name);
                    evaluationSelections.add(evaluationSelection);
                }
                return evaluationSelections;

            } else
                return null;


        } catch (Exception e) {
            return null;
        }
    }

    public static List<TestModel> getEvaluationTestFromResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                List<TestModel> testModelList = new ArrayList<>();
                JSONArray jsonArrayTest = jsonObject.getJSONArray("questions");

                for (int i = 0; i < jsonArrayTest.length(); i++) {

                    JSONObject jsonObjectTest = jsonArrayTest.getJSONObject(i);

                    String idTest = jsonObjectTest.getString("id_evaluation");
                    String id_question = jsonObjectTest.getString("id_question_evaluation");
                    String quesstion = jsonObjectTest.getString("question");

                    testModelList.add(new TestModel(idTest, null, quesstion, id_question));

                }

                return testModelList;

            }

            return null;

        } catch (Exception e) {
            return null;
        }


    }

    public static Evaluation getEvaluationAssociateForGroup(String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONObject jsonObjectEvaluation = jsonObject.getJSONObject("group_evaluation");
                String evaluationId = jsonObjectEvaluation.getString("id_evaluation");
                String created_at = jsonObjectEvaluation.getString("created_at");
                String idEvaluationGroup = jsonObjectEvaluation.getString("id_group_evaluation");
                int state = jsonObjectEvaluation.getInt("completed");
                return new Evaluation(null, state, evaluationId, idEvaluationGroup, created_at, null);
            }

            return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Evaluation getEvaluationById(Long evaluationId) {
        return Evaluation.findById(Evaluation.class, evaluationId);
    }

    public static List<Evaluation> getEvaluationForGroup(MyGroup myGroup) {

        return Evaluation.find(Evaluation.class, "MY_GROUP = ?", String.valueOf(myGroup.getId()));

    }

    public static boolean incompleteEvaluationByGroup(Long id) {

        boolean incompleteDiagnose = false;
        MyGroup myGroup = GroupController.getGroupById(id);
        List<Evaluation> evaluationListGroup = getEvaluationForGroup(myGroup);

        for (Evaluation evaluation : evaluationListGroup) {

            if (evaluation.getState() == Constants.INCOMPLETE) {
                incompleteDiagnose = true;
            }

        }

        return incompleteDiagnose;
    }

    public static Evaluation getIncompleteEvaluation(Long id) {
        MyGroup myGroup = GroupController.getGroupById(id);
        List<Evaluation> evaluationsDatabase = getEvaluationForGroup(myGroup);
        for (Evaluation evaluation : evaluationsDatabase) {

            if (evaluation.getState() == Constants.INCOMPLETE) {
                return evaluation;
            }

        }

        return null;

    }

    public static List<Evaluation> getListEvaluationPending(String response, MyGroup myGroup) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("evaluations");

            if (jsonArray.length() > 0) {

                List<Evaluation> evaluations = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObjectDiagnostic = jsonArray.getJSONObject(i);
                    String idEvaluationType = (jsonObjectDiagnostic.getString("id_evaluation"));
                    String date = jsonObjectDiagnostic.getString("created_at");
                    String idGroupDiagnostic = (jsonObjectDiagnostic.getString("id_group_evaluation"));
                    int complete = jsonObjectDiagnostic.getInt("completed");

                    evaluations.add(new Evaluation(null, complete, idEvaluationType, idGroupDiagnostic, date, myGroup));

                }

                return evaluations;

            }

            return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void saveListEvaluation(List<Evaluation> evaluationList, Long id) {
        MyGroup myGroup = GroupController.getGroupById(id);
        List<Evaluation> evaluationsDatabase = getEvaluationForGroup(myGroup);
        boolean diagnoseInDisk = false;
        for (Evaluation evaluation : evaluationList) {

            for (Evaluation evaluationDatabase : evaluationsDatabase) {

                if (evaluationDatabase.getEvaluationGroupId().equals(evaluation.getEvaluationGroupId())) {
                    diagnoseInDisk = true;
                }
            }

            if (!diagnoseInDisk) {

                evaluation.save();
                diagnoseInDisk = false;

            }

        }
    }

    public static String getFileExcelPathForPlanning(String path, Evaluation evaluation) {

        return path + File.separator + evaluation.getEvaluationGroupId() + "_" + evaluation.getEvaluationId() + "Evaluacion" + ".xlsx";

    }

    public static boolean isEvaluationFileInDisk(Evaluation evaluation, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "evaluations";
        storage.createDirectory(newDir);
        String filePath = getFileExcelPathForPlanning(newDir, evaluation);
        byte[] bytes = storage.readFile(filePath);
        return bytes != null;

    }

    public static String getFileAbsoluteFileExcelPathForEvaluation(Evaluation evaluation, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "evaluations";
        storage.createDirectory(newDir);
        return getFileExcelPathForPlanning(newDir, evaluation);

    }

    static void deleteFilesForEvaluation(Context context) {

        List<Evaluation> evaluationList = Evaluation.listAll(Evaluation.class);
        final Storage storage = new Storage(context);

        for (int i = 0; i < evaluationList.size(); i++) {

            if (isEvaluationFileInDisk(evaluationList.get(i), context)) {

                storage.deleteFile(getFileAbsoluteFileExcelPathForEvaluation(evaluationList.get(i), context));

            }

        }

        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "evaluations";
        storage.deleteDirectory(newDir);


    }


}
