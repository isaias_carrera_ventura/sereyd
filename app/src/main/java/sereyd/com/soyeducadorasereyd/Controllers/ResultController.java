package sereyd.com.soyeducadorasereyd.Controllers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.ResultModel;

/**
 * Created by chaycv on 27/07/17.
 */

public class ResultController {

    public static List<ResultModel> getResultsFromResponseDiagnostic(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                JSONArray jsonArrayResults = jsonObject.getJSONArray("final_diagnostics");
                List<ResultModel> resultModels = new ArrayList<>();
                for (int i = 0; i < jsonArrayResults.length(); i++) {

                    JSONObject jsonResult = jsonArrayResults.getJSONObject(i);
                    String name = jsonResult.getString("name");
                    String level = jsonResult.getString("level");
                    int percentage = jsonResult.getInt("percent");

                    resultModels.add(new ResultModel(name, level, percentage));

                }

                return resultModels;

            } else return null;

        } catch (Exception e) {
            return null;
        }

    }

    public static ResultModel getResultsFromResponseEvaluation(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                String name = jsonObject.getString("evaluation_name");
                String level = jsonObject.getString("created_at");
                int percent = jsonObject.getInt("evaluation_percent");

                return new ResultModel(name, level, percent);


            } else return null;

        } catch (Exception e) {
            return null;
        }

    }
}
