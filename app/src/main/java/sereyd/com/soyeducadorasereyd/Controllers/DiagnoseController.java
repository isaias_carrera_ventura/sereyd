package sereyd.com.soyeducadorasereyd.Controllers;

import android.content.Context;
import android.util.Log;

import com.snatik.storage.Storage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.Diagnose;
import sereyd.com.soyeducadorasereyd.Models.DiagnoseSelection;
import sereyd.com.soyeducadorasereyd.Models.Evaluation;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 19/07/17.
 */

public class DiagnoseController {

    public static List<DiagnoseSelection> getDiagnoseForSelection(String response) {

        try {

            ArrayList<DiagnoseSelection> diagnoseSelections = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONArray jsonArray = jsonObject.getJSONArray("diagnostics");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectDiagnose = jsonArray.getJSONObject(i);
                    String id = jsonObjectDiagnose.getString("id_diagnostic");
                    String name = jsonObjectDiagnose.getString("name");
                    DiagnoseSelection diagnoseSelection = new DiagnoseSelection(id, name);
                    diagnoseSelections.add(diagnoseSelection);
                }
                return diagnoseSelections;

            } else
                return null;


        } catch (Exception e) {
            return null;
        }

    }

    public static void deleteDiagnose(Long diagnoseId) {

        Diagnose diagnose = Diagnose.findById(Diagnose.class, diagnoseId);
        diagnose.delete();

    }

    public static int getDiagnoseResponse(JSONObject jsonObject) {

        try {

            return jsonObject.getInt("error");

        } catch (Exception e) {
            return -1;

        }

    }

    public static Diagnose getDiagnoseCreationResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {


                JSONObject jsonObjectDiagnose = jsonObject.getJSONObject("group_diagnostic");
                int idDiagnosticType = Integer.parseInt(jsonObjectDiagnose.getString("id_diagnostic"));
                String date = jsonObjectDiagnose.getString("created_at");
                int idGroupDiagnostic = Integer.parseInt(jsonObjectDiagnose.getString("id_group_diagnostic"));
                int complete = jsonObjectDiagnose.getInt("completed");

                Diagnose diagnose = new Diagnose(date, complete, idDiagnosticType, idGroupDiagnostic, null);
                return diagnose;

            }

            return null;

        } catch (Exception e) {
            return null;
        }
    }

    public static boolean getStatusForCompletionDiagnose(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                return true;

            }

            return false;

        } catch (Exception e) {
            return false;
        }

    }

    public static List<Diagnose> getListDiagnosticPending(String response, MyGroup myGroup) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("diagnostics");

            if (jsonArray.length() > 0) {

                List<Diagnose> diagnoses = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObjectDiagnostic = jsonArray.getJSONObject(i);
                    int idDiagnosticType = Integer.parseInt(jsonObjectDiagnostic.getString("diagnostic"));
                    String date = jsonObjectDiagnostic.getString("created_at");
                    int idGroupDiagnostic = Integer.parseInt(jsonObjectDiagnostic.getString("id_group_diagnostic"));
                    int complete = jsonObjectDiagnostic.getInt("completed");

                    diagnoses.add(new Diagnose(date, complete, idDiagnosticType, idGroupDiagnostic, myGroup));

                }

                return diagnoses;

            }

            return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static ArrayList<Diagnose> getDiagnoseByGroup(Long id) {
        return (ArrayList<Diagnose>) Diagnose.find(Diagnose.class, "MY_GROUP = ?", String.valueOf(id));
    }

    public static boolean incompleteDiagnoseByGroup(Long id) {

        boolean incompleteDiagnose = false;
        ArrayList<Diagnose> diagnoseListFromGrup = getDiagnoseByGroup(id);

        for (Diagnose diagnose : diagnoseListFromGrup) {

            if (diagnose.getState() == Constants.INCOMPLETE) {
                incompleteDiagnose = true;
            }

        }

        return incompleteDiagnose;

    }

    public static int numberOfDiagnoseByGroup(Long id) {

        return getDiagnoseByGroup(id).size();

    }

    public static Diagnose getDiagnoseById(Long id) {
        return Diagnose.findById(Diagnose.class, id);
    }

    public static void saveListDiagnose(List<Diagnose> diagnoseList, Long id) {

        List<Diagnose> diagnosesDatabase = getDiagnoseByGroup(id);
        boolean diagnoseInDisk = false;
        for (Diagnose diagnose : diagnoseList) {

            for (Diagnose diagnoseDatabase : diagnosesDatabase) {

                if (diagnoseDatabase.getIdGroupDiagnose() == diagnose.getIdGroupDiagnose()) {
                    diagnoseInDisk = true;
                }
            }

            if (!diagnoseInDisk) {

                diagnose.save();
                diagnoseInDisk = false;

            }

        }
    }

    public static Diagnose getIncompleteDiagnose(Long id) {

        List<Diagnose> diagnosesDatabase = getDiagnoseByGroup(id);
        for (Diagnose diagnose : diagnosesDatabase) {

            if (diagnose.getState() == Constants.INCOMPLETE) {
                return diagnose;
            }

        }

        return null;

    }

    //FILE
    public static String getFileExcelPathForDiagnose(String path, Diagnose diagnose) {

        return path + File.separator + diagnose.getIdGroupDiagnose() + "_" + diagnose.getDiagnoseId() + "Diagnostico" + ".xlsx";

    }

    public static boolean isDiagnoseFileInDisk(Diagnose diagnose, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "diagnoses";
        storage.createDirectory(newDir);
        String filePath = getFileExcelPathForDiagnose(newDir, diagnose);
        byte[] bytes = storage.readFile(filePath);
        return bytes != null;

    }

    public static String getFileAbsoluteFileExcelPathForDiagnose(Diagnose diagnose, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "diagnoses";
        storage.createDirectory(newDir);
        return getFileExcelPathForDiagnose(newDir, diagnose);

    }

    public static void deleteFilesForDiagnose(Context context) {

        List<Diagnose> diagnoseList = Diagnose.listAll(Diagnose.class);
        final Storage storage = new Storage(context);

        for (int i = 0; i < diagnoseList.size(); i++) {

            if (isDiagnoseFileInDisk(diagnoseList.get(i), context)) {

                storage.deleteFile(getFileAbsoluteFileExcelPathForDiagnose(diagnoseList.get(i), context));

            }

        }


        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "diagnoses";
        storage.deleteDirectory(newDir);



    }

}


