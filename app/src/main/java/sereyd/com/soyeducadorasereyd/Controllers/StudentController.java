package sereyd.com.soyeducadorasereyd.Controllers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.InformationStudent;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 18/06/17.
 */

public class StudentController {

    public static ArrayList<Student> getStudents(Long id) {

        return (ArrayList<Student>) Student.find(Student.class, "MY_GROUP = ?", String.valueOf(id));

    }

    public static ArrayList<InformationStudent> getStudentInformationInDatabase(Long id) {

        return (ArrayList<InformationStudent>) InformationStudent.find(InformationStudent.class, "STUDENT = ?", String.valueOf(id));

    }

    public static int getStudentNumberInGroup(Long id) {
        return getStudents(id).size();
    }


    public static void addStudentToDatabase(Student student) {
        student.save();
    }

    public static Student getStudentFromResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONObject jsonObjectStudent = jsonObject.getJSONObject("student");

                String birthday = jsonObjectStudent.getString("birthday");
                String lastname = jsonObjectStudent.getString("lastname");
                String name = jsonObjectStudent.getString("name");
                int gender = Integer.parseInt(jsonObjectStudent.getString("gender"));
                String emergency_contact = jsonObjectStudent.getString("emergency_contact");
                String idStudent = jsonObjectStudent.getString("id_student");
                String image = Constants.URL_SERVER + jsonObjectStudent.getString("image");
                String emailTutor = (jsonObjectStudent.getString("email") != null) ? jsonObjectStudent.getString("email") : "";
                return new Student(name, lastname, birthday, gender, emergency_contact, emailTutor, idStudent, image, null);

            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static List<Student> getListStudentForGroup(String response, MyGroup myGroup) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                JSONArray group = jsonObject.getJSONArray("data");
                ArrayList<Student> students = new ArrayList<>();

                for (int j = 0; j < group.length(); j++) {

                    JSONArray jsonObjectStudentsArray = group.getJSONObject(j).getJSONArray("students");

                    for (int i = 0; i < jsonObjectStudentsArray.length(); i++) {

                        JSONObject jsonObjectStudent = jsonObjectStudentsArray.getJSONObject(i);
                        if (jsonObjectStudent.getString("id_group").equals(String.valueOf(myGroup.getIdGpr()))) {

                            String birthday = jsonObjectStudent.getString("birthday").replace("-", "/");
                            String lastname = jsonObjectStudent.getString("lastname");
                            String name = jsonObjectStudent.getString("name");
                            int gender = Integer.parseInt(jsonObjectStudent.getString("gender"));
                            String emergency_contact = jsonObjectStudent.getString("emergency_contact");
                            String idStudent = jsonObjectStudent.getString("id_student");
                            String image = Constants.URL_SERVER + jsonObjectStudent.getString("image");
                            String emailTutor = (jsonObjectStudent.getString("email") != null) ? jsonObjectStudent.getString("email") : "";
                            students.add(new Student(name, lastname, birthday, gender, emergency_contact, emailTutor, idStudent, image, null));

                        }

                    }
                }

                return students;
            }

            return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static List<Student> getStudentListFromResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONObject group = jsonObject.getJSONObject("data");
                JSONArray jsonObjectStudentsArray = group.getJSONArray("students");

                List<Student> studentList = new ArrayList<>();

                for (int i = 0; i < jsonObjectStudentsArray.length(); i++) {

                    JSONObject jsonObjectStudent = jsonObjectStudentsArray.getJSONObject(i);
                    String birthday = jsonObjectStudent.getString("birthday").replace("-", "/");
                    String lastname = jsonObjectStudent.getString("lastname");
                    String name = jsonObjectStudent.getString("name");
                    int gender = Integer.parseInt(jsonObjectStudent.getString("gender"));
                    String emergency_contact = jsonObjectStudent.getString("emergency_contact");
                    String idStudent = jsonObjectStudent.getString("id_student");
                    String image = Constants.URL_SERVER + jsonObjectStudent.getString("image");
                    String emailTutor = (jsonObjectStudent.getString("email") != null) ? jsonObjectStudent.getString("email") : "";
                    studentList.add(new Student(name, lastname, birthday, gender, emergency_contact, emailTutor, idStudent, image, null));

                }

                return studentList;

            } else {
                return null;
            }

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        }
    }

    public static void saveListStudentInDatabase(List<Student> studentListFromServer, Long idGroup) {

        MyGroup myGroup = MyGroup.findById(MyGroup.class, idGroup);
        List<Student> studentListInDisk = getStudents(idGroup);

        if (studentListFromServer.size() > studentListInDisk.size()) {
            for (int i = 0; i < studentListFromServer.size(); i++) {

                boolean isGroupInDisk = false;
                Student student = studentListFromServer.get(i);

                for (int j = 0; j < studentListInDisk.size(); j++) {

                    if (student.getId_Student().equals(studentListInDisk.get(j).getId_Student())) {
                        isGroupInDisk = true;
                    }

                }

                if (!isGroupInDisk) {

                    student.setMyGroup(myGroup);
                    addStudentToDatabase(student);

                }

            }
        } else if (studentListFromServer.size() < studentListInDisk.size()) {

            for (int i = 0; i < studentListInDisk.size(); i++) {

                boolean isGroupInDisk = false;
                Student studentInDisk = studentListInDisk.get(i);

                for (int j = 0; j < studentListFromServer.size(); j++) {

                    if (studentInDisk.getId_Student().equals(studentListFromServer.get(j).getId_Student())) {
                        isGroupInDisk = true;
                    }

                }

                if (!isGroupInDisk) {

                    studentInDisk.delete();

                }

            }

        }

        saveListForUpdatedObjects(getStudents(idGroup), studentListFromServer);

    }

    private static void saveListForUpdatedObjects(ArrayList<Student> studentsInDistk, List<Student> studentListFromServer) {

        for (int i = 0; i < studentsInDistk.size(); i++) {

            Student studentToEdit = Student.findById(Student.class, studentsInDistk.get(i).getId());
            Student studentAux = getGroupFromServerById(studentToEdit.getId_Student(), studentListFromServer);
            if (studentAux != null) {
                studentToEdit.setName(studentAux.getName());
                studentToEdit.setLastName(studentAux.getLastName());
                studentToEdit.setBirthday(studentAux.getBirthday());
                studentToEdit.setSex(studentAux.getSex());
                studentToEdit.setEmergencyContact(studentAux.getEmergencyContact());
                studentToEdit.setEmailTutor(studentAux.getEmailTutor());
                studentToEdit.save();
            }

        }

    }


    private static Student getGroupFromServerById(String idStudent, List<Student> studentList) {

        for (int i = 0; i < studentList.size(); i++) {

            if (studentList.get(i).getId_Student().equals(idStudent))
                return studentList.get(i);

        }

        return null;

    }


    public static boolean deleteStudentFromServer(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {
                return true;
            }

            return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public static boolean editStudentResponse(String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {
                return true;
            }

            return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static InformationStudent getInformationForStudent(String response) {
        try {

            //Attendance
            JSONObject jsonObjectAttendace = new JSONObject(response);
            JSONObject jsonObjectAttendaceDetail = jsonObjectAttendace.getJSONObject("attendance");
            //Behavior
            JSONObject jsonObjectBehavior = new JSONObject(response);
            JSONObject jsonObjectBehaviorDetail = jsonObjectBehavior.getJSONObject("behavior");

            int attendanceCount = jsonObjectAttendaceDetail.getInt("count");
            String behavior = jsonObjectBehaviorDetail.getString("description");
            double behaviorAverage = 0.0;//Math.round(jsonObjectBehaviorDetail.getDouble("average"));
            double diagnostic = Math.round(jsonObjectAttendace.getDouble("diagnostic"));
            double performance = Math.round(jsonObjectAttendace.getDouble("school_performance"));
            double languaje = Math.round(jsonObjectAttendace.getDouble("language"));
            double math = Math.round(jsonObjectAttendace.getDouble("math"));
            double world = Math.round(jsonObjectAttendace.getDouble("world"));
            double health = Math.round(jsonObjectAttendace.getDouble("health"));
            double social = Math.round(jsonObjectAttendace.getDouble("social"));
            double art = Math.round(jsonObjectAttendace.getDouble("art"));

            return new InformationStudent(attendanceCount, behavior, behaviorAverage, languaje, math, world, health, social, art, diagnostic, performance, null);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static InformationStudent getInformationById(Long id) {

        return InformationStudent.findById(InformationStudent.class, id);

    }

    public static InformationStudent getInformationByStudent(Long id) {

        ArrayList<InformationStudent> list = (ArrayList<InformationStudent>) InformationStudent.find(InformationStudent.class, "STUDENT = ?", String.valueOf(id));

        if (list.size() > 0) {
            return list.get(0);
        }

        return null;
    }

    public static Student getStudentById(String idStudent) {

        //idStudent
        List<Student> studentList = Student.find(Student.class, "id_student = ?", idStudent);
        return studentList.get(0);

    }
}
