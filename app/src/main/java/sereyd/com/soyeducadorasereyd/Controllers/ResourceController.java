package sereyd.com.soyeducadorasereyd.Controllers;

import android.content.Context;

import com.snatik.storage.Storage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.Planning;
import sereyd.com.soyeducadorasereyd.Models.ResourceItemFile;
import sereyd.com.soyeducadorasereyd.Models.ResourcesItem;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 04/08/17.
 */

public class ResourceController {

    public static List<ResourcesItem> getListItem(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                List<ResourcesItem> resourcesItemList = new ArrayList<>();

                JSONArray jsonArray = jsonObject.getJSONArray("resources");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObjectItemResource = jsonArray.getJSONObject(i);
                    ResourcesItem resourcesItem = new ResourcesItem(jsonObjectItemResource.getString("name"), jsonObjectItemResource.getString("id_resource"));
                    resourcesItemList.add(resourcesItem);

                }

                return resourcesItemList;

            } else {
                return null;
            }

        } catch (Exception e) {
            return null;
        }
    }

    public static List<ResourceItemFile> getListItemFile(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                List<ResourceItemFile> resourcesItemList = new ArrayList<>();
                JSONArray jsonArray = jsonObject.getJSONArray("resources");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObjectItemResource = jsonArray.getJSONObject(i);
                    ResourceItemFile resourceItemFile = new ResourceItemFile(jsonObjectItemResource.getString("id_resource_file"), jsonObjectItemResource.getString("name"), Constants.URL_SERVER + jsonObjectItemResource.getString("url"));
                    resourcesItemList.add(resourceItemFile);

                }

                return resourcesItemList;

            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    public static String getFilePathForResource(String path, ResourceItemFile resourceItemFile) {
        return path + File.separator + resourceItemFile.getIdResource() + "_" + resourceItemFile.getName() + ".pdf";
    }

    public static boolean isResourceFileInDisk(ResourceItemFile resourceItemFile, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "resources";
        storage.createDirectory(newDir);
        String filePath = getFilePathForResource(newDir, resourceItemFile);
        byte[] bytes = storage.readFile(filePath);
        return bytes != null;

    }

    public static String getFileAbsoluteFilePDFPathForPlanning(ResourceItemFile itemFile, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "resources";
        storage.createDirectory(newDir);
        return getFilePathForResource(newDir, itemFile);

    }

    public static byte[] getResourceFilePDFInDisk(ResourceItemFile itemFile, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "resources";
        storage.createDirectory(newDir);
        String filePath = getFilePathForResource(newDir, itemFile);
        return storage.readFile(filePath);

    }

    public static void deleteFilesForResource(Context context) {

        List<ResourceItemFile> resourceItemFiles = ResourceItemFile.listAll(ResourceItemFile.class);
        final Storage storage = new Storage(context);

        for (int i = 0; i < resourceItemFiles.size(); i++) {

            if (isResourceFileInDisk(resourceItemFiles.get(i), context)) {

                storage.deleteFile(getFileAbsoluteFilePDFPathForPlanning(resourceItemFiles.get(i), context));

            }

        }


        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "resources";
        storage.deleteDirectory(newDir);


    }

    public static boolean isResourceInDatabase(ResourceItemFile resourceItemFile) {
        List<ResourceItemFile> resourceItemFiles = ResourceItemFile.listAll(ResourceItemFile.class);

        for (int i = 0; i < resourceItemFiles.size(); i++) {
            if (resourceItemFile.getIdResource().equals(resourceItemFiles.get(i).getIdResource())) {
                return true;
            }
        }

        return false;
    }

    public static boolean asignResourceToDatabase(ResourceItemFile resourceItemFile, String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0 || jsonObject.getInt("error") == 2) {

                if (!ResourceController.isResourceInDatabase(resourceItemFile)) {
                    resourceItemFile.save();
                }
                return true;

            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }

    }


    public static void addResourceListToDatabase(List<ResourceItemFile> resourceItemFilesFromServer) {

        List<ResourceItemFile> resourceItemFilesInDatabase = ResourceItemFile.listAll(ResourceItemFile.class);
        for (int i = 0; i < resourceItemFilesFromServer.size(); i++) {
            ResourceItemFile resourceItemFile = resourceItemFilesFromServer.get(i);
            boolean isResourceDownlaoded = false;

            for (int j = 0; j < resourceItemFilesInDatabase.size(); j++) {

                if (resourceItemFile.getIdResource().equals(resourceItemFilesInDatabase.get(j).getIdResource())) {
                    isResourceDownlaoded = true;
                }

            }

            if (!isResourceDownlaoded) {
                resourceItemFile.save();
            }
        }

    }

    public static String getMissingResource(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONObject jsonObjectResponse = jsonObject.getJSONObject("resource_count");

            if (jsonObjectResponse.getInt("error") == 0) {

                return "\nTe quedan " + jsonObjectResponse.getString("payments") +" recursos para descargar.";

            } else {
                return "";
            }

        } catch (Exception e) {
            return "";
        }

    }
}
