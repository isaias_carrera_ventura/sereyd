package sereyd.com.soyeducadorasereyd.Controllers;

import android.content.Context;

import com.snatik.storage.Storage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.Planning;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 12/07/17.
 */

public class PlanningController {


    public static List<Planning> getPlanningInDatabase() {

        return Planning.listAll(Planning.class);

    }

    public static List<Planning> getPlanningFromResponse(String response) {

        List<Planning> planningList = new ArrayList<>();

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                JSONArray jsonArray = jsonObject.getJSONArray("planifications");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObjectPlanning = jsonArray.getJSONObject(i);
                    int idPlanning = jsonObjectPlanning.getInt("id_planification");
                    String name = jsonObjectPlanning.getString("name");
                    String image = Constants.URL_SERVER + jsonObjectPlanning.getString("image");
                    String description = jsonObjectPlanning.getString("description");
                    String urlPDF = Constants.URL_SERVER + jsonObjectPlanning.getString("url");
                    String urlWord = Constants.URL_SERVER + jsonObjectPlanning.getString("url_word");
                    String urlPreview = Constants.URL_SERVER + jsonObjectPlanning.getString("preview");
                    String fieldString = jsonObjectPlanning.getString("formative_field");
                    int type = jsonObjectPlanning.getInt("premium");
                    String field = new String(fieldString.getBytes(), "UTF-8").replace("?", "");
                    planningList.add(new Planning(idPlanning, urlPDF, urlWord, urlPreview, name, description, image, field, type));

                }

                return planningList;

            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static int getNumberPlanningAllowed(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 1) {
                return 0;
            } else {
                return jsonObject.getInt("planification_count");
            }

        } catch (Exception e) {
            return 0;
        }

    }

    public static void savePlanningListToDatabase(List<Planning> planningListFromServer) {

        List<Planning> planningListInDatabase = Planning.listAll(Planning.class);

        for (int i = 0; i < planningListFromServer.size(); i++) {

            Planning planning = planningListFromServer.get(i);
            boolean isPlanningDownloaded = false;

            for (int j = 0; j < planningListInDatabase.size(); j++) {

                if (planning.getIdPlanning() == planningListInDatabase.get(j).getIdPlanning()) {
                    isPlanningDownloaded = true;
                }

            }

            if (!isPlanningDownloaded) {
                planning.save();
            }

        }
    }

    public static boolean isPlanningInDatabase(Planning planning) {

        List<Planning> planningListInDatabase = Planning.listAll(Planning.class);

        for (int i = 0; i < planningListInDatabase.size(); i++) {
            if (planning.getIdPlanning() == planningListInDatabase.get(i).getIdPlanning()) {
                return true;
            }
        }

        return false;

    }

    public static boolean asignPlanningToDatabase(Planning planning, String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                planning.save();
                return true;

            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }

    }

    public static boolean isPaymentCorrect(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("error") == 0) {

                return true;

            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }

    }

    public static String getFilePDFPathForPlanning(String path, Planning planning) {
        return path + File.separator + planning.getIdPlanning() + "_" + planning.getName() + ".pdf";
    }

    public static String getFileWordPathForPlanning(String path, Planning planning) {
        return path + File.separator + planning.getIdPlanning() + "_" + planning.getName() + ".docx";
    }

    public static boolean isPlanningFilePDFInDisk(Planning planning, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "plannings";
        storage.createDirectory(newDir);
        String filePath = getFilePDFPathForPlanning(newDir, planning);
        byte[] bytes = storage.readFile(filePath);
        return bytes != null;

    }

    public static boolean isPlanningFileWordInDisk(Planning planning, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "plannings";
        storage.createDirectory(newDir);
        String filePath = getFileWordPathForPlanning(newDir, planning);
        byte[] bytes = storage.readFile(filePath);
        return bytes != null;

    }

    public static String getFileAbsoluteFilePDFPathForPlanning(Planning planning, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "plannings";
        storage.createDirectory(newDir);
        return getFilePDFPathForPlanning(newDir, planning);

    }

    public static String getFileAbsoluteFileWordPathForPlanning(Planning planning, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "plannings";
        storage.createDirectory(newDir);
        return getFileWordPathForPlanning(newDir, planning);

    }

    public static byte[] getPlanningFilePDFInDisk(Planning planning, Context context) {

        final Storage storage = new Storage(context);
        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "plannings";
        storage.createDirectory(newDir);
        String filePath = getFilePDFPathForPlanning(newDir, planning);
        return storage.readFile(filePath);

    }

    public static void deleteFilesForPlanning(Context context) {

        List<Planning> planningList = Planning.listAll(Planning.class);
        final Storage storage = new Storage(context);

        for (int i = 0; i < planningList.size(); i++) {

            if (isPlanningFilePDFInDisk(planningList.get(i), context)) {

                storage.deleteFile(getFileAbsoluteFilePDFPathForPlanning(planningList.get(i), context));
                storage.deleteFile(getFileAbsoluteFileWordPathForPlanning(planningList.get(i), context));

            }

        }

        final String path = storage.getExternalStorageDirectory();
        String newDir = path + File.separator + "plannings";
        storage.deleteDirectory(newDir);


    }

    public static List<Planning> getListForMissingPlanning(List<Planning> planningListFromServer) {

        List<Planning> listDatabase = getPlanningInDatabase();
        List<Planning> listToReturn = new ArrayList<>();

        for (int i = 0; i < listDatabase.size(); i++) {

            Planning planningDatabase = listDatabase.get(i);

            for (int j = 0; j < planningListFromServer.size(); j++) {

                if (planningDatabase.getIdPlanning() == planningListFromServer.get(j).getIdPlanning()) {

                    planningListFromServer.remove(j);

                }
            }
        }

        listToReturn.addAll(listDatabase);
        listToReturn.addAll(planningListFromServer);

        return listToReturn;
    }
}