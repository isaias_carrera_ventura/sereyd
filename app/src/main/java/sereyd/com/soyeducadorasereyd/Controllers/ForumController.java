package sereyd.com.soyeducadorasereyd.Controllers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.ForumDiscussionModel;
import sereyd.com.soyeducadorasereyd.Models.ForumQuestionModel;
import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 19/12/17.
 */

public class ForumController {

    public static List<ForumQuestionModel> getDiscussionList(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("questions");

            List<ForumQuestionModel> forumQuestionModels = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonQuestion = jsonArray.getJSONObject(i);
                String idDiscussion = jsonQuestion.getString("id_discussion");
                String title = jsonQuestion.getString("title");
                String question = jsonQuestion.getString("question");
                String date = jsonQuestion.getString("date_creation");
                String idUser = jsonQuestion.getString("id_user");
                String name = jsonQuestion.getString("name");
                int ranking = jsonQuestion.getInt("ranking");
                String image = Constants.URL_SERVER + jsonQuestion.getString("image");

                ForumQuestionModel forumQuestionModel = new ForumQuestionModel(idDiscussion, title, question, date, idUser, name, image, ranking);
                forumQuestionModels.add(forumQuestionModel);
            }

            return forumQuestionModels;


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static List<ForumDiscussionModel> getDiscussionAnswerList(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("answers");

            List<ForumDiscussionModel> forumQuestionModels = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonQuestion = jsonArray.getJSONObject(i);
                String id = jsonQuestion.getString("id_answer");
                String answer = jsonQuestion.getString("answer");
                String date = jsonQuestion.getString("date_creation");
                String name = jsonQuestion.getString("name");
                int ranking = jsonQuestion.getInt("ranking");
                String image = Constants.URL_SERVER + jsonQuestion.getString("image");

                forumQuestionModels.add(new ForumDiscussionModel(id, answer, date, name, image, ranking));
            }

            return forumQuestionModels;


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

}
