package sereyd.com.soyeducadorasereyd.Controllers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Models.TestModel;

/**
 * Created by chaycv on 25/07/17.
 */

public class TestController {

    public static List<TestModel> getTestFromResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                List<TestModel> testModelList = new ArrayList<>();
                JSONArray jsonArrayTest = jsonObject.getJSONArray("tests");

                for (int i = 0; i < jsonArrayTest.length(); i++) {

                    JSONObject jsonObjectTest = jsonArrayTest.getJSONObject(i);
                    String nameTest = jsonObjectTest.getString("name");
                    String idTest = jsonObjectTest.getString("id_test");

                    JSONArray jsonArrayQuestions = jsonObjectTest.getJSONArray("questions");

                    for (int j = 0; j < jsonArrayQuestions.length(); j++) {

                        JSONObject jsonObjectQuestion = jsonArrayQuestions.getJSONObject(j);
                        String questionId = jsonObjectQuestion.getString("id_question");
                        String questionStr = jsonObjectQuestion.getString("question");

                        testModelList.add(new TestModel(idTest, nameTest, questionStr, questionId));

                    }

                }

                return testModelList;

            }

            return null;

        } catch (Exception e) {
            return null;
        }


    }


    public static List<String> getListStudentForPending(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.getInt("error") == 0) {

                JSONArray jsonArrayStudentsPending = jsonObject.getJSONArray("students_pending");

                List<String> stringList = new ArrayList<>();
                for (int i = 0; i < jsonArrayStudentsPending.length(); i++) {
                    stringList.add(jsonArrayStudentsPending.getString(i));
                }

                return stringList;

            } else return null;

        } catch (Exception e) {
            return null;
        }

    }

    public static List<Student> getStudentArrayInDatabaseForPendingDiagnose(List<String> stringList, Long id) {

        List<Student> studentInDatabase = StudentController.getStudents(id);
        List<Student> studentListToReturn = new ArrayList<>();
        for (int i = 0; i < stringList.size(); i++) {

            for (int j = 0; j < studentInDatabase.size(); j++) {

                if (studentInDatabase.get(j).getId_Student().equals(stringList.get(i))) {
                    studentListToReturn.add(studentInDatabase.get(j));
                }

            }

        }

        return studentListToReturn;

    }
}
