package sereyd.com.soyeducadorasereyd;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.orm.Database;
import com.orm.SugarApp;

/**
 * Created by chaycv on 23/11/17.
 */

public class SereydApp extends com.orm.SugarApp{

    private Database database;
    private static SugarApp sugarContext;

    @Override
    public void onCreate(){
        super.onCreate();
        SereydApp.sugarContext = this;
        this.database = new Database(this);
    }

    public void onTerminate(){
        if (this.database != null) {
            this.database.getDB().close();
        }
        super.onTerminate();
    }

    public static SugarApp getSugarContext(){
        return sugarContext;
    }

    protected Database getDatabase() {
        return database;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

}
