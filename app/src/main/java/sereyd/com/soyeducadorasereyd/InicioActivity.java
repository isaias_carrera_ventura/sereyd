package sereyd.com.soyeducadorasereyd;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import sereyd.com.soyeducadorasereyd.Controllers.GroupController;
import sereyd.com.soyeducadorasereyd.Controllers.StudentController;
import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.GenericClass.MySingleton;
import sereyd.com.soyeducadorasereyd.Models.MyGroup;
import sereyd.com.soyeducadorasereyd.Models.Student;
import sereyd.com.soyeducadorasereyd.Models.User;
import sereyd.com.soyeducadorasereyd.Utils.Constants;
import sereyd.com.soyeducadorasereyd.Utils.FontUtil;
import sereyd.com.soyeducadorasereyd.Utils.HandlerVolleyError;
import sereyd.com.soyeducadorasereyd.View.MainMenuActivity;
import sereyd.com.soyeducadorasereyd.View.Sign.SignInActivity;
import sereyd.com.soyeducadorasereyd.View.Sign.SignUpActivity;

public class InicioActivity extends AppCompatActivity {

    @BindView(R.id.textViewRegister)
    TextView textViewReg;
    @BindView(R.id.textViewRegisterClickerLabel)
    TextView textviewRegClick;
    @BindView(R.id.textViewTermsAndCons)
    TextView textViewTermsAndCons;
    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    @BindView(R.id.login_button_facebook)
    Button buttonFacebok;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    CallbackManager callbackManager;

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        setCustomViews();

        try {

            User user = UserController.getUser();

            if (user != null) {

                startActivity(new Intent(getApplicationContext(), MainMenuActivity.class));
                finish();

            } else {

                UserController.deleteSession(this);
                FacebookInit();

            }

        } catch (Exception e) {

            /*UserController.deleteSession(this);*/
            FacebookInit();
        }

    }

    private void FacebookInit() {


        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(

                callbackManager,

                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {

                                        makeStandarRequest(object, response);

                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, name, link, email");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {

                        hideProgress();
                        new SweetAlertDialog(InicioActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops!")
                                .setContentText(getString(R.string.cancel_session))
                                .show();

                    }

                    @Override
                    public void onError(FacebookException exception) {

                        hideProgress();
                        new SweetAlertDialog(InicioActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops!")
                                .setContentText(exception.toString())
                                .show();

                    }
                }
        );

    }

    @OnClick(R.id.login_button_facebook)
    public void submit(View view) {

        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }

        showProgress();

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_photos", "email", "user_birthday", "public_profile"));

    }


    @OnClick(R.id.textViewTermsAndCons)
    public void terms() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TERMS_URL));
        startActivity(browserIntent);
    }

    @OnClick(R.id.buttonLogin)
    public void logIn(View view) {
        startActivity(new Intent(getApplicationContext(), SignInActivity.class));
    }

    @OnClick(R.id.textViewRegisterClickerLabel)
    public void registerLabel(View view) {
        startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setCustomViews() {

        ButterKnife.bind(this);
        textViewReg.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        buttonFacebok.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        buttonLogin.setTypeface(FontUtil.getRegularFont(getApplicationContext()));
        textviewRegClick.setTypeface(FontUtil.getBoldFont(getApplicationContext()));
        textViewTermsAndCons.setTypeface(FontUtil.getHairlineFont(getApplicationContext()));

    }


    private void makeStandarRequest(final JSONObject objectFacebook, final GraphResponse graphResponse) {

        final User user = UserController.getUserFromFacebookResponse(objectFacebook);

        if (user != null) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_ENDPOINT_LOGIN_FACEBOOK, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    hideProgress();
                    final User userAux = UserController.getUserFromLogin(response);
                    login(userAux);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.printStackTrace();
                    hideProgress();

                    try {

                        boolean band = false;

                        if (error != null) {
                            if (error.networkResponse != null) {

                                String errorResponse = new String(error.networkResponse.data);
                                JSONObject jsonObject = new JSONObject(errorResponse);
                                if (jsonObject.getInt("error") != 1) {

                                    band = true;

                                }
                            } else {

                                band = true;
                            }
                        } else {

                            band = true;
                        }

                        //PROCESS BAND

                        if (band) {

                            new SweetAlertDialog(InicioActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText(getResources().getString(R.string.error_general))
                                    .show();

                        } else {

                            signUpForUser(user);

                        }


                    } catch (Exception e) {

                        e.printStackTrace();
                        new SweetAlertDialog(InicioActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops!")
                                .setContentText(getResources().getString(R.string.error_general))
                                .show();
                    }


                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("id_facebook", user.getIdUser());
                    return params;
                }
            };

            MySingleton.getInstance(InicioActivity.this).addToRequestQueue(stringRequest);
            showProgress();

        } else {

            hideProgress();

            new SweetAlertDialog(InicioActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops!")
                    .setContentText(getResources().getString(R.string.error_general))
                    .show();
        }


    }

    private void login(final User userAux) {

        if (userAux != null) {

            downloadUserContentForGroupStudents(userAux);

        } else {

            new SweetAlertDialog(InicioActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops!")
                    .setContentText(getResources().getString(R.string.error_general))
                    .show();

        }

    }

    private void downloadUserContentForGroupStudents(User userAux) {

        UserController.saveSession(userAux, InicioActivity.this);
        new DownloadUserContent().execute();
        startActivity(new Intent(getApplicationContext(), MainMenuActivity.class));
        finish();

    }

    private void signUpForUser(final User user) {

        String url = Constants.URL_ENDPOINT_USER;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                hideProgress();
                User userAux = UserController.getUserFromLogin(response);
                login(userAux);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                hideProgress();
                new SweetAlertDialog(InicioActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(HandlerVolleyError.getErrorMessage(error, InicioActivity.this))
                        .show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", user.getName());
                params.put("id_facebook", user.getIdUser());
                params.put("image", user.getLinkImage());
                params.put("email", user.getEmail() != null ? user.getEmail() : "");
                return params;
            }
        };

        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        showProgress();

    }

    private class DownloadUserContent extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {

            String url = Constants.URL_ENDPOINT_GROUP_USER + UserController.getUser().getIdUser();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    List<MyGroup> list = GroupController.getListFromResponse(response);

                    if (list != null) {

                        GroupController.saveListGroupInDatabase(list);
                        List<MyGroup> myGroups = GroupController.getCourses();
                        for (int i = 0; i < myGroups.size(); i++) {

                            List<Student> studentList = StudentController.getListStudentForGroup(response, myGroups.get(i));
                            if (studentList != null) {
                                StudentController.saveListStudentInDatabase(studentList, myGroups.get(i).getId());
                            }

                        }

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });


            //PAYMENT INFORMATION
            /*String urlPayment = Constants.URL_ENDPOINT_PAYMENT + UserController.getUser().getIdUser();
            StringRequest stringRequestPayment = new StringRequest(Request.Method.GET, urlPayment, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {



                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });


            MySingleton.getInstance(InicioActivity.this).addToRequestQueue(stringRequestPayment);*/
            MySingleton.getInstance(InicioActivity.this).addToRequestQueue(stringRequest);

            return null;

        }
    }

}
