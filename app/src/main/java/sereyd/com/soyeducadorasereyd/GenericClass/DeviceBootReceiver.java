package sereyd.com.soyeducadorasereyd.GenericClass;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;

import java.util.Calendar;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Controllers.UserController;
import sereyd.com.soyeducadorasereyd.Models.AlertReminder;
import sereyd.com.soyeducadorasereyd.Models.User;

/**
 * Created by isaiascarreraventura on 25/01/17.
 */

public class DeviceBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {


            User user = UserController.getUser();
            if (user != null) {

                List<AlertReminder> alertReminderList = AlertReminder.listAll(AlertReminder.class);

                for (AlertReminder alert : alertReminderList) {

                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.MONTH, alert.getMonth());
                    cal.set(Calendar.YEAR, alert.getYear());
                    cal.set(Calendar.DAY_OF_MONTH, alert.getDay());
                    cal.set(Calendar.HOUR_OF_DAY, alert.getHour());
                    cal.set(Calendar.MINUTE, alert.getMinute());
                    cal.set(Calendar.SECOND, 0);

                    Long idAlert = alert.getId();

                    Intent alarmIntent = new Intent(context, AlarmReceiver.class);
                    alarmIntent.putExtra("idAlarmToTrigger", idAlert);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, idAlert.intValue(), alarmIntent, 0);

                    AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);


                }

            }


        }
    }
}
