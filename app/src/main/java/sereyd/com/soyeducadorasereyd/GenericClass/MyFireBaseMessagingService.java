package sereyd.com.soyeducadorasereyd.GenericClass;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import sereyd.com.soyeducadorasereyd.Utils.CustomNotification;

/**
 * Created by isaiascarreraventura on 11/01/17.
 */

public class MyFireBaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        CustomNotification.createNotification(this, "SEREYD NOTIFICACION" ,remoteMessage.getNotification().getBody());

    }
}
