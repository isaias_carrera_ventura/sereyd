package sereyd.com.soyeducadorasereyd.GenericClass;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by isaiascarreraventura on 11/01/17.
 */
public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String REG_TOKEN = "REG_TOKEN";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String resentToken = FirebaseInstanceId.getInstance().getToken();
        Log.w(REG_TOKEN, resentToken);
    }

}
