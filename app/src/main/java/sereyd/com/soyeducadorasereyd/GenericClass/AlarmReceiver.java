package sereyd.com.soyeducadorasereyd.GenericClass;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import sereyd.com.soyeducadorasereyd.Models.AlertReminder;
import sereyd.com.soyeducadorasereyd.Utils.CustomNotification;

/**
 * Created by isaiascarreraventura on 10/01/17.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // For our recurring task, we'll just display a message
        Long idAlarmToTrigger = intent.getLongExtra("idAlarmToTrigger", -1);

        if (idAlarmToTrigger != -1) {

            AlertReminder alertReminder = AlertReminder.findById(AlertReminder.class, idAlarmToTrigger);
            CustomNotification.createNotification(context, alertReminder.getMyGroup().getGrade() + alertReminder.getMyGroup().getGroupLetter() + " " + alertReminder.getSubject(), alertReminder.getDescription());
            alertReminder.delete();
        }

        Log.w("mensaje", "Saludos");

    }

}
