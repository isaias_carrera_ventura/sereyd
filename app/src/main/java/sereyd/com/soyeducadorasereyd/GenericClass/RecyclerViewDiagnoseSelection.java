package sereyd.com.soyeducadorasereyd.GenericClass;

import android.view.View;

/**
 * Created by chaycv on 19/07/17.
 */

public interface RecyclerViewDiagnoseSelection {

    void recyclerViewListClicked(View v, int position);

}
