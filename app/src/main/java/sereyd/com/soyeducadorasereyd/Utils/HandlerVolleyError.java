package sereyd.com.soyeducadorasereyd.Utils;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import sereyd.com.soyeducadorasereyd.R;

/**
 * Created by chaycv on 31/05/17.
 */

public class HandlerVolleyError {

    public static String getErrorMessage(VolleyError error, Context context) {
        NetworkResponse response = error.networkResponse;
        String json = null;
        if (response != null && response.data != null) {
            switch (response.statusCode) {
                case 400:

                    return context.getResources().getString(R.string.HTTP_400);

                case 401:
                    return context.getResources().getString(R.string.HTTP_401);

                case 500:
                    return context.getResources().getString(R.string.HTTP_500);

            }
        } else {

            return context.getResources().getString(R.string.error_internet);

        }

        return "";
    }

    public static String getErrorUpdateResponse(VolleyError error, Context context) {


        try {
            if (error.networkResponse.data != null) {
                JSONObject jsonObject = new JSONObject(new String(error.networkResponse.data, "UTF-8"));
                int errorResponse = jsonObject.getInt("error");
                switch (errorResponse) {
                    case 2:
                        return jsonObject.getString("description");
                    default:
                        return null;

                }

            }
            return null;
        } catch (Exception e) {
            return null;
        }

    }


}
