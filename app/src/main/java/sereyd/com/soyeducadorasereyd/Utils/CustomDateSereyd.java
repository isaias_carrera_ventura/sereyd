package sereyd.com.soyeducadorasereyd.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by chaycv on 22/04/17.
 */

public class CustomDateSereyd {

    public static String getDate() {
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy");
        return df.format(Calendar.getInstance().getTime());
    }


}
