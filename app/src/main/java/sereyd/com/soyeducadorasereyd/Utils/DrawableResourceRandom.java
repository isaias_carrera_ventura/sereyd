package sereyd.com.soyeducadorasereyd.Utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

import sereyd.com.soyeducadorasereyd.R;

/**
 * Created by chaycv on 31/07/17.
 */

public class DrawableResourceRandom {

    public static int[] drawableResources = {
            R.drawable.shape_aqua,
            R.drawable.shape_green,
            R.drawable.shape_purple,
            R.drawable.shape_red,
            R.drawable.shape_yellow,
    };

    public static Drawable getDrawableForRandom(Context context) {

        int position = (int) (Math.random() * 5);
        return context.getResources().getDrawable(drawableResources[position]);

    }

}
