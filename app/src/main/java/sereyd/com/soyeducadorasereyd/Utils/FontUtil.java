package sereyd.com.soyeducadorasereyd.Utils;

import android.content.Context;
import android.graphics.Typeface;

import sereyd.com.soyeducadorasereyd.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by isaiascarreraventura on 08/12/16.
 */
public class FontUtil {

    public static Typeface getBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/bold.ttf");
    }

    public static Typeface getRegularFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/regular.ttf");
    }

    public static Typeface getLightFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/light.ttf");
    }

    public static Typeface getHairlineFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/hairline.ttf");
    }

    public static Typeface getBoldItalicFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/bolditalic.ttf");
    }
    //bolditalic

    public static void setFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

}
