package sereyd.com.soyeducadorasereyd.Utils;

import android.text.TextUtils;

/**
 * Created by isaiascarreraventura on 20/12/16.
 */
public class Constants {

    public static final String MESSAGE_ERROR = "ERROR MENSAJE";
    public static final String URL_SHOP = "https://www.soyeducadorasereyd.com/tienda/";
    public static final long LIMIT_NUMBER_GROUP = 8;

    public static final int MALE = 0;
    public static final int FEMALE = 1;

    public static final int ADD_ACTIVITY_RESULT = 1;
    public static final int EDIT_ACTIVITY_RESULT = 2;

    public static final int LIMIT_NUMBER_TUTORIAL_PAGES = 4;

    public static final int INCOMPLETE = 0;
    public static final int COMPLETE = 1;
    public static final int UNDEFINED = -1;
    public static final int PREMIUM = 1;
    public static final int FREE = 0;

    public static final String EMPTY_STRING = "";

    public static final String URL_SERVER = "https://app.soyeducadorasereyd.com";
    public static final String URL_ENDPOINT_USER = "https://app.soyeducadorasereyd.com/user/";
    public static final String URL_ENDPOINT_USER_IMAGE = "https://app.soyeducadorasereyd.com/user/image/";
    public static final String URL_ENDPOINT_LOGIN = "https://app.soyeducadorasereyd.com/login";
    public static final String URL_ENDPOINT_LOGIN_FACEBOOK = "https://app.soyeducadorasereyd.com/login/facebook";
    public static final String URL_ENDPOINT_GROUP = "https://app.soyeducadorasereyd.com/group/";
    public static final String URL_ENDPOINT_STUDENT = "https://app.soyeducadorasereyd.com/student/";
    public static final String URL_ENDPOINT_HOMEWORK = "https://app.soyeducadorasereyd.com/homework/";
    public static final String URL_ENDPOINT_COOPERATION = "https://app.soyeducadorasereyd.com/cooperation/";
    public static final String URL_ENDPOINT_GROUP_USER = "https://app.soyeducadorasereyd.com/group/user/";
    public static final String URL_ENDPOINT_PLANIFICATION = "https://app.soyeducadorasereyd.com/planification";
    public static final String URL_ENDPOINT_PLANIFICATION_USER = "https://app.soyeducadorasereyd.com/planification/user/";
    public static final String URL_ENDPOINT_PAYMENT = "https://app.soyeducadorasereyd.com/payment/";
    public static final String URL_ENDPOINT_PLANIFICATION_USER_DOWNLOAD = "https://app.soyeducadorasereyd.com/planification_user/";
    public static final String URL_ENDPOINT_DIAGNOSTIC = "https://app.soyeducadorasereyd.com/diagnostic";
    public static final String URL_ENDPOINT_TEST = "https://app.soyeducadorasereyd.com/test/full/";
    public static final String URL_ENDPOINT_GROUP_DIAGNOSTIC = "https://app.soyeducadorasereyd.com/group_diagnostic";
    public static final String URL_ENDPOINT_GROUP_EVALUATION = "https://app.soyeducadorasereyd.com/group_evaluation";
    public static final String URL_ENDPOINT_QUESTION_STUDENT = "https://app.soyeducadorasereyd.com/question_student";
    public static final String URL_ENDPOINT_QUESTION_EVALUATION_STUDENT = "https://app.soyeducadorasereyd.com/question_student/evaluation/";
    public static final String URL_TEST_FULL = "https://app.soyeducadorasereyd.com/group_diagnostic/final/";
    public static final String URL_TEST_EVALUATION_FULL = "https://app.soyeducadorasereyd.com/group_evaluation/final/";
    public static final String URL_PENDING_STUDENT_TEST_EVALUATION = "https://app.soyeducadorasereyd.com/group_evaluation/";
    public static final String URL_PENDING_STUDENT_TEST = "https://app.soyeducadorasereyd.com/group_diagnostic/";
    public static final String URL_ENDPOINT_TEST_FINISHED = "https://app.soyeducadorasereyd.com/group_diagnostic/completed/";
    public static final String URL_ENDPOINT_DAY_PLANNING = "https://app.soyeducadorasereyd.com/day_planning";
    public static final String URL_ENDPOINT_RESOURCE = "https://app.soyeducadorasereyd.com/resource";
    public static final String URL_ENDPOINT_RESOURCE_USER = "https://app.soyeducadorasereyd.com/resource_user/";
    public static final String URL_ENDPOINT_STUDENT_INFORMATION = "https://app.soyeducadorasereyd.com/student/information/";
    public static final String URL_ENDPOINT_EVALUATION_USER = "https://app.soyeducadorasereyd.com/evaluation/user/";
    public static final int EXPORT_OPTION = 1;
    public static final int SHARE_OPTION = 2;
    public static final int ASSIST = 1;
    public static final int NO_ASSIST = 0;
    public static final int BAD = 0;
    public static final int NORMAL = 1;
    public static final int GOOD = 2;
    public static final int NO_ELEMENTS = 0;
    public static final String URL_ENDPOINT_EVALUATION = "https://app.soyeducadorasereyd.com/evaluation/";
    public static final String URL_DIAGNOSTIC_PENDING = "https://app.soyeducadorasereyd.com/group_diagnostic/pending/";
    public static final String FAQ_URL = "https://www.soyeducadorasereyd.com/app/preguntas.html";
    public static final String TERMS_URL = "https://www.soyeducadorasereyd.com/app/privacidad.html";
    public static final String URL_ENDPOINT_TEST_EVALUATION_FINISHED = "https://app.soyeducadorasereyd.com/group_evaluation/completed/";
    public static final String URL_EVALUATION_PENDING = "https://app.soyeducadorasereyd.com/group_evaluation/pending/";
    public static final String URL_RESTORE_PASSWORD = "https://app.soyeducadorasereyd.com/password";
    public static final String URL_RESTORE_PASSWORD_CHANGE = "https://app.soyeducadorasereyd.com/password/";
    public static final String URL_EVALUATION_FILE = "https://app.soyeducadorasereyd.com/group_evaluation/file/";
    public static final int SUSCRIPTION_MONTH = 0;
    public static final int SUSCRIPTION_YEAR = 1;

    public static final String URL_DIAGNOSE_FILE = "https://app.soyeducadorasereyd.com/group_diagnostic/file/";

    public static final int ACTIVE_SUSCRIPTION = 2;
    public static final int TEST_PERIOD = 1;
    public static final int PAYMENT_REQUIRED = 3;

    public static final String URL_ENDPOINT_PAYMENT_CARD = "https://app.soyeducadorasereyd.com/openpay/";
    public static final String URL_DIARY = "https://app.soyeducadorasereyd.com/diary";
    public static final String URL_ENDPOINT_DISCUSSION = "https://app.soyeducadorasereyd.com/discussion";
    public static final String URL_ENDPOINT_ANSWER = "https://app.soyeducadorasereyd.com/answer";
    public static final String URL_ENDPOINT_ANSWER_RANKING = "https://app.soyeducadorasereyd.com/answer_ranking";
    public static final String CANCEL_SUSCRIPTION_ENDPOINT = "https://app.soyeducadorasereyd.com/subscription/cancel/";
    public static int CANCELLED = 1;

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
