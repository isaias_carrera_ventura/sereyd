package sereyd.com.soyeducadorasereyd.Utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import sereyd.com.soyeducadorasereyd.InicioActivity;
import sereyd.com.soyeducadorasereyd.R;

/**
 * Created by isaiascarreraventura on 10/01/17.
 */
public class CustomNotification {

    public static void createNotification(Context context, String title, String text) {

        // notification is selected
        Intent intent = new Intent(context, InicioActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 0);

        // Build notification
        // Actions are just fake
        Notification noti = new Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent).build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;
        noti.defaults |= Notification.DEFAULT_VIBRATE;
        noti.defaults |= Notification.DEFAULT_SOUND;

        notificationManager.notify(0, noti);
    }
}


