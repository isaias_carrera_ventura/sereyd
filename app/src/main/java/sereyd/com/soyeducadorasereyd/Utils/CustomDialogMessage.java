package sereyd.com.soyeducadorasereyd.Utils;

import android.content.Context;
import android.graphics.Color;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by isaiascarreraventura on 08/01/17.
 */
public class CustomDialogMessage {

    public static void showMessage(Context context, MaterialDialog.SingleButtonCallback positiveCallback, String title, String message) {
        new MaterialDialog.Builder(context)
                .backgroundColor(Color.WHITE)
                .titleColor(Color.GRAY)
                .contentColor(Color.GRAY)
                .title(title)
                .content(message)
                .positiveText("Aceptar")
                .negativeText("Cancelar")
                .onPositive(positiveCallback)
                .show();
    }

}
