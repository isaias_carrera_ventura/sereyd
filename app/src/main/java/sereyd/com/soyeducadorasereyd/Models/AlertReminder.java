package sereyd.com.soyeducadorasereyd.Models;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sereyd.com.soyeducadorasereyd.GenericClass.AlarmReceiver;

/**
 * Created by isaiascarreraventura on 10/01/17.
 */
public class AlertReminder extends SugarRecord implements Serializable {

    private int year, month, day; //DATE
    private int hour, minute; //TIME
    private String subject;
    private String description;
    private MyGroup myGroup;

    public AlertReminder() {
    }

    public AlertReminder(int year, int month, int day, int hour, int minute, String subject, String description, MyGroup myGroup) {

        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.subject = subject;
        this.description = description;
        this.myGroup = myGroup;

    }

    public MyGroup getMyGroup() {
        return myGroup;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMyGroup(MyGroup myGroup) {
        this.myGroup = myGroup;
    }

    public static void removeAlertsFromDatabase(Context context) {

        List<AlertReminder> reminderList = AlertReminder.listAll(AlertReminder.class);

        for (AlertReminder alarmItem : reminderList) {

            Intent alarmIntent = new Intent(context, AlarmReceiver.class);
            Long idAlert = alarmItem.getId();
            alarmIntent.putExtra("idAlarmToTrigger", idAlert);
            PendingIntent pi = PendingIntent.getBroadcast(context, idAlert.intValue(), alarmIntent, 0);
            AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            manager.cancel(pi);
            pi.cancel();

        }
    }

    public static ArrayList<AlertReminder> getAlerts(Long id) {
        return (ArrayList<AlertReminder>) AlertReminder.find(AlertReminder.class, "MY_GROUP = ?", String.valueOf(id));
    }

    public static AlertReminder getFirstAlerReminder(Long id) {

        ArrayList<AlertReminder> alertReminderArrayList = (ArrayList<AlertReminder>) AlertReminder.findWithQuery(AlertReminder.class, "Select * FROM ALERT_REMINDER WHERE MY_GROUP = " + id + " LIMIT 1");
        if (alertReminderArrayList.size() > 0) {
            return alertReminderArrayList.get(0);
        } else {
            return null;
        }

    }
}
