package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by isaiascarreraventura on 26/12/16.
 */

public class MyGroup extends SugarRecord implements Serializable {

    private int idGpr;
    private String school;
    private int grade;
    private String groupLetter;
    private String period;
    private double attendancePercent;

    public MyGroup() {
    }

    public MyGroup(int idGpr, String school, int grade, String group, String period, Double attendancePercent) {

        super();
        this.idGpr = idGpr;
        this.school = school;
        this.grade = grade;
        this.groupLetter = group;
        this.period = period;
        this.attendancePercent = attendancePercent;
    }

    public int getIdGpr() {
        return idGpr;
    }

    public void setIdGpr(int idGpr) {
        this.idGpr = idGpr;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }


    public String getSchool() {
        return school;
    }

    public int getGrade() {
        return grade;
    }

    public String getGroupLetter() {
        return groupLetter;
    }


    public void setAttendancePercent(double attendancePercent) {
        this.attendancePercent = attendancePercent;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public void setGroupLetter(String groupLetter) {
        this.groupLetter = groupLetter;
    }


    public double getAttendancePercent() {
        return attendancePercent;
    }
}
