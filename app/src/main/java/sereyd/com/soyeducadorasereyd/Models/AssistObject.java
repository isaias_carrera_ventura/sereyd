package sereyd.com.soyeducadorasereyd.Models;

/**
 * Created by chaycv on 02/08/17.
 */

public class AssistObject {

    private String idStudent;
    private int assist;
    private int conductT;

    public AssistObject(String idStudent, int assist, int conudcT) {
        this.idStudent = idStudent;
        this.assist = assist;
        this.conductT = conudcT;
    }

    public String getIdStudent() {
        return idStudent;
    }

    public int getAssistStatus() {
        return assist;
    }

    public void setIdStudent(String idStudent) {
        this.idStudent = idStudent;
    }

    public void setAssist(int assist) {
        this.assist = assist;
    }

    public void setConductT(int conductT) {
        this.conductT = conductT;
    }

    public int getConudcT() {
        return conductT;
    }

}
