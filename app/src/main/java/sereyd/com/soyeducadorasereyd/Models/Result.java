package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

import java.util.ArrayList;

/**
 * Created by isaiascarreraventura on 28/02/17.
 * Project SoyeducadoraSEREYD
 */

public class Result extends SugarRecord {

    Diagnose diagnose;
    Student student;
    int result;

    public Result() {
    }

    public Result(Diagnose diagnose, Student student, int result) {
        this.diagnose = diagnose;
        this.student = student;
        this.result = result;
    }

    public Diagnose getDiagnose() {
        return diagnose;
    }

    public Student getStudent() {
        return student;
    }

    public int getResult() {
        return result;
    }

    public void setDiagnose(Diagnose diagnose) {
        this.diagnose = diagnose;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public static ArrayList<Result> getResult(Long id) {
        return (ArrayList<Result>) Result.find(Result.class, "DIAGNOSE = ?", String.valueOf(id));
    }

    public static int getResultNumberInDiagnose(Long id) {
        return Result.getResult(id).size();
    }

}
