package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by chaycv on 01/05/17.
 */

public class Diary extends SugarRecord implements Serializable {

    int idDiary;
    private String title, text;
    private String date;

    public Diary() {

    }

    public Diary(int idDiary, String title, String text, String date) {
        this.idDiary = idDiary;
        this.title = title;
        this.text = text;
        this.date = date;
    }

    public int getIdDiary() {
        return idDiary;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getDate() {
        return date;
    }

    public void setIdDiary(int idDiary) {
        this.idDiary = idDiary;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
