package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

/**
 * Created by chaycv on 03/01/18.
 */

public class AssistBU extends SugarRecord {

    MyGroup myGroup;
    Student student;
    int assistance;
    int conduct;

    public AssistBU(){

    }

    public AssistBU(MyGroup myGroup, Student student, int assistance, int conduct) {
        this.myGroup = myGroup;
        this.student = student;
        this.assistance = assistance;
        this.conduct = conduct;
    }

    public void setMyGroup(MyGroup myGroup) {
        this.myGroup = myGroup;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setAssistance(int assistance) {
        this.assistance = assistance;
    }

    public void setConduct(int conduct) {
        this.conduct = conduct;
    }

    public MyGroup getMyGroup() {
        return myGroup;
    }

    public Student getStudent() {
        return student;
    }

    public int getAssistance() {
        return assistance;
    }

    public int getConduct() {
        return conduct;
    }
}
