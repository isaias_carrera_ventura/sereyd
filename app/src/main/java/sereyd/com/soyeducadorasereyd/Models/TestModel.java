package sereyd.com.soyeducadorasereyd.Models;

import java.io.Serializable;
import java.util.List;

import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by chaycv on 25/07/17.
 */

public class TestModel implements Serializable {

    String idTest;
    String nameTest;
    String questionString;
    String idQuestion;
    int result;

    public TestModel(String idTest, String nameTest, String questionString, String idQuestion) {
        this.idTest = idTest;
        this.nameTest = nameTest;
        this.questionString = questionString;
        this.idQuestion = idQuestion;
        this.result = Constants.UNDEFINED;
    }

    public String getIdTest() {
        return idTest;
    }

    public String getNameTest() {
        return nameTest;
    }

    public String getQuestionString() {
        return questionString;
    }

    public String getIdQuestion() {
        return idQuestion;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }
}
