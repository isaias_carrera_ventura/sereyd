package sereyd.com.soyeducadorasereyd.Models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by chaycv on 19/12/17.
 */

public class ForumDiscussionModel implements Serializable {

    String idAnswer, answer, date, name, image;
    int ranking;

    public ForumDiscussionModel(String idAnswer, String answer, String date, String name, String image, int ranking) {
        this.idAnswer = idAnswer;
        this.answer = answer;
        this.date = date;
        this.name = name;
        this.image = image;
        this.ranking = ranking;
    }

    public int getRanking() {
        return ranking;
    }

    public String getIdAnswer() {
        return idAnswer;
    }

    public String getAnswer() {
        return answer;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

}
