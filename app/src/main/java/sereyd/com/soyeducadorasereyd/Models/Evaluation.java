package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by chaycv on 23/08/17.
 */

public class Evaluation extends SugarRecord implements Serializable {

    private String name;
    private int state;
    private String evaluationId;
    private String evaluationGroupId;
    private String date;
    private MyGroup myGroup;

    public Evaluation() {
    }

    public Evaluation(String name, int state, String evaluationId, String evaluationGroupId, String date, MyGroup myGroup) {
        this.name = name;
        this.state = state;
        this.evaluationId = evaluationId;
        this.evaluationGroupId = evaluationGroupId;
        this.date = date;
        this.myGroup = myGroup;
    }

    public String getName() {
        return name;
    }

    public int getState() {
        return state;
    }

    public String getEvaluationId() {
        return evaluationId;
    }

    public String getEvaluationGroupId() {
        return evaluationGroupId;
    }

    public String getDate() {
        return date;
    }

    public MyGroup getMyGroup() {
        return myGroup;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setEvaluationId(String evaluationId) {
        this.evaluationId = evaluationId;
    }

    public void setEvaluationGroupId(String evaluationGroupId) {
        this.evaluationGroupId = evaluationGroupId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setMyGroup(MyGroup myGroup) {
        this.myGroup = myGroup;
    }
}
