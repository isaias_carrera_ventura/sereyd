package sereyd.com.soyeducadorasereyd.Models;

import java.io.Serializable;

/**
 * Created by chaycv on 19/12/17.
 */

public class ForumQuestionModel implements Serializable{

    String idDiscussion;
    String title, question, date, idUser, nameUser, imageUser;
    int ranking;

    public ForumQuestionModel(String idDiscussion, String title, String question, String date, String idUser, String nameUser, String imageUser, int ranking) {
        this.idDiscussion = idDiscussion;
        this.title = title;
        this.question = question;
        this.date = date;
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.ranking = ranking;
        this.imageUser = imageUser;
    }

    public int getRanking() {
        return ranking;
    }

    public String getIdDiscussion() {
        return idDiscussion;
    }

    public String getTitle() {
        return title;
    }

    public String getQuestion() {
        return question;
    }

    public String getDate() {
        return date;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public String getImageUser() {
        return imageUser;
    }
}
