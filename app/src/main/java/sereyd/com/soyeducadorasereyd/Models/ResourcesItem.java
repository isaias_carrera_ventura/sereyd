package sereyd.com.soyeducadorasereyd.Models;

/**
 * Created by chaycv on 04/08/17.
 */

public class ResourcesItem {

    String name;
    String id;

    public ResourcesItem(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
