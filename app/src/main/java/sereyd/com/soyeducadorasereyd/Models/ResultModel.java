package sereyd.com.soyeducadorasereyd.Models;

/**
 * Created by chaycv on 27/07/17.
 */

public class ResultModel {

    String name;
    String level;
    int percent;

    public ResultModel(String name, String level, int percent) {
        this.name = name;
        this.level = level;
        this.percent = percent;
    }

    public String getName() {
        return name;
    }

    public String getLevel() {
        return level;
    }

    public int getPercent() {
        return percent;
    }
}
