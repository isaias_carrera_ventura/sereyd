package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by chaycv on 21/08/17.
 */

public class InformationStudent extends SugarRecord implements Serializable {

    int attendance;
    String behaviorDescription;
    double behaviorAverage;
    double language;
    double math;
    double world;
    double health;
    double social;
    double art;

    double diagnostic;
    double schoolPerformance;

    Student student;

    public InformationStudent() {
    }

    public InformationStudent(int attendance, String behaviorDescription, double behaviorAverage, double language, double math, double world, double health, double social, double art, double diagnostic, double schoolPerformance, Student student) {
        this.attendance = attendance;
        this.behaviorDescription = behaviorDescription;
        this.behaviorAverage = behaviorAverage;
        this.language = language;
        this.math = math;
        this.world = world;
        this.health = health;
        this.social = social;
        this.art = art;
        this.diagnostic = diagnostic;
        this.schoolPerformance = schoolPerformance;
        this.student = student;

    }

    public int getAttendance() {
        return attendance;
    }

    public String getBehaviorDescription() {
        return behaviorDescription;
    }

    public double getBehaviorAverage() {
        return behaviorAverage;
    }

    public double getLanguage() {
        return language;
    }

    public double getMath() {
        return math;
    }

    public double getWorld() {
        return world;
    }

    public double getHealth() {
        return health;
    }

    public double getSocial() {
        return social;
    }

    public double getArt() {
        return art;
    }

    public double getDiagnostic() {
        return diagnostic;
    }

    public double getSchoolPerformance() {
        return schoolPerformance;
    }
    public Student getStudent() {
        return student;
    }

    public void setAttendance(int attendance) {
        this.attendance = attendance;
    }

    public void setBehaviorDescription(String behaviorDescription) {
        this.behaviorDescription = behaviorDescription;
    }

    public void setBehaviorAverage(double behaviorAverage) {
        this.behaviorAverage = behaviorAverage;
    }

    public void setLanguage(double language) {
        this.language = language;
    }

    public void setMath(double math) {
        this.math = math;
    }

    public void setWorld(double world) {
        this.world = world;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public void setSocial(double social) {
        this.social = social;
    }

    public void setArt(double art) {
        this.art = art;
    }

    public void setDiagnostic(double diagnostic) {
        this.diagnostic = diagnostic;
    }

    public void setSchoolPerformance(double schoolPerformance) {
        this.schoolPerformance = schoolPerformance;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
