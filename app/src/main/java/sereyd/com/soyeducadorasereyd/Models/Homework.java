package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by isaiascarreraventura on 22/01/17.
 */

public class Homework extends SugarRecord implements Serializable {


    private String idHomework;
    private String name, description;
    private int year, month, day; //DATE

    private MyGroup myGroup;

    public Homework() {
    }

    public Homework(String idHomework, int year, int month, int day, String name, String description, MyGroup myGroup) {

        this.idHomework = idHomework;
        this.name = name;
        this.description = description;
        this.year = year;
        this.month = month;
        this.day = day;
        this.myGroup = myGroup;

    }


    public MyGroup getMyGroup() {
        return myGroup;
    }

    public String getIdHomework() {
        return idHomework;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMyGroup(MyGroup myGroup) {
        this.myGroup = myGroup;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

}
