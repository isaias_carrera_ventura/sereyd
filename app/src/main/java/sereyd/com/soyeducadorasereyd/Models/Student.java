package sereyd.com.soyeducadorasereyd.Models;

import android.content.Context;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by isaiascarreraventura on 02/01/17.
 */
public class Student extends SugarRecord implements Serializable {

    String name;
    String lastName;
    String birthday;
    int sex;
    String emergencyContact;
    String emailTutor;
    String idStudent;
    String imageUrl;

    MyGroup myGroup;

    public Student() {
    }

    public Student(String name, String lastName, String birthday, int sex, String emergencyContact, String emailTutor, String idStudent, String imageUrl, MyGroup myGroup) {
        super();
        this.imageUrl = imageUrl;
        this.name = name;
        this.lastName = lastName;
        this.birthday = birthday;
        this.sex = sex;
        this.emergencyContact = emergencyContact;
        this.emailTutor = emailTutor;
        this.idStudent = idStudent;
        this.myGroup = myGroup;
    }

    public void setEmailTutor(String emailTutor) {
        this.emailTutor = emailTutor;
    }

    public String getId_Student() {
        return idStudent;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setMyGroup(MyGroup myGroup) {
        this.myGroup = myGroup;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public int getSex() {
        return sex;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public MyGroup getMyGroup() {
        return myGroup;
    }

    public String getEmailTutor() {
        return emailTutor;
    }


}
