package sereyd.com.soyeducadorasereyd.Models;

/**
 * Created by chaycv on 27/09/17.
 */

public class PaymentObjectSereyd {

    int error;
    int codeError;
    String description;
    boolean annual;
    int resourceCount;
    int planningCount;

    public PaymentObjectSereyd(int error, int codeError, String description, boolean annual, int resourceCount, int planningCount) {
        this.error = error;
        this.codeError = codeError;
        this.description = description;
        this.annual = annual;
        this.resourceCount = resourceCount;
        this.planningCount = planningCount;
    }

    public int getError() {
        return error;
    }

    public int getCodeError() {
        return codeError;
    }

    public String getDescription() {
        return description;
    }

    public boolean isAnnual() {
        return annual;
    }

    public int getResourceCount() {
        return resourceCount;
    }

    public int getPlanningCount() {
        return planningCount;
    }
}
