package sereyd.com.soyeducadorasereyd.Models;

/**
 * Created by chaycv on 02/08/17.
 */

public class AssistModel {

    private String idGroup;
    private AssistObject assist;

    public AssistModel(String idGroup, AssistObject assistAux) {
        this.idGroup = idGroup;
        assist = assistAux;
    }

    public String getIdGroup() {
        return idGroup;
    }

    public AssistObject getAssist() {
        return assist;
    }


}

