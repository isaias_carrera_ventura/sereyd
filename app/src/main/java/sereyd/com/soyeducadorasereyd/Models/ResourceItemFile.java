package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by chaycv on 04/08/17.
 */

public class ResourceItemFile extends SugarRecord implements Serializable {

    String idResource;
    String name;
    String urlFile;

    public ResourceItemFile() {

    }

    public void setUrlFile(String urlFile) {
        this.urlFile = urlFile;
    }

    public ResourceItemFile(String idResource, String name, String urlFile) {
        this.idResource = idResource;
        this.name = name;
        this.urlFile = urlFile;
    }

    public String getIdResource() {
        return idResource;
    }

    public String getName() {
        return name;
    }

    public String getUrlFile() {
        return urlFile;
    }
}
