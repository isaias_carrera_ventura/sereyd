package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by isaiascarreraventura on 20/01/17.
 */

public class Cooperation extends SugarRecord implements Serializable {

    private String concept;
    private String description;
    private Double ammount;
    private String id_cooperation;
    private MyGroup myGroup;

    public Cooperation() {
    }

    public Cooperation(String id_cooperation, String concept, String description, Double ammount, MyGroup myGroup) {
        this.concept = concept;
        this.description = description;
        this.ammount = ammount;
        this.myGroup = myGroup;
        this.id_cooperation = id_cooperation;
    }

    public String getId_cooperation() {
        return id_cooperation;
    }

    public String getConcept() {
        return concept;
    }

    public String getDescription() {
        return description;
    }

    public Double getAmmount() {
        return ammount;
    }

    public MyGroup getMyGroup() {
        return myGroup;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAmmount(Double ammount) {
        this.ammount = ammount;
    }

    public void setMyGroup(MyGroup myGroup) {
        this.myGroup = myGroup;
    }


}

