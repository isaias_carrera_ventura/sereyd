package sereyd.com.soyeducadorasereyd.Models;

/**
 * Created by chaycv on 19/07/17.
 */

public class DiagnoseSelection {

    private String idDiagnose;
    private String name;

    public DiagnoseSelection(String idDiagnose, String name) {
        this.idDiagnose = idDiagnose;
        this.name = name;
    }

    public String getIdDiagnose() {
        return idDiagnose;
    }

    public String getName() {
        return name;
    }
}
