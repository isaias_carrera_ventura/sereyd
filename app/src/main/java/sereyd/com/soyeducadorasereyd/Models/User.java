package sereyd.com.soyeducadorasereyd.Models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by isaiascarreraventura on 12/12/16.
 */
public class User extends SugarRecord {

    private String name, idUser, email, linkImage;

    public User() {

    }

    public User(String name, String idUser, String email, String linkImage) {
        super();
        this.name = name;
        this.idUser = idUser;
        this.email = email;
        this.linkImage = linkImage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public String getName() {
        return name;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getEmail() {
        return email;
    }

    public String getLinkImage() {
        return linkImage;
    }



}
