package sereyd.com.soyeducadorasereyd.Models;

import android.util.Log;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;

import sereyd.com.soyeducadorasereyd.Utils.Constants;

/**
 * Created by isaiascarreraventura on 28/02/17.
 * Project SoyeducadoraSEREYD
 */

public class Diagnose extends SugarRecord implements Serializable {

    private String date;
    private int state;
    private int diagnoseId;
    private int idGroupDiagnose;
    private MyGroup myGroup;

    public Diagnose() {
    }

    public Diagnose(String date, int state, int diagnoseId, int idGroupDiagnose, MyGroup myGroup) {
        this.date = date;
        this.state = state;
        this.diagnoseId = diagnoseId;
        this.idGroupDiagnose = idGroupDiagnose;
        this.myGroup = myGroup;
    }

    public int getIdGroupDiagnose() {
        return idGroupDiagnose;
    }

    public int getDiagnoseId() {
        return diagnoseId;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {

        return state;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setMyGroup(MyGroup myGroup) {
        this.myGroup = myGroup;
    }

    public String getDate() {
        return date;
    }

    public MyGroup getMyGroup() {
        return myGroup;
    }

}
