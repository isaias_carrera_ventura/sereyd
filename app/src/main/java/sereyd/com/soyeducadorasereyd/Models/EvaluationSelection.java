package sereyd.com.soyeducadorasereyd.Models;

/**
 * Created by chaycv on 23/08/17.
 */

public class EvaluationSelection {

    private String idEvaluation;
    private String name;

    public EvaluationSelection(String idEvaluation, String name) {
        this.idEvaluation = idEvaluation;
        this.name = name;
    }

    public String getIdEvaluation() {
        return idEvaluation;
    }

    public String getName() {
        return name;
    }
}
