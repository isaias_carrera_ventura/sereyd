package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by chaycv on 12/07/17.
 */

public class Planning extends SugarRecord implements Serializable {

    private int idPlanning;
    private String urlPDF;
    private String name;
    private String description;
    private String urlImage;
    private String urlWord;
    private String urlPreview;
    private String field;
    private int type;

    public Planning() {
    }

    public Planning(int idPlanning, String urlFile, String urlWord, String urlPreview, String name, String description, String urlImage, String field, int type) {
        this.idPlanning = idPlanning;
        this.urlPDF = urlFile;
        this.urlWord = urlWord;
        this.urlPreview = urlPreview;
        this.name = name;
        this.description = description;
        this.urlImage = urlImage;
        this.field = field;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public int getIdPlanning() {
        return idPlanning;
    }

    public String getUrlPDF() {
        return urlPDF;
    }

    public String getUrlWord() {
        return urlWord;
    }

    public String getUrlPreview() {
        return urlPreview;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public String getField() {
        return field;
    }
}
