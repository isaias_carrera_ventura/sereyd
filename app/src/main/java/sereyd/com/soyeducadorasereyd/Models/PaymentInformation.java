package sereyd.com.soyeducadorasereyd.Models;

import com.orm.SugarRecord;
import com.paypal.android.sdk.payments.PaymentConfirmation;

/**
 * Created by chaycv on 31/12/17.
 */

public class PaymentInformation extends SugarRecord {

    private String description, initDate, finishDate;
    private int code;

    private String cardNumber, holderName, chargeDate, descriptionCard;
    private Double amount;
    private int cancelled;

    public PaymentInformation(int code) {
        this.code = code;
    }

    public void setCancelled(int cancelled) {
        this.cancelled = cancelled;
    }

    public int getCancelled() {
        return cancelled;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setInitDate(String initDate) {
        this.initDate = initDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public void setChargeDate(String chargeDate) {
        this.chargeDate = chargeDate;
    }

    public void setDescriptionCard(String descriptionCard) {
        this.descriptionCard = descriptionCard;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public String getInitDate() {
        return initDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getHolderName() {
        return holderName;
    }

    public String getChargeDate() {
        return chargeDate;
    }

    public String getDescriptionCard() {
        return descriptionCard;
    }

    public Double getAmount() {
        return amount;
    }
}
